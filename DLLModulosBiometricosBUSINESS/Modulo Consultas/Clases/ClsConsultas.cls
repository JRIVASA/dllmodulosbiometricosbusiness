VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsConsultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Connection As Connection
Dim rsConsulta As ADODB.Recordset

Public strBotonPresionado As String
Public strCadBusCod As String
Public strCadBusDes As String
Public strOrderBy As String
Public strTitulo As String

'-----------------------------------

Public strItemC1 As String
Public strItemC2 As String
Public strItemC3 As String
Public strItemC4 As String
Public strItemC5 As String
Public strItemC6 As String
Public strItemC7 As String
Public strItemC8 As String
Public strItemC9 As String
Public strItemC10 As String

'-----------------------------------

Dim intNumCol As Integer
Dim lista As ListView
Dim Item As ListItem
Dim pantalla As FrmConsultas

Public Function Consulta_Inicializar(strSSql As String, CConnection As ADODB.Connection, Optional VerImpPrevio As Boolean) As Boolean
    intNumCol = 0
    Set Connection = CConnection
    Set pantalla = New FrmConsultas
    Set pantalla.Connection = Connection
    
   If VerImpPrevio = True Then
            pantalla.cmd_Imprimir.Visible = True
            pantalla.cmd_Previo.Visible = True
            pantalla.cmd_Buscar.Visible = False
    
    Else
            pantalla.cmd_Imprimir.Visible = False
            pantalla.cmd_Previo.Visible = False
            pantalla.cmd_Buscar.Visible = True
    End If
    pantalla.strSQL = strSSql
    Set lista = pantalla.GRID
End Function

Public Function Consulta_Limpiar()

    lista.ListItems.Clear
End Function

Public Function Consulta_AgregarCol(strnombre As String, intWidth As Integer, intAligmnet As String) As Boolean
       lista.ColumnHeaders.Add , , strnombre, intWidth, intAligmnet
       intNumCol = intNumCol + 1
End Function

Public Function Consulta_Hide()
       Set pantalla = Nothing
End Function

Public Function Consulta_Show()
    pantalla.strCadBusCod = Me.strCadBusCod
    pantalla.strCadBusDes = Me.strCadBusDes
    pantalla.strOrderBy = Me.strOrderBy
    pantalla.lblTitulo = Me.strTitulo
    pantalla.txtDato.Text = "%"
    On Error Resume Next
    pantalla.Show vbModal
    Me.strBotonPresionado = pantalla.strBotonPresionado
    Select Case pantalla.strBotonPresionado
        Case "Aceptar", "Previo", "Reimprimir"
                strItemC1 = IIf(lista.ListItems(lista.SelectedItem.index) <> "", lista.ListItems(lista.SelectedItem.index), "")
                
                If intNumCol >= 2 Then
                  strItemC2 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(1), "")
                End If
                If intNumCol >= 3 Then
                  strItemC3 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(2), "")
                End If
                If intNumCol >= 4 Then
                  strItemC4 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(3), "")
                End If
                If intNumCol >= 5 Then
                  strItemC5 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(4), "")
                End If
                If intNumCol >= 6 Then
                  strItemC6 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(5), "")
                End If
                If intNumCol >= 7 Then
                  strItemC7 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(6), "")
                End If
                If intNumCol >= 8 Then
                  strItemC8 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(7), "")
                End If
                If intNumCol >= 9 Then
                  strItemC9 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(8), "")
                End If
                If intNumCol >= 10 Then
                  strItemC10 = IIf(lista.ListItems(lista.SelectedItem.index).SubItems(1) <> "", lista.ListItems(lista.SelectedItem.index).SubItems(9), "")
                End If
                Unload pantalla
                
        Case "Salir"
                
    End Select

End Function
