VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FrmConsultas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "B�squeda de..."
   ClientHeight    =   6405
   ClientLeft      =   1380
   ClientTop       =   1725
   ClientWidth     =   8445
   Icon            =   "FrmConsultas.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6405
   ScaleWidth      =   8445
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame4 
      Caption         =   " Acci�n a tomar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   885
      Left            =   6165
      TabIndex        =   11
      Top             =   5520
      Width           =   2265
      Begin VB.CommandButton cmd_Buscar 
         Caption         =   "&Buscar"
         CausesValidation=   0   'False
         Height          =   570
         Left            =   120
         Picture         =   "FrmConsultas.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   675
      End
      Begin VB.CommandButton cmd_Imprimir 
         Caption         =   "&Imprimir"
         CausesValidation=   0   'False
         Height          =   570
         Left            =   825
         Picture         =   "FrmConsultas.frx":6594
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   675
      End
      Begin VB.CommandButton cmd_Previo 
         Caption         =   "&Ver"
         CausesValidation=   0   'False
         DisabledPicture =   "FrmConsultas.frx":6696
         Height          =   570
         Left            =   120
         Picture         =   "FrmConsultas.frx":6B5C
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   675
      End
      Begin VB.CommandButton cmd_salir 
         Caption         =   "&Salir"
         CausesValidation=   0   'False
         Height          =   570
         Left            =   1545
         Picture         =   "FrmConsultas.frx":7022
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   645
      End
   End
   Begin VB.PictureBox CoolBar1 
      Align           =   1  'Align Top
      Height          =   795
      Left            =   0
      ScaleHeight     =   735
      ScaleWidth      =   8385
      TabIndex        =   7
      Top             =   0
      Width           =   8445
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   30
         TabIndex        =   8
         Top             =   30
         Width           =   8325
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   705
            Left            =   60
            TabIndex        =   9
            Top             =   30
            Width           =   8190
            _ExtentX        =   14446
            _ExtentY        =   1244
            ButtonWidth     =   1349
            ButtonHeight    =   1244
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de la B�squeda"
                  ImageIndex      =   9
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda del Sistema"
                  ImageIndex      =   10
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Ordenar y Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   780
      Left            =   120
      TabIndex        =   4
      Top             =   5520
      Width           =   1980
      Begin VB.OptionButton optCod 
         Caption         =   "Por &C�digo"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   6
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton optDes 
         Caption         =   "Por &Descripci�n"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   480
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   780
      Left            =   2145
      TabIndex        =   1
      Top             =   5520
      Width           =   4005
      Begin VB.TextBox txtDato 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   75
         TabIndex        =   0
         Top             =   210
         Width           =   3885
      End
      Begin MSComctlLib.ProgressBar barra_prg 
         Height          =   195
         Left            =   75
         TabIndex        =   2
         Top             =   510
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   344
         _Version        =   393216
         Appearance      =   1
         Max             =   1000
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1155
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":749C
            Key             =   "Agrergar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":8178
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":8E54
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":9B30
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":A80C
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":B4E8
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":C1C4
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":CEA0
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":DB7C
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":DE98
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   1845
      Top             =   630
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":EB74
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":F850
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":FB6C
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":FE88
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":10B64
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":11840
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":1251C
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":12838
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":13514
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":13830
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   480
      Top             =   585
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":1450C
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":151E8
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":15EC4
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":161E0
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":16EBC
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":17B98
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":18874
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":19550
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":1A22C
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConsultas.frx":1A548
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   4275
      Left            =   120
      TabIndex        =   10
      Top             =   1170
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   7541
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "System"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Image Image1 
      Height          =   3060
      Left            =   2520
      Picture         =   "FrmConsultas.frx":1B224
      Stretch         =   -1  'True
      Top             =   1710
      Width           =   3240
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   8115
   End
End
Attribute VB_Name = "FrmConsultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Connection As New ADODB.Connection
Public strBotonPresionado As String
Public strSQL As String
Dim strSqlMasCondicion As String
Public strCadBusCod As String
Public strCadBusDes As String
Public strOrderBy As String
Public Reimpresion As Boolean

Private Sub cmd_Buscar_Click()
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub cmd_Imprimir_Click()
    Me.strBotonPresionado = "Imprimir"
    grid_DblClick
End Sub

Private Sub cmd_Previo_Click()
    Me.strBotonPresionado = "Previo"
    grid_DblClick
End Sub

Private Sub cmd_salir_Click()
    Me.strBotonPresionado = "Salir"
    Me.Hide
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF12
            txtDato_KeyDown vbKeyEscape, 0
    End Select
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
         cmd_salir_Click
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
   Case "Aceptar"
   Case "Cancelar"
   Case "Informaci�n"
   Case "Salir"
     txtDato_KeyDown vbKeyEscape, 0
   End Select
End Sub

Private Sub txtDato_GotFocus()
  If Mid(txtDato, 1, 1) = "%" Then txtDato.SelStart = 1
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then
       strBotonPresionado = "Salir"
       Me.Hide
    End If
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
  On Error GoTo GetError
  
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            If txtDato = "" Then
                txtDato.SetFocus
                Exit Sub
            End If
            
            If Trim(txtDato) = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
               
                strSqlMasCondicion = strSQL
                If strOrderBy <> "" Then
                         strSqlMasCondicion = strSqlMasCondicion + " order by  " + Me.strOrderBy
                      End If
                Consulta_Mostrar
                
               Exit Sub
            Else
               If optCod.value = True Then
                  If strCadBusCod <> "" Then
                      strSqlMasCondicion = strSQL + " AND " + strCadBusCod + " LIKE '" + Me.txtDato + "%'"
                      
                      If InStr(strSQL, " where ") Or InStr(strSQL, " WHERE ") Then
                           strSqlMasCondicion = strSQL + " AND " + strCadBusCod + " LIKE '" + Me.txtDato + "%'"
                      Else
                           strSqlMasCondicion = strSQL + " WHERE  " + strCadBusCod + " LIKE '" + Me.txtDato + "%'"
                      End If
                      
                      If strOrderBy <> "" Then
                         strSqlMasCondicion = strSqlMasCondicion + " order by  " + Me.strOrderBy
                      End If
                      
                  Else
                     MsgBox "DEBE ESPECIFICAR MEJOR"
                     Screen.MousePointer = 0
                     Exit Sub
                  End If
               End If
               
               If optDes.value = True Then
                    If strCadBusDes <> "" Then
                       
                      If InStr(strSQL, "where") Or InStr(strSQL, "WHERE") Then
                           strSqlMasCondicion = strSQL + " AND " + strCadBusDes + " LIKE '" + Me.txtDato + "%'"
                      Else
                           strSqlMasCondicion = strSQL + " WHERE  " + strCadBusDes + " LIKE '" + Me.txtDato + "%'"
                      End If
                      
                      If strOrderBy <> "" Then
                         strSqlMasCondicion = strSqlMasCondicion + " order by  " + Me.strOrderBy
                      End If
                      
                    Else
                       MsgBox "DEBE ESPECIFICAR MEJOR"
                       Screen.MousePointer = 0
                       Exit Sub
                    End If
               End If
            End If
            Consulta_Mostrar
    End Select
    Screen.MousePointer = 0
    On Error GoTo 0
  Exit Sub
GetError:
  MsgBox "Error N0." + Err.Description
  Err.Clear
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If GRID.SortOrder = lvwAscending Then
        GRID.SortOrder = lvwDescending
    Else
        GRID.SortOrder = lvwAscending
    End If
        GRID.SortKey = ColumnHeader.index - 1
        GRID.Sorted = True
End Sub

Private Sub grid_DblClick()
    With GRID
        If GRID.ListItems.Count <> 0 Then
            If Me.cmd_Previo.Visible = False Then
                strBotonPresionado = "Aceptar"
            End If
            
            If Len(Trim(strBotonPresionado)) = 0 Then
                strBotonPresionado = "Aceptar"
            End If
            
            Hide
        Else
        
        End If
    End With
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Public Function Consulta_Mostrar() As Boolean
Dim rsConsulta As New ADODB.Recordset
Dim Item As ListItem
Dim lista As ListView
Dim oField As ADODB.Field
Set lista = GRID
Me.strBotonPresionado = ""
Call Desactivar_Objetos(False)
On Error GoTo GetError
    Screen.MousePointer = 11
     GRID.ListItems.Clear
     Debug.Print strSqlMasCondicion
     rsConsulta.Open strSqlMasCondicion, Connection, adOpenStatic, adLockReadOnly, adCmdText
     If Not rsConsulta.EOF Then
        rsConsulta.MoveFirst
        Do Until rsConsulta.EOF
           Set Item = lista.ListItems.Add(, , rsConsulta.Fields(0))
           DoEvents
           For A = 1 To rsConsulta.Fields.Count - 1
               If A < GRID.ColumnHeaders.Count Then
                       Item.SubItems(A) = rsConsulta.Fields(A)
               End If
           Next A
           If Me.strBotonPresionado = "Cancelar" Then
                Call Desactivar_Objetos(True)
                Screen.MousePointer = 0
                Exit Function
           End If
           rsConsulta.MoveNext
        Loop
        Call Desactivar_Objetos(True)
        lista.SetFocus
     End If
     rsConsulta.Close
 Screen.MousePointer = 0
 On Error GoTo 0
 Consulta_Mostrar = True
 Me.strBotonPresionado = ""
Call Desactivar_Objetos(True)

Exit Function

GetError:
    Call Desactivar_Objetos(True)
     Me.strBotonPresionado = ""
     Screen.MousePointer = 0
     MsgBox "Error N0." & Err.Number & " " & Err.Description
     Err.Clear
     Consulta_Mostrar = False

End Function

Sub Desactivar_Objetos(Tipo As Boolean)
    Me.Frame3.Enabled = Tipo
    Me.txtDato.Enabled = Tipo
    GRID.Enabled = Tipo
    GRID.Visible = Tipo
    Me.cmd_Buscar.Enabled = Tipo
End Sub
