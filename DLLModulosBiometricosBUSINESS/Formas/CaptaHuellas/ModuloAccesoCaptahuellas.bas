Attribute VB_Name = "ModuloCaptahuellas"
Global ADM_CaptaHuellas_Maneja                          As Boolean
Global ADM_CaptaHuellas_HayRegistros                    As Boolean
Global ADM_CaptaHuellas_IDDispositivo                   As String
Global ADM_CaptaHuellas_ModeloDispositivo               As String
Global ADM_CaptaHuellas_nVerificaciones                 As Integer
Global ADM_CaptaHuellas_nMaxErrores                     As Integer
Global ADM_CaptaHuellas_CriterioSQL                     As String
Global ADM_CaptaHuellas_LocalidadUsuarios               As String
Global ADM_CaptaHuellas_CodUsuarioActivo                As String
Global ADM_CaptaHuellas_DispositivoCompatible           As Boolean
Global ADM_CaptaHuellas_DemoKey                         As String

Global ADM_CaptaHuellas_TiempoEsperaPOSTVerificacion    As Long

Global ValidacionEnProceso                              As Boolean

Global FrmAppLink                                       As Object

Global Retorno As Boolean
Global Uno As Boolean
Global CancelarMensajeInfinito As Boolean
Global txtMensaje1 As String
Global txtMensaje2 As String

Public Function VerificarDispositivoCompatible() As Boolean
    
    Select Case ADM_CaptaHuellas_ModeloDispositivo
        
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "BioTrack", "BioTrack BioUsb"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "Suprema", "Suprema BioMini (SFR300)", "Suprema BioMini Slim (SFU-S20)", "Suprema BioMini Plus"
            
            VerificarDispositivoCompatible = True
            
        Case Is = "DEMO"
            
            'Dim TmpValidToken As String
            
            'TmpValidToken = "*" & Mid(Year(Now), 4, 1) & "d" & Right(Format(Month(Now), "00"), 1) & _
            "3M" & StrReverse(Format(Hour(Now), "00")) & "0" & Right(StrReverse(Format(Minute(Now), "00")), 1) & "k" & _
            IIf(Month(Now) >= 1 And Month(Now) <= 3, "A", IIf(Month(Now) >= 4 And Month(Now) <= 6, "B", _
            IIf(Month(Now) >= 7 And Month(Now) <= 9, "C", "D")))
            
            'If ModuloCaptahuellas.ADM_CaptaHuellas_DemoKey = TmpValidToken Then
                VerificarDispositivoCompatible = True
            'Else
                'VerificarDispositivoCompatible = False
            'End If
            
        Case Else
            
            VerificarDispositivoCompatible = False
            
            MsgBox "Disculpe, en estos momentos no existe compatibilidad para utilizar este modelo de dispositivo Capta Huellas."
            
    End Select
    
End Function

Public Function VerificarRegistros() As Boolean
    On Error Resume Next
    ADM_CaptaHuellas_HayRegistros = Not (ENT.BDD.Execute("SELECT TOP (1) * FROM TR_CAPTAHUELLAS_PERFILES").EOF)
End Function

Public Function ValidarAccesoCaptaHuellas() As Boolean
    
    Select Case ADM_CaptaHuellas_ModeloDispositivo
        
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasDPUAREUCFPR(ADM_CaptaHuellas_CriterioSQL)
            
        Case Is = "BioTrack", "BioTrack BioUsb"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasBioTrack(ADM_CaptaHuellas_CriterioSQL)
            
        Case Is = "Suprema", "Suprema BioMini (SFR300)", "Suprema BioMini Slim (SFU-S20)", "Suprema BioMini Plus"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasSuprema(ADM_CaptaHuellas_CriterioSQL)
            
        Case Is = "DEMO"
            
            ValidarAccesoCaptaHuellas = ValidarAccesoCaptaHuellasDEMO(ADM_CaptaHuellas_CriterioSQL)
            
        Case Else
            
            ValidarAccesoCaptaHuellas = False
            
    End Select
    
    If Not ValidarAccesoCaptaHuellas Then
        ADM_CaptaHuellas_CodUsuarioActivo = Empty
    End If
    
End Function

Private Function ValidarAccesoCaptaHuellasDPUAREUCFPR(Criterio As String) As Boolean
    
    ' Validacion para Modelos Digital Persona U.are.U 4000B/4500/Other Compatible FingerPrint Readers
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Object
    Dim PlantillasUsuario() As String
    
    ReDim Preserve PlantillasHuellas(0) As Object
    ReDim Preserve PlantillasUsuario(0) As String
    
    Dim TempObj As Object 'DPFPTemplate
    
    Debug.Print ADM_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open ADM_CaptaHuellas_CriterioSQL, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    If Not mRsValidarHuellas.EOF Then
        
        While Not mRsValidarHuellas.EOF
            
            ReDim Preserve PlantillasHuellas( _
            IIf(PlantillasHuellas(UBound(PlantillasHuellas)) Is Nothing, _
            0, UBound(PlantillasHuellas) + 1))
            
            'Set TempObj = New DPFPTemplate
            
            Set TempObj = CreateObject("DPFPShrX.DPFPTemplate")
            
            Blob = mRsValidarHuellas.Fields("bin_Data").GetChunk(mRsValidarHuellas.Fields("bin_Data").ActualSize)
            
            Call TempObj.Deserialize(CVar(Blob))
            
            Set PlantillasHuellas(UBound(PlantillasHuellas)) = TempObj
            
            ReDim Preserve PlantillasUsuario(UBound(PlantillasHuellas))
            
            PlantillasUsuario(UBound(PlantillasUsuario)) = mRsValidarHuellas!C_CODUSUARIO
            
            mRsValidarHuellas.MoveNext
            
        Wend
        
    Else
        
        ADM_CaptaHuellas_HayRegistros = False
        
        Mensaje True, "No ha registrado ninguna huella a�n, a continuaci�n se desactivar� el acceso por captahuellas hasta que registre las huellas de alg�n usuario."
        
        ValidarAccesoCaptaHuellasDPUAREUCFPR = False
        
        Exit Function
        
    End If
        
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCHDPUAUCFPR.Templates = PlantillasHuellas
    frm_VerificacionCHDPUAUCFPR.Usuarios = PlantillasUsuario
    
    For I = 1 To ADM_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCHDPUAUCFPR.Verificado = False
        frm_VerificacionCHDPUAUCFPR.Show vbModal
        
        Aceptado = frm_VerificacionCHDPUAUCFPR.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next I
    
    Set frm_VerificacionCHDPUAUCFPR = Nothing
    
    If nIntentosFallidos >= ADM_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasDPUAREUCFPR = False
    Else
        ValidarAccesoCaptaHuellasDPUAREUCFPR = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasDPUAREUCFPR = False
    
    Mensaje True, "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte t�cnico y reporte lo siguiente: " _
    & vbNewLine & vbNewLine & Err.Description
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasBioTrack(Criterio As String) As Boolean
    
    ' Validacion para Modelo BioTrack BioUsb (ZK7500)
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    
    Dim TempVar As Variant
    
    'Debug.Print ADM_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open ADM_CaptaHuellas_CriterioSQL, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    If Not mRsValidarHuellas.EOF Then
        
        While Not mRsValidarHuellas.EOF
            
            ReDim Preserve PlantillasHuellas(IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = "", 0, UBound(PlantillasHuellas) + 1))
            
            'TempVar = mRsValidarHuellas.Fields("bin_DataArray").value
            
            PlantillasHuellas(UBound(PlantillasHuellas)) = mRsValidarHuellas.Fields("bin_DataArray").value 'TempVar
            
            ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
            
            PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
            
            mRsValidarHuellas.MoveNext
            
        Wend
        
    Else
        
        ADM_CaptaHuellas_HayRegistros = False
        
        Mensaje True, "No ha registrado ninguna huella a�n, a continuaci�n se desactivar� el acceso por captahuellas hasta que registre las huellas de alg�n usuario."
        
        ValidarAccesoCaptaHuellasBioTrack = False
        
        Exit Function
        
    End If
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaBioTrackBioUsb.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaBioTrackBioUsb.Usuarios = PlantillasUsuarios
    
    For I = 1 To ADM_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaBioTrackBioUsb.Verificado = False
        frm_VerificacionCaptaHuellaBioTrackBioUsb.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaBioTrackBioUsb.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next I
    
    Set frm_VerificacionCaptaHuellaBioTrackBioUsb = Nothing
    
    If nIntentosFallidos >= ADM_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasBioTrack = False
    Else
        ValidarAccesoCaptaHuellasBioTrack = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasBioTrack = False
    
    Mensaje True, "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte t�cnico y reporte lo siguiente: " _
    & vbNewLine & vbNewLine & Err.Description
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasSuprema(Criterio As String) As Boolean
    
    ' Validacion para Modelos de la marca Suprema.
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    'Dim Blob() As Byte
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    Dim PlantillasTama�os() As Long
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    ReDim Preserve PlantillasTama�os(0) As Long
    
    Dim TempVar As Variant
    
    'Debug.Print ADM_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open ADM_CaptaHuellas_CriterioSQL, _
    ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    If Not mRsValidarHuellas.EOF Then
                
        While Not mRsValidarHuellas.EOF
            
            ReDim Preserve PlantillasHuellas( _
            IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = "", _
            0, UBound(PlantillasHuellas) + 1))
            
            TempVar = mRsValidarHuellas.Fields("bin_DataArray").value
            
            PlantillasHuellas(UBound(PlantillasHuellas)) = TempVar
            
            ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
            
            PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
            
            ReDim Preserve PlantillasTama�os(UBound(PlantillasHuellas))
            
            PlantillasTama�os(UBound(PlantillasTama�os)) = UBound(PlantillasHuellas(UBound(PlantillasHuellas))) + 1
            
            mRsValidarHuellas.MoveNext
            
        Wend
        
    Else
        
        ADM_CaptaHuellas_HayRegistros = False
        
        Mensaje True, "No ha registrado ninguna huella a�n, a continuaci�n se desactivar� el acceso por captahuellas hasta que registre las huellas de alg�n usuario."
        
        ValidarAccesoCaptaHuellasSuprema = False
        
        Exit Function
        
    End If
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaSuprema.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaSuprema.Usuarios = PlantillasUsuarios
    frm_VerificacionCaptaHuellaSuprema.Tama�os = PlantillasTama�os
    
    For I = 1 To ADM_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaSuprema.Verificado = False
        frm_VerificacionCaptaHuellaSuprema.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaSuprema.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next I
    
    Set frm_VerificacionCaptaHuellaSuprema = Nothing
    
    If nIntentosFallidos >= ADM_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasSuprema = False
    Else
        ValidarAccesoCaptaHuellasSuprema = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    ValidarAccesoCaptaHuellasSuprema = False
    
    Debug.Print Err.Description
    
    Mensaje True, "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte t�cnico y reporte lo siguiente: " _
    & vbNewLine & vbNewLine & Err.Description
    
    GoTo Finally
    
End Function

Private Function ValidarAccesoCaptaHuellasDEMO(Criterio As String) As Boolean
    
    ' Validacion DEMO
    
    On Error GoTo ErrorCaptaHuellas
    
    Dim mRsValidarHuellas As New ADODB.Recordset
    
    Dim nIntentosAceptados As Integer, nIntentosFallidos As Integer
    
    Dim Aceptado As Boolean
    
    nIntentosAceptados = 0
    nIntentosFallidos = 0
    Aceptado = False
    
    Dim PlantillasHuellas() As Variant
    Dim PlantillasUsuarios() As String
    
    ReDim Preserve PlantillasHuellas(0) As Variant
    ReDim Preserve PlantillasUsuarios(0) As String
    
    Dim TempVar As Variant
    
    'Debug.Print ADM_CaptaHuellas_CriterioSQL
    
    mRsValidarHuellas.Open ADM_CaptaHuellas_CriterioSQL, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    If Not mRsValidarHuellas.EOF Then
        
        While Not mRsValidarHuellas.EOF
            
            ReDim Preserve PlantillasHuellas( _
            IIf(PlantillasUsuarios(UBound(PlantillasUsuarios)) = Empty, _
            0, UBound(PlantillasHuellas) + 1))
            
            'TempVar = mRsValidarHuellas.Fields("bin_DataArray").value
            
            PlantillasHuellas(UBound(PlantillasHuellas)) = mRsValidarHuellas.Fields("bin_DataArray").value 'TempVar
            
            ReDim Preserve PlantillasUsuarios(UBound(PlantillasHuellas))
            
            PlantillasUsuarios(UBound(PlantillasUsuarios)) = mRsValidarHuellas!C_CODUSUARIO
            
            mRsValidarHuellas.MoveNext
            
        Wend
        
    Else
        
        ADM_CaptaHuellas_HayRegistros = False
        
        Mensaje True, "No ha registrado ninguna huella a�n, a continuaci�n se desactivar� el acceso por captahuellas hasta que registre las huellas de alg�n usuario."
        
        ValidarAccesoCaptaHuellasDEMO = False
        
        Exit Function
        
    End If
    
    mRsValidarHuellas.Close
    
    ValidacionEnProceso = True
    
    frm_VerificacionCaptaHuellaDEMO.Templates = PlantillasHuellas
    frm_VerificacionCaptaHuellaDEMO.Usuarios = PlantillasUsuarios
    
    For I = 1 To ADM_CaptaHuellas_nVerificaciones
        
        frm_VerificacionCaptaHuellaDEMO.Verificado = False
        frm_VerificacionCaptaHuellaDEMO.Show vbModal
        
        Aceptado = frm_VerificacionCaptaHuellaDEMO.Verificado
        
        If Aceptado Then
            nIntentosAceptados = nIntentosAceptados + 1
        Else
            nIntentosFallidos = nIntentosFallidos + 1
        End If
        
    Next I
    
    Set frm_VerificacionCaptaHuellaDEMO = Nothing
    
    If nIntentosFallidos >= ADM_CaptaHuellas_nMaxErrores Then
        ValidarAccesoCaptaHuellasDEMO = False
    Else
        ValidarAccesoCaptaHuellasDEMO = True
    End If
    
Finally:
    
    ValidacionEnProceso = False
    
    Exit Function
    
ErrorCaptaHuellas:
    
    Debug.Print Err.Description
    
    ValidarAccesoCaptaHuellasDEMO = False
    
    Mensaje True, "Ha ocurrido un error durante el proceso de reconocimiento" & vbNewLine & _
    "Contacte al departamento de soporte t�cnico y reporte lo siguiente: " _
    & vbNewLine & vbNewLine & Err.Description
    
    GoTo Finally
    
End Function
