VERSION 5.00
Begin VB.Form DPFP_UAUConfig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de CaptaHuellas"
   ClientHeight    =   5250
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7440
   Icon            =   "Enrollment.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   7440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Close 
      Caption         =   "Close"
      Height          =   375
      Left            =   5760
      TabIndex        =   0
      Top             =   4800
      Width           =   1455
   End
End
Attribute VB_Name = "DPFP_UAUConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents DPFPEnrollmentCtrl As VBControlExtender
Attribute DPFPEnrollmentCtrl.VB_VarHelpID = -1
Private InnerObj As Object

Public Enum CH_Dedos

    Me�iqueIzquierdo = 1
    AnularIzquierdo = 2
    MedioIzquierdo = 4
    IndiceIzquierdo = 8
    PulgarIzquierdo = 16
    
    PulgarDerecho = 32
    IndiceDerecho = 64
    MedioDerecho = 128
    AnularDerecho = 256
    Me�iqueDerecho = 512

End Enum

Private Sub DPFPEnrollmentCtrl_ObjectEvent(Info As EventInfo)
    
    With Info.EventParameters
        
        Select Case Info.Name
            Case "OnDelete"
                Call DPFPEnrollmentCtrl_OnDelete( _
                .Item("lFingerMask").value, .Item("pStatus").value)
            Case "OnEnroll"
                Call DPFPEnrollmentCtrl_OnEnroll( _
                .Item("lFingerMask").value, .Item("pTemplate").value, .Item("pStatus").value)
            Case Else ' Unknown Event
                ' Handle unknown events here.
        End Select
        
    End With
    
End Sub

Private Sub Close_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    
    If DPFPEnrollmentCtrl Is Nothing Then
        Mensaje True, "El componente RTE de Digital Persona no est� correctamente instalado. " & _
        "Por favor reinstale dicho componente antes de poder continuar."
        Unload Me
        Exit Sub
    End If
    
    'Set properties to DPFPEnrollment object.
    
    On Error Resume Next
    
    InnerObj.MaxEnrollFingerCount = CInt(ficha_PerfilesCaptaHuellas.txt_LimiteHuellas)
    
    InnerObj.EnrolledFingersMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    If Err.Number <> 0 Then
       Mensaje True, "La configuraci�n de huellas no es correcta, por favor verifique." + Err.Description
       ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(0)
       Unload Me
    End If
 
End Sub

Private Sub Form_Load()
    
    On Error Resume Next
    
    Set DPFPEnrollmentCtrl = Me.Controls.Add("DPFPActX.DPFPEnrollmentControl.1", "DPFPEnrollmentCtrl", Me)
    
    If Err.Number <> 0 Then
        Exit Sub
    End If
    
    With DPFPEnrollmentCtrl
        
        .CausesValidation = True
        .Left = 0
        .Top = 0
        .Width = 7455
        .Height = 4695
        .TabStop = False
        .Visible = True
        
    End With
    
    Set InnerObj = DPFPEnrollmentCtrl.Object
    InnerObj.ReaderSerialNumber = "{00000000-0000-0000-0000-000000000000}"
    ' Mejor dejar dicho valor como esta por defecto. Esta instrucci�n hace que se tarde
    ' mas la aplicaci�n durante unos segundos, a la hora de inicializar.
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    'Show new fingerprint mask.
    
    On Error Resume Next
    
    ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(InnerObj.EnrolledFingersMask)
    
    'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & _
    CInt(InnerObj.EnrolledFingersMask) & "WHERE c_CodUsuario = '" & _
    ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND ID_DISPOSITIVO = '" & _
    ficha_PerfilesCaptaHuellas.txt_Codigo & "'"

End Sub

Private Sub DPFPEnrollmentCtrl_OnEnroll(ByVal Mask As Long, ByVal Templ As Object, ByVal Stat As Object)
    
    On Error GoTo ErrorEnroll
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Ingreso_Enrollment, CurrentMask, CInt(InnerObj.EnrolledFingersMask))
    
    If ActualizarPerfilUsuario(Ingreso_Enrollment, Diferencia, Templ) Then
        ActualizarPlantillaUsuario (CInt(InnerObj.EnrolledFingersMask))
        Exit Sub
    End If
    
ErrorEnroll:
    
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    
End Sub

Private Sub ActualizarPlantillaUsuario(ByVal Valor As Integer)
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS " & _
    "WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    mRs.Open mSql, ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not mRs.EOF Then
        
        mRs.Update
        
        mRs!NU_PLANTILLA = Valor
        
        mRs.UpdateBatch
        
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS", mRs, _
        ENT.BDD, ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
        
        mRs.Close
        
    End If
    
    'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & valor & " WHERE " & _
    '"ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' AND C_CODUSUARIO = '" & _
    'ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND c_CodLocalidad = '" & Sucursal & "'"
    
    ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(Valor)
    
End Sub

Private Function ActualizarPerfilUsuario(TipoOperacion As TipoOperacion_CH, MaskUpdate As Integer, _
Optional CHData As Object) As Boolean
    
    On Error GoTo ErrorOperacion
    
    Dim ID_Dedo As String
    Dim RsOperacion As New ADODB.Recordset
    Dim mSql As String
    Dim Data() As Byte
    Dim MiConexion As New ADODB.Connection
    Dim CadConexion As String
    
    MiConexion.ConnectionString = ENT.BDD.ConnectionString
    
    MiConexion.Open
    
    ActualizarPerfilUsuario = True
    
    Select Case MaskUpdate
        Case CH_Dedos.Me�iqueIzquierdo
            ID_Dedo = "Me�ique Izquierdo"
        Case CH_Dedos.AnularIzquierdo
            ID_Dedo = "Anular Izquierdo"
        Case CH_Dedos.MedioIzquierdo
            ID_Dedo = "Medio Izquierdo"
        Case CH_Dedos.IndiceIzquierdo
            ID_Dedo = "Indice Izquierdo"
        Case CH_Dedos.PulgarIzquierdo
            ID_Dedo = "Pulgar Izquierdo"
        Case CH_Dedos.PulgarDerecho
            ID_Dedo = "Pulgar Derecho"
        Case CH_Dedos.IndiceDerecho
            ID_Dedo = "Indice Derecho"
        Case CH_Dedos.MedioDerecho
            ID_Dedo = "Medio Derecho"
        Case CH_Dedos.AnularDerecho
            ID_Dedo = "Anular Derecho"
        Case CH_Dedos.Me�iqueDerecho
            ID_Dedo = "Me�ique Derecho"
        Case Else
            ID_Dedo = "Desconocido"
    End Select
    
    If ID_Dedo = "Desconocido" Then
        
        Mensaje True, "La huella digital no se ha podido grabar." & vbNewLine & _
        "El reconocimiento ha sido invalido." & vbNewLine & _
        "Intente con otra posici�n."
        
        ActualizarPerfilUsuario = False
    
        Exit Function
        
    End If
    
    If (Not CHData Is Nothing) Then
        Data = CHData.Serialize
    End If
    
    mSql = "SELECT * FROM TR_CAPTAHUELLAS_PERFILES " & _
    "WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND C_DEDO = '" & ID_Dedo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    RsOperacion.Open mSql, MiConexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            If RsOperacion.EOF Then
                
                RsOperacion.AddNew
                
                RsOperacion!ID_Dispositivo = ficha_PerfilesCaptaHuellas.txt_Codigo
                RsOperacion!C_CODUSUARIO = ficha_PerfilesCaptaHuellas.txt_CodUsuario
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_CodLocalidad = ficha_PerfilesCaptaHuellas.SucursalUsuario
                'RsOperacion!BIN_DATA.AppendChunk (Data)
                RsOperacion!BIN_DATA = Data
                RsOperacion.UpdateBatch
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
                
            End If
            
        Case TipoOperacion_CH.Borrado_Deletion
            
            If Not RsOperacion.EOF Then
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 10)), True
                
                RsOperacion.MoveFirst
                RsOperacion.Delete
                RsOperacion.UpdateBatch
                
            End If
            
    End Select
    
    RsOperacion.Close
    MiConexion.Close
    
    ActualizarPerfilUsuario = True
    
    Exit Function
    
ErrorOperacion:
    
    ActualizarPerfilUsuario = False
    
    Debug.Print Err.Description
    Debug.Print Data
    
End Function

Private Function DiferenciaBytes(TipoOperacion As TipoOperacion_CH, _
CurrentMask As Integer, NewMask As Integer) As Integer
    
    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            DiferenciaBytes = (NewMask - CurrentMask)
            
        Case TipoOperacion_CH.Borrado_Deletion
            
            DiferenciaBytes = (CurrentMask - NewMask)
            
    End Select
    
End Function

Private Function GetCurrentUserMask() As Integer
    
    On Error GoTo Error
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT nu_Plantilla FROM MA_CAPTAHUELLAS_USUARIOS " & _
    "WHERE c_CodUsuario = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    mRs.Open mSql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        GetCurrentUserMask = mRs!NU_PLANTILLA
    Else
        GetCurrentUserMask = 0
    End If
    
    mRs.Close
    
    Exit Function
    
Error:
    
    GetCurrentUserMask = 0
    
End Function

Private Sub DPFPEnrollmentCtrl_OnDelete(ByVal Mask As Long, ByVal Stat As Object)
    
    On Error GoTo ErrorDelete
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Borrado_Deletion, CurrentMask, CInt(InnerObj.EnrolledFingersMask))
    
    If ActualizarPerfilUsuario(Borrado_Deletion, Diferencia) Then
        ActualizarPlantillaUsuario (CInt(InnerObj.EnrolledFingersMask))
        Exit Sub
    End If
    
ErrorDelete:
    
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    
End Sub
