VERSION 5.00
Object = "{D95CB779-00CB-4B49-97B9-9F0B61CAB3C1}#4.0#0"; "biokey.ocx"
Begin VB.Form BioTrack_BioUsb_Config 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de CaptaHuellas"
   ClientHeight    =   5460
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5835
   Icon            =   "BioTrackEnrollment.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin ZKFPEngXControl.ZKFPEngX ZKEngine 
      Left            =   3840
      Top             =   4920
      EnrollCount     =   3
      SensorIndex     =   0
      Threshold       =   10
      VerTplFileName  =   ""
      RegTplFileName  =   ""
      OneToOneThreshold=   10
      Active          =   0   'False
      IsRegister      =   0   'False
      EnrollIndex     =   0
      SensorSN        =   ""
      FPEngineVersion =   "9"
      ImageWidth      =   0
      ImageHeight     =   0
      SensorCount     =   0
      TemplateLen     =   1152
      EngineValid     =   0   'False
      ForceSecondMatch=   0   'False
      IsReturnNoLic   =   -1  'True
      LowestQuality   =   30
      FakeFunOn       =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "    Mano Izquierda"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoMe�iqueIzquierdo 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularIzquierdo 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioIzquierdo 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceIzquierdo 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoPulgarIzquierdo 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   360
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.Frame FrameManoDerecha 
      Caption         =   "     Mano Derecha"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoPulgarDerecho 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2760
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceDerecho 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioDerecho 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularDerecho 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMe�iqueDerecho 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   360
         Width           =   1935
      End
      Begin VB.Line LineRellenoFrameIzquierdo 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   375
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.CommandButton Close 
      BackColor       =   &H80000003&
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      MaskColor       =   &H80000003&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4920
      Width           =   1455
   End
   Begin VB.Label lblAccionDispositivo 
      BackStyle       =   0  'Transparent
      Caption         =   "Accion del Dispositivo: Listo para Detectar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   14
      Top             =   4440
      Width           =   5115
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblStatus 
      BackStyle       =   0  'Transparent
      Caption         =   "Registrando..."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   13
      Top             =   3960
      Width           =   5115
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "BioTrack_BioUsb_Config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Public Enum BioTrack_CH_Dedos
'
'Me�iqueIzquierdo = 1
'AnularIzquierdo = 2
'MedioIzquierdo = 4
'IndiceIzquierdo = 8
'PulgarIzquierdo = 16
'
'PulgarDerecho = 32
'IndiceDerecho = 64
'MedioDerecho = 128
'AnularDerecho = 256
'Me�iqueDerecho = 512
'
'End Enum

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public MaxEnrollFingerCount As Integer
Public EnrolledFingersMask As Integer
Public CurrentEnrollFingerMask As Integer

Public RegistrandoMe�iqueIzquierdo As Boolean, BorrandoMe�iqueIzquierdo As Boolean
Public RegistrandoAnularIzquierdo As Boolean, BorrandoAnularIzquierdo As Boolean
Public RegistrandoMedioIzquierdo As Boolean, BorrandoMedioIzquierdo As Boolean
Public RegistrandoIndiceIzquierdo As Boolean, BorrandoIndiceIzquierdo As Boolean
Public RegistrandoPulgarIzquierdo As Boolean, BorrandoPulgarIzquierdo As Boolean

Public RegistrandoMe�iqueDerecho As Boolean, BorrandoMe�iqueDerecho As Boolean
Public RegistrandoAnularDerecho As Boolean, BorrandoAnularDerecho As Boolean
Public RegistrandoMedioDerecho As Boolean, BorrandoMedioDerecho As Boolean
Public RegistrandoIndiceDerecho As Boolean, BorrandoIndiceDerecho As Boolean
Public RegistrandoPulgarDerecho As Boolean, BorrandoPulgarDerecho As Boolean

Public Cargando As Boolean
Public EvitarActivate As Boolean

Private UniTemplateFull() As Byte
Private UniTemplatePart() As Byte
Private UniTemplateSize As Long
Private Const MaxTemplateSize = 1024
Private FileNumber As Long

Private Sub Close_Click()
    
    'On Error GoTo Error
    
    'Me.ZKEngine.CancelEnroll
    'Me.ZKEngine.EndEngine
    Unload Me
    'Set BioTrack_BioUsb_Config = Nothing
    
'Error:
    
    ''Error del SDK.
    'Resume Next
    
End Sub

Private Sub cmdDedoAnularDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularDerecho.value = vbChecked And Not RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'Para que no haga nada
        BorrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbChecked And RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoAnularDerecho = False
        Exit Sub
    End If
    
    If cmdDedoAnularDerecho.value = vbUnchecked And Not BorrandoAnularDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'para que no haga nada
        RegistrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbUnchecked And BorrandoAnularDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoAnularDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoAnularIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularIzquierdo.value = vbChecked And Not RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'Para que no haga nada
        BorrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbChecked And RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoAnularIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoAnularIzquierdo.value = vbUnchecked And Not BorrandoAnularIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'para que no haga nada
        RegistrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbUnchecked And BorrandoAnularIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoAnularIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoIndiceDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceDerecho.value = vbChecked And Not RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'Para que no haga nada
        BorrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbChecked And RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoIndiceDerecho = False
        Exit Sub
    End If
    
    If cmdDedoIndiceDerecho.value = vbUnchecked And Not BorrandoIndiceDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'para que no haga nada
        RegistrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbUnchecked And BorrandoIndiceDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoIndiceDerecho = False
        Exit Sub
    End If
    
End Sub

Private Sub cmdDedoIndiceIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceIzquierdo.value = vbChecked And Not RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'Para que no haga nada
        BorrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbChecked And RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoIndiceIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoIndiceIzquierdo.value = vbUnchecked And Not BorrandoIndiceIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'para que no haga nada
        RegistrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbUnchecked And BorrandoIndiceIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoIndiceIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioDerecho.value = vbChecked And Not RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'Para que no haga nada
        BorrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbChecked And RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMedioDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMedioDerecho.value = vbUnchecked And Not BorrandoMedioDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'para que no haga nada
        RegistrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbUnchecked And BorrandoMedioDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMedioDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioIzquierdo.value = vbChecked And Not RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'Para que no haga nada
        BorrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbChecked And RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMedioIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMedioIzquierdo.value = vbUnchecked And Not BorrandoMedioIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'para que no haga nada
        RegistrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbUnchecked And BorrandoMedioIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMedioIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMe�iqueDerecho.value = vbChecked And Not RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'Para que no haga nada
        BorrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbChecked And RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMe�iqueDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueDerecho.value = vbUnchecked And Not BorrandoMe�iqueDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'para que no haga nada
        RegistrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbUnchecked And BorrandoMe�iqueDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMe�iqueDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMe�iqueIzquierdo.value = vbChecked And Not RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'Para que no haga nada
        BorrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbChecked And RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMe�iqueIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueIzquierdo.value = vbUnchecked And Not BorrandoMe�iqueIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'para que no haga nada
        RegistrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbUnchecked And BorrandoMe�iqueIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMe�iqueIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarDerecho_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoPulgarDerecho.value = vbChecked And Not RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'Para que no haga nada
        BorrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbChecked And RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoPulgarDerecho = False
        Exit Sub
    End If
    
    If cmdDedoPulgarDerecho.value = vbUnchecked And Not BorrandoPulgarDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'para que no haga nada
        RegistrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbUnchecked And BorrandoPulgarDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoPulgarDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoPulgarIzquierdo.value = vbChecked And Not RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        ZKEngine.BeginEnroll
        CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'Para que no haga nada
        BorrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbChecked And RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoPulgarIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoPulgarIzquierdo.value = vbUnchecked And Not BorrandoPulgarIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'para que no haga nada
        RegistrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbChecked
        ZKEngine_Delete (CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbUnchecked And BorrandoPulgarIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoPulgarIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub Form_Activate()

    ' Set properties to ZKEngine object.
     
    If EvitarActivate Then EvitarActivate = False: Exit Sub
     
    On Error GoTo Error
     
    ZKEngine.LowestQuality = 60
    
    ZKEngine.EnrollCount = CInt(ficha_PerfilesCaptaHuellas.txt_Muestreos)
    
    ZKEngine.FPEngineVersion = "10"
    
    Dim RetVal As Long
    
    RetVal = ZKEngine.InitEngine
    
    If RetVal <> 0 Then
        IgnorarActivate
        Mensaje True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificaci�n."
        Unload Me
        Set BioTrack_BioUsb_Config = Nothing
        Exit Sub
    End If
    
    Select Case 2
        'Case 0 'Formato Propietario del Captahuellas.
            'No hay que hacer nada, se aplica por defecto.
        'Case 1 'Formato Est�ndar ANSI 378
            'If Not CBool(ZKEngine.SetTemplateFormat(0)) Then 'Manejo de Est�ndares
                'mensaje True, "Existe un problema al definir el formato de est�ndares. Por favor contacte a Soporte T�cnico."
                'If ModoReconocimiento Then Cerrar = False: ModoReconocimiento_Respuesta = "Error de inicializacion"
                'Unload Me
                'Exit Sub
            'End If
        Case 1 'Formato Est�ndar ANSI 378
            If Not CBool(ZKEngine.SetTemplateFormat(0)) Then 'Manejo de Est�ndares
                'IgnorarActivate
                'Mensaje True, "Existe un problema al definir el formato de est�ndares. Por favor contacte a Soporte T�cnico."
                'Unload Me
                'Exit Sub
            End If
        Case 2 'Formato Est�ndar ISO 19794-2
            If Not CBool(ZKEngine.SetTemplateFormat(1)) Then 'Manejo de Est�ndares
                'IgnorarActivate
                'Mensaje True, "Existe un problema al definir el formato de est�ndares. Por favor contacte a Soporte T�cnico."
                'Unload Me
                'Exit Sub
            End If
    End Select
    
    ZKEngine.VerTplFileName = App.path & "\HuellaRegistro.Tpl"
    
    MaxEnrollFingerCount = CInt(ficha_PerfilesCaptaHuellas.txt_LimiteHuellas)
    
    EnrolledFingersMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    'Marcar los que Apliquen.
    
    SetUpMask
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    IgnorarActivate
    
    Mensaje True, "Existe un problema con el dispositivo, " & _
    "no se pudo iniciar el motor de verificaci�n. " & _
    mErrorDesc & " (" & mErrorNumber & ")"
    
    Unload Me
    
    Set BioTrack_BioUsb_Config = Nothing
    
End Sub

Private Sub SetUpMask()
    
    Dim Mask As Integer
    Mask = EnrolledFingersMask
    
    'With CH_Dedos
        
        If Mask - CH_Dedos.Me�iqueDerecho >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueDerecho
            Cargando = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularDerecho >= 0 Then
            Mask = Mask - CH_Dedos.AnularDerecho
            Cargando = True
            cmdDedoAnularDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioDerecho >= 0 Then
            Mask = Mask - CH_Dedos.MedioDerecho
            Cargando = True
            cmdDedoMedioDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceDerecho >= 0 Then
            Mask = Mask - CH_Dedos.IndiceDerecho
            Cargando = True
            cmdDedoIndiceDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.PulgarDerecho >= 0 Then
            Mask = Mask - CH_Dedos.PulgarDerecho
            Cargando = True
            cmdDedoPulgarDerecho.value = vbChecked
        End If
        
        '-------------------------------------------'
        
        If Mask - CH_Dedos.PulgarIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.PulgarIzquierdo
            Cargando = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.IndiceIzquierdo
            Cargando = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.MedioIzquierdo
            Cargando = True
            cmdDedoMedioIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.AnularIzquierdo
            Cargando = True
            cmdDedoAnularIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.Me�iqueIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueIzquierdo
            Cargando = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        End If
    
    'End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    On Error GoTo Error
    
     ' Show new fingerprint mask.
    ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(EnrolledFingersMask)
    
    'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & _
    CInt(DPFPEnrollmentCtrl.EnrolledFingersMask) & "WHERE c_CodUsuario = '" & _
    ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND ID_DISPOSITIVO = '" & _
    ficha_PerfilesCaptaHuellas.txt_Codigo & "'"
    
    Me.ZKEngine.CancelEnroll
    Me.ZKEngine.EndEngine
    
    Set BioTrack_BioUsb_Config = Nothing
    
    Exit Sub
    
Error:
    
    'Error del SDK.
    Resume Next
    
End Sub

Private Sub ZKEngine_OnEnroll(ByVal ActionResult As Boolean, ByVal ATemplate As Variant)
    
    On Error GoTo ErrorEnroll
    
    If ActionResult = False Then
        DoEvents
        lblStatus.Caption = "El Registro ha fallado."
        lblAccionDispositivo.Caption = "Por favor espere..."
        DoEvents
        Sleep 2500: ResetearStatus
        Exit Sub
    End If
    
    Dim TmpArchivo As String: TmpArchivo = App.path & "\HuellaRegistro.Tpl"
    
    RellenarTemplate
    
    If ZKEngine.SaveTemplate(TmpArchivo, UniTemplatePart) Then
        
        Dim TmpHuella As String
        
        TmpHuella = LoadFile(TmpArchivo)            'Funci�n Marynel-Proof
        
    Else
    
        IgnorarActivate
        Mensaje True, "No se ha podido obtener la huella. Por favor intente nuevamente."
        ResetearStatus
        Exit Sub
        
    End If
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Ingreso_Enrollment, CurrentMask, EnrolledFingersMask)
    
    EnrolledFingersMask = EnrolledFingersMask + CurrentEnrollFingerMask
    
    If ActualizarPerfilUsuario(Ingreso_Enrollment, CurrentEnrollFingerMask, TmpHuella) Then
        Call ActualizarPlantillaUsuario(EnrolledFingersMask)
        'Exit Sub
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            RegistrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        Case CH_Dedos.AnularIzquierdo
            RegistrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbChecked
        Case CH_Dedos.MedioIzquierdo
            RegistrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbChecked
        Case CH_Dedos.IndiceIzquierdo
            RegistrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarIzquierdo
            RegistrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarDerecho
            RegistrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbChecked
        Case CH_Dedos.IndiceDerecho
            RegistrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbChecked
        Case CH_Dedos.MedioDerecho
            RegistrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbChecked
        Case CH_Dedos.AnularDerecho
            RegistrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbChecked
        Case CH_Dedos.Me�iqueDerecho
            RegistrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    CurrentEnrollFingerMask = 0
    
    Exit Sub
    
ErrorEnroll:
    
    'Debug.Print Err.Description
    
    IgnorarActivate
    KillShot TmpArchivo
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    ResetearStatus
    
End Sub

Private Sub ActualizarPlantillaUsuario(Valor As Integer)

    Dim mRs As New ADODB.Recordset

    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "'" & _
    " AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "'" & _
    " AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "'"
    
    mRs.Open mSql, ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs.Update
        mRs!NU_PLANTILLA = Valor
        mRs.UpdateBatch
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS", mRs, ENT.BDD, ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
        mRs.Close
    End If

   'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & valor & " WHERE " & _
   '"ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' AND C_CODUSUARIO = '" & _
   'ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND c_CodLocalidad = '" & Sucursal & "'"
   
   ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(Valor)

End Sub

Private Function ActualizarPerfilUsuario( _
TipoOperacion As TipoOperacion_CH, _
MaskUpdate As Integer, _
Optional CHData As Variant) As Boolean
    
    'On Error GoTo ErrorOperacion
    
    Dim ID_Dedo As String
    Dim RsOperacion As New ADODB.Recordset
    Dim mSql As String
    Dim Data As Variant
    Dim MiConexion As New ADODB.Connection
    Dim CadConexion As String
    
    MiConexion.ConnectionString = ENT.BDD.ConnectionString
    
    MiConexion.Open
    
    ActualizarPerfilUsuario = True
    
    Select Case MaskUpdate
        Case CH_Dedos.Me�iqueIzquierdo
            ID_Dedo = "Me�ique Izquierdo"
        Case CH_Dedos.AnularIzquierdo
            ID_Dedo = "Anular Izquierdo"
        Case CH_Dedos.MedioIzquierdo
            ID_Dedo = "Medio Izquierdo"
        Case CH_Dedos.IndiceIzquierdo
            ID_Dedo = "Indice Izquierdo"
        Case CH_Dedos.PulgarIzquierdo
            ID_Dedo = "Pulgar Izquierdo"
        Case CH_Dedos.PulgarDerecho
            ID_Dedo = "Pulgar Derecho"
        Case CH_Dedos.IndiceDerecho
            ID_Dedo = "Indice Derecho"
        Case CH_Dedos.MedioDerecho
            ID_Dedo = "Medio Derecho"
        Case CH_Dedos.AnularDerecho
            ID_Dedo = "Anular Derecho"
        Case CH_Dedos.Me�iqueDerecho
            ID_Dedo = "Me�ique Derecho"
        Case Else
            ID_Dedo = "Desconocido"
    End Select
    
    If ID_Dedo = "Desconocido" Then
        
        IgnorarActivate
        
        Mensaje True, "La huella digital no se ha podido grabar." & vbNewLine & _
        "El reconocimiento ha sido invalido." & vbNewLine & _
        "Intente con otra posici�n."
        
        ActualizarPerfilUsuario = False
        
        Exit Function
        
    End If
    
    If Not IsMissing(CHData) Then
        Data = CHData
    End If
    
    mSql = _
    "SELECT * FROM TR_CAPTAHUELLAS_PERFILES " & _
    "WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND C_DEDO = '" & ID_Dedo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    RsOperacion.Open mSql, MiConexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            If RsOperacion.EOF Then
                
                RsOperacion.AddNew
                RsOperacion!ID_Dispositivo = ficha_PerfilesCaptaHuellas.txt_Codigo
                RsOperacion!C_CODUSUARIO = ficha_PerfilesCaptaHuellas.txt_CodUsuario
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_CodLocalidad = ficha_PerfilesCaptaHuellas.SucursalUsuario
                RsOperacion!BIN_DATA = UniTemplatePart 'Data
                RsOperacion!bin_DataArray = UniTemplatePart
                RsOperacion.UpdateBatch
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
                
            Else
                
                RsOperacion.Update
                RsOperacion!ID_Dispositivo = ficha_PerfilesCaptaHuellas.txt_Codigo
                RsOperacion!C_CODUSUARIO = ficha_PerfilesCaptaHuellas.txt_CodUsuario
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_CodLocalidad = ficha_PerfilesCaptaHuellas.SucursalUsuario
                RsOperacion!BIN_DATA = UniTemplatePart 'Data
                RsOperacion!bin_DataArray = UniTemplatePart
                RsOperacion.UpdateBatch
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
                
            End If
            
        Case TipoOperacion_CH.Borrado_Deletion
            
            If Not RsOperacion.EOF Then
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 10)), True
                RsOperacion.MoveFirst
                RsOperacion.Delete
                RsOperacion.UpdateBatch
                
            End If
            
    End Select
    
    RsOperacion.Close
    MiConexion.Close
    
    ActualizarPerfilUsuario = True
    
    Exit Function
    
ErrorOperacion:
    
    ActualizarPerfilUsuario = False
    Debug.Print Err.Description
    Debug.Print Data
    
End Function

Private Function DiferenciaBytes(TipoOperacion As TipoOperacion_CH, CurrentMask As Integer, NewMask As Integer) As Integer

    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            DiferenciaBytes = NewMask - CurrentMask
            
        Case TipoOperacion_CH.Borrado_Deletion
    
            DiferenciaBytes = CurrentMask - NewMask
    
    End Select

End Function

Private Function GetCurrentUserMask() As Integer
    
    On Error GoTo Error
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT nu_Plantilla FROM MA_CAPTAHUELLAS_USUARIOS " & _
    "WHERE c_CodUsuario = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    mRs.Open mSql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        GetCurrentUserMask = mRs!NU_PLANTILLA
    Else
        GetCurrentUserMask = 0
    End If
    
    mRs.Close
    
    Exit Function
    
Error:
    
    GetCurrentUserMask = 0
    
End Function

Private Sub ZKEngine_Delete(ByVal Template As Variant)
    
    On Error GoTo ErrorDelete
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Borrado_Deletion, CurrentMask, CurrentEnrollFingerMask)
    
    If ActualizarPerfilUsuario(Borrado_Deletion, CurrentEnrollFingerMask) Then
        ActualizarPlantillaUsuario (Diferencia)
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            BorrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        Case CH_Dedos.AnularIzquierdo
            BorrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbUnchecked
        Case CH_Dedos.MedioIzquierdo
            BorrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbUnchecked
        Case CH_Dedos.IndiceIzquierdo
            BorrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarIzquierdo
            BorrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarDerecho
            BorrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbUnchecked
        Case CH_Dedos.IndiceDerecho
            BorrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbUnchecked
        Case CH_Dedos.MedioDerecho
            BorrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbUnchecked
        Case CH_Dedos.AnularDerecho
            BorrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbUnchecked
        Case CH_Dedos.Me�iqueDerecho
            BorrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbUnchecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    EnrolledFingersMask = EnrolledFingersMask - CurrentEnrollFingerMask
    
    CurrentEnrollFingerMask = 0
    
    Exit Sub
    
ErrorDelete:
    
    IgnorarActivate
    
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    
End Sub

Private Sub ZKEngine_OnFeatureInfo(ByVal AQuality As Long)
    
    If ZKEngine.isRegister Then
        lblAccionDispositivo.Caption = "Calidad: " & ZKEngine.LastQuality & ". Intento " & IIf(AQuality <> 0, "Fallido", "Acertado") & ". Prosiga."
        lblStatus.Caption = "Registrando... Aciertos Restantes: " & ZKEngine.EnrollIndex - 1
    End If

End Sub

Private Sub ZKEngine_OnFingerLeaving()
    'Me.lblAccionDispositivo.Caption = "Detecci�n Finalizada."
End Sub

Private Sub ZKEngine_OnFingerTouching()
    Me.lblAccionDispositivo.Caption = "Detectando Huella..."
End Sub

Private Sub RellenarTemplate()
    UniTemplatePart = ZKEngine.GetTemplate
    UniTemplateSize = UBound(UniTemplatePart) + 1
    ReDim UniTemplateFull(MaxTemplateSize - 1) As Byte
    Dim I As Long
    For I = 0 To UniTemplateSize - 1
        UniTemplateFull(I) = UniTemplatePart(I)
    Next I
End Sub

Private Function IgnorarActivate() As Boolean
    IgnorarActivate = True: EvitarActivate = True
End Function

Private Sub ResetearStatus()
    lblStatus.Caption = "Pulse un bot�n disponible para comenzar."
    lblAccionDispositivo.Caption = "Accion del Dispositivo: Listo para Detectar"
End Sub
