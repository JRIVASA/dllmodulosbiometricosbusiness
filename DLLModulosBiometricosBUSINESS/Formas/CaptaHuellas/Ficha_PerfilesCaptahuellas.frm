VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form ficha_PerfilesCaptaHuellas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ficha de Perfiles de CaptaHuellas"
   ClientHeight    =   6225
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8895
   Icon            =   "Ficha_PerfilesCaptahuellas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6225
   ScaleWidth      =   8895
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frame_Usuario 
      Caption         =   "Datos del Usuario"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1335
      Left            =   120
      TabIndex        =   16
      Top             =   4800
      Width           =   8655
      Begin VB.CommandButton cmd_Configuracion 
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   345
         Left            =   8040
         Picture         =   "Ficha_PerfilesCaptahuellas.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   840
         Width           =   450
      End
      Begin VB.TextBox txt_Plantilla 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   3030
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   23
         ToolTipText     =   "Ingrese el c�digo del comprador o usuario"
         Top             =   900
         Width           =   1515
      End
      Begin VB.CommandButton cmd_Usuario 
         CausesValidation=   0   'False
         Enabled         =   0   'False
         Height          =   345
         Left            =   2640
         Picture         =   "Ficha_PerfilesCaptahuellas.frx":6570
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   360
         Width           =   330
      End
      Begin VB.TextBox txt_CodUsuario 
         CausesValidation=   0   'False
         Height          =   330
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   17
         ToolTipText     =   "Ingrese el c�digo del comprador o usuario"
         Top             =   360
         Width           =   1515
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Configuracion de Huellas"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   5520
         TabIndex        =   26
         Top             =   960
         Width           =   2430
      End
      Begin VB.Label lbl_Plantilla 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Plantilla de Huellas: "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   960
         Width           =   1995
      End
      Begin VB.Label lbl_DescripcionUsuario 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   330
         Left            =   3030
         TabIndex        =   20
         Top             =   360
         Width           =   5475
      End
      Begin VB.Label lbl_Usuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   405
         Width           =   810
      End
   End
   Begin VB.Frame frame_Datos 
      Caption         =   "Datos de Descripcion"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3855
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   8655
      Begin VB.ComboBox Cmb_Modelo 
         Height          =   315
         ItemData        =   "Ficha_PerfilesCaptahuellas.frx":665A
         Left            =   1920
         List            =   "Ficha_PerfilesCaptahuellas.frx":667F
         Sorted          =   -1  'True
         TabIndex        =   28
         Text            =   "Ahora se utiliza este control."
         Top             =   1080
         Width           =   2655
      End
      Begin VB.TextBox txt_Modelo 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   100
         TabIndex        =   27
         Text            =   "Este se mantendr� oculto."
         Top             =   1080
         Width           =   2400
      End
      Begin VB.TextBox txt_LimiteErrores 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   21
         Top             =   2880
         Width           =   2400
      End
      Begin VB.TextBox txt_LimiteHuellas 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   14
         Top             =   2520
         Width           =   2400
      End
      Begin VB.TextBox txt_Verificaciones 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   12
         Top             =   2160
         Width           =   2400
      End
      Begin VB.TextBox txt_Muestreos 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   10
         Top             =   1800
         Width           =   2400
      End
      Begin VB.CheckBox chk_Activo 
         Caption         =   "Perfil Activo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   480
         TabIndex        =   9
         Top             =   3360
         Width           =   2055
      End
      Begin VB.TextBox txt_Serial 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   7
         Top             =   1440
         Width           =   2400
      End
      Begin VB.TextBox txt_Codigo 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   0
         Top             =   360
         Width           =   2400
      End
      Begin VB.TextBox txt_Descripcion 
         BackColor       =   &H80000018&
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4680
         MaxLength       =   10
         TabIndex        =   1
         Top             =   720
         Width           =   2400
      End
      Begin VB.Label lbl_LimiteErrores 
         AutoSize        =   -1  'True
         Caption         =   "Intentos Erroneos Max. en Verificacion:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   22
         Top             =   2910
         Width           =   3855
      End
      Begin VB.Label lbl_LimiteHuellas 
         AutoSize        =   -1  'True
         Caption         =   "Limite de Huellas por Usuario: "
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   15
         Top             =   2550
         Width           =   3000
      End
      Begin VB.Label lbl_Verificaciones 
         AutoSize        =   -1  'True
         Caption         =   "Intentos V�lidos para Verificaci�n:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   13
         Top             =   2190
         Width           =   3375
      End
      Begin VB.Label lbl_Muestreos 
         AutoSize        =   -1  'True
         Caption         =   "Intentos V�lidos para Registro:"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   11
         Top             =   1830
         Width           =   3030
      End
      Begin VB.Label lbl_Serial 
         AutoSize        =   -1  'True
         Caption         =   "Serial:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   8
         Top             =   1470
         Width           =   630
      End
      Begin VB.Label lbl_Modelo 
         AutoSize        =   -1  'True
         Caption         =   "Modelo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   6
         Top             =   1110
         Width           =   750
      End
      Begin VB.Label lbl_Descripcion 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   5
         Top             =   750
         Width           =   1140
      End
      Begin VB.Label lbl_Codigo 
         AutoSize        =   -1  'True
         Caption         =   "ID Dispositivo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   480
         TabIndex        =   4
         Top             =   360
         Width           =   1425
      End
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   1296
      ButtonWidth     =   1799
      ButtonHeight    =   1244
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "Icono_Apagado"
      DisabledImageList=   "Icono_deshabilitado"
      HotImageList    =   "Iconos_Encendidos"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Agregar"
            Key             =   "Agregar"
            Object.ToolTipText     =   "Agregar una Nueva Ficha"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "Buscar"
            Object.ToolTipText     =   "Buscar una Ficha"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "Modificar"
            Object.ToolTipText     =   "Modificar una Ficha"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Eliminar"
            Key             =   "Eliminar"
            Object.ToolTipText     =   "Eliminar una Ficha"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "Cancelar"
            Object.ToolTipText     =   "Cancelar esta Ficha"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Grabar"
            Key             =   "Grabar"
            Object.ToolTipText     =   "Grabar esta Ficha"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "Salir"
            Object.ToolTipText     =   "Salir del Fichero"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Ayuda"
            Key             =   "Ayuda"
            Object.ToolTipText     =   "Ayuda del Sistema"
            ImageIndex      =   10
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   10080
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":6794
            Key             =   "Agrergar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":7470
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":814C
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":8E28
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":9B04
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":A7E0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":B4BC
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":C198
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":CE74
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":D190
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   8880
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":DE6C
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":EB48
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":EE64
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":F180
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":FE5C
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":10B38
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":11814
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":11B30
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":1280C
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":12B28
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   9480
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":13804
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":144E0
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":151BC
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":154D8
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":161B4
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":16E90
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":17B6C
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":18848
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":19524
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_PerfilesCaptahuellas.frx":19840
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "ficha_PerfilesCaptaHuellas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public SucursalUsuario As String
Private blnModificar As Boolean
Private Cmb_Modelo_SelectedIndex As Long

Private Sub Cmb_Modelo_Click()
    
    If Cmb_Modelo_SelectedIndex <> Cmb_Modelo.ListIndex Then
        
        Cmb_Modelo_SelectedIndex = Cmb_Modelo.ListIndex
        
        'C�digo del Evento
            
            Num = DeterminarNumMuestreos
            
            Me.txt_Muestreos.Text = CStr(Num)
            'Me.txt_Muestreos.Locked = True
            
            If Me.txt_Verificaciones.Text = Empty Then Me.txt_Verificaciones.Text = "1"
            If Me.txt_LimiteHuellas.Text = Empty Then Me.txt_LimiteHuellas.Text = "10"
            If Me.txt_LimiteErrores.Text = Empty Then Me.txt_LimiteErrores.Text = "1"
            
        'C�digo del Evento
        
    End If
    
End Sub

Private Sub Cmb_Modelo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.txt_Serial.SetFocus
    End If
End Sub

Private Sub Cmb_Modelo_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub

Private Sub cmd_configuracion_Click()
    
    Select Case Cmb_Modelo.Text
        
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            
            DPFP_UAUConfig.Show vbModal
            
        Case Is = "BioTrack", "BioTrack BioUsb"
            
            BioTrack_BioUsb_Config.Show vbModal
            
        Case Is = "Suprema", "Suprema BioMini (SFR300)", "Suprema BioMini Slim (SFU-S20)", "Suprema BioMini Plus"
            
            Suprema_Config.Show vbModal
            
        Case Is = "DEMO"
            
            DEMO_Enrollment.Show vbModal
            
        Case Else
            
            Mensaje True, "Disculpe, en estos momentos no existe compatibilidad para configurar este dispositivo."
            
    End Select
    
End Sub

Private Sub cmd_usuario_Click()
    txt_CodUsuario_KeyDown vbKeyF2, 0
End Sub

Private Sub Form_Load()
    
    Me.txt_Modelo.Visible = False
    Me.Cmb_Modelo.Visible = True
    Me.Cmb_Modelo.Top = Me.txt_Modelo.Top: Me.Cmb_Modelo.Left = Me.txt_Modelo.Left
    'Me.Cmb_Modelo.Height = Me.txt_Modelo.Height
    Me.Cmb_Modelo.Width = Me.txt_Modelo.Width
    Me.Cmb_Modelo.ListIndex = -1: Cmb_Modelo_SelectedIndex = -1
    Me.Cmb_Modelo.Text = Empty
    
    HabilitarDatos False
    HabilitarFrameUsuarios False
    
    txt_Codigo.MaxLength = Funciones.campoLength("ID_DISPOSITIVO", _
    "MA_CAPTAHUELLAS_PERFILES", ENT.BDD, Alfanumerico)
    txt_Descripcion.MaxLength = Funciones.campoLength("c_Descripcion", _
    "MA_CAPTAHUELLAS_PERFILES", ENT.BDD, Alfanumerico)
    txt_Modelo.MaxLength = Funciones.campoLength("c_Modelo", _
    "MA_CAPTAHUELLAS_PERFILES", ENT.BDD, Alfanumerico)
    txt_Serial.MaxLength = Funciones.campoLength("C_SERIAL", _
    "MA_CAPTAHUELLAS_PERFILES", ENT.BDD, Alfanumerico)
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        
        Case Is = "Agregar"
             Call Form_KeyDown(vbKeyF3, 0)
        Case Is = "Buscar"
             Call Form_KeyDown(vbKeyF2, 0)
        Case Is = "Modificar"
            Call Form_KeyDown(vbKeyF5, 0)
        Case Is = "Cancelar"
            Call Form_KeyDown(vbKeyF7, 0)
        Case Is = "Eliminar"
             Call Form_KeyDown(vbKeyF6, 0)
        Case Is = "Grabar"
            Call Form_KeyDown(vbKeyF4, 0)
        Case Is = "Salir"
            Call Form_KeyDown(vbKeyF12, 0)
        Case Is = "Ayuda"
            Call Form_KeyDown(vbKeyF1, 0)
                
    End Select
    
End Sub

Public Sub Form_BotonPresionado(Optional objToolBar As Object, Optional strBoton As String, Optional StrCasosEspeciales As String)
    
    Select Case strBoton
        
        Case Is = "Agregar"
'           me.Toolbar1.Buttons()
            
            objToolBar.Buttons("Agregar").Enabled = False
            objToolBar.Buttons("Modificar").Enabled = False
            objToolBar.Buttons("Buscar").Enabled = False
            objToolBar.Buttons("Cancelar").Enabled = True
            objToolBar.Buttons("Eliminar").Enabled = False
            objToolBar.Buttons("Grabar").Enabled = True
            objToolBar.Buttons("Salir").Enabled = False
            
        Case Is = "Modificar"
            objToolBar.Buttons("Agregar").Enabled = False
            objToolBar.Buttons("Modificar").Enabled = False
            objToolBar.Buttons("Buscar").Enabled = False
            objToolBar.Buttons("Cancelar").Enabled = True
            objToolBar.Buttons("Eliminar").Enabled = False
            objToolBar.Buttons("Grabar").Enabled = True
            objToolBar.Buttons("Salir").Enabled = False
            
        Case Is = "Buscar"
            objToolBar.Buttons("Agregar").Enabled = False
            objToolBar.Buttons("Modificar").Enabled = True
            objToolBar.Buttons("Buscar").Enabled = False
            objToolBar.Buttons("Cancelar").Enabled = True
            objToolBar.Buttons("Eliminar").Enabled = True
            objToolBar.Buttons("Grabar").Enabled = False
            objToolBar.Buttons("Salir").Enabled = True
                           
        Case Is = "Cancelar"
            objToolBar.Buttons("Agregar").Enabled = True
            objToolBar.Buttons("Modificar").Enabled = False
            objToolBar.Buttons("Buscar").Enabled = True
            objToolBar.Buttons("Cancelar").Enabled = False
            objToolBar.Buttons("Eliminar").Enabled = False
            objToolBar.Buttons("Grabar").Enabled = False
            objToolBar.Buttons("Salir").Enabled = True
            
        Case Is = "Eliminar"
            objToolBar.Buttons("Agregar").Enabled = True
            objToolBar.Buttons("Modificar").Enabled = False
            objToolBar.Buttons("Buscar").Enabled = True
            objToolBar.Buttons("Cancelar").Enabled = False
            objToolBar.Buttons("Eliminar").Enabled = False
            objToolBar.Buttons("Grabar").Enabled = False
            objToolBar.Buttons("Salir").Enabled = True
                  
        Case Is = "Grabar"
            objToolBar.Buttons("Agregar").Enabled = True
            objToolBar.Buttons("Modificar").Enabled = True
            objToolBar.Buttons("Buscar").Enabled = True
            objToolBar.Buttons("Cancelar").Enabled = False
            objToolBar.Buttons("Eliminar").Enabled = True
            objToolBar.Buttons("Grabar").Enabled = False
            objToolBar.Buttons("Salir").Enabled = True
            
        Case "Salir"
        
    End Select
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case Shift
        
        Case vbAltMask
            
            Select Case KeyCode
                
                Case vbKeyA
                    
                Case vbKeyE
                
            End Select
            
        Case Else
            
            Select Case KeyCode
                
                Case Is = vbKeyF1
                
                    oTeclado.Key_F1
        
                Case Is = vbKeyF3
                
                    Call agregar
                
                Case Is = vbKeyF2
                
                    Call buscar
                
                Case Is = vbKeyF4
                
                    Call Grabar
                
                Case Is = vbKeyF5
                
                    Call modificar

                Case Is = vbKeyF7
                
                    Call CANCELAR
                
                Case Is = vbKeyF6
                
                    Call eliminar
                
                Case Is = vbKeyF12
                    
                    Unload Me
                    
                    Set ficha_PerfilesCaptaHuellas = Nothing
                    
            End Select
            
    End Select
    
    Exit Sub
    
End Sub

Private Sub agregar()
    
    Form_BotonPresionado Me.Toolbar1, "Agregar"
    LimpiarDatos
    HabilitarDatos True
    HabilitarFrameUsuarios False
    Me.txt_Codigo.SetFocus
    
End Sub

Private Sub modificar()
    
    Form_BotonPresionado Me.Toolbar1, "Modificar"
    HabilitarDatos True
    HabilitarFrameUsuarios True
    Me.txt_Codigo.Locked = True
    blnModificar = True
    
End Sub

Private Sub eliminar()
    
    On Error GoTo ErrEliminar
    
    Form_BotonPresionado Me.Toolbar1, "Eliminar"
    
    If vbYes = MsgBox("ATENCION: Esta a punto de borrar todas las configuraciones para este dispositivo!" & vbNewLine & _
    "Lo cual indica que las huellas de usuarios y opciones actuales no podran ser accesibles." & vbNewLine & _
    "�Esta totalmente seguro de que realmente desea hacer esto?", vbInformation + vbYesNo, "Mensaje Stellar isBusiness") Then
        
        Dim Rs1 As New ADODB.Recordset
        Dim Rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        
        Rs1.Open "SELECT * FROM MA_CAPTAHUELLAS_PERFILES WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "'", ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        Rs2.Open "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "'", ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        rs3.Open "SELECT * FROM TR_CAPTAHUELLAS_PERFILES WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "'", ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_PERFILES", Rs1, ENT.BDD, ENT.BDD, Array("id"), Array(Array("Tipo_Cambio", 10)), True
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS", Rs2, ENT.BDD, ENT.BDD, Array("id"), Array(Array("Tipo_Cambio", 10)), False
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", rs3, ENT.BDD, ENT.BDD, Array("id"), Array(Array("Tipo_Cambio", 10)), False
        
        Rs1.Close
        Rs2.Close
        rs3.Close
        
        ENT.BDD.Execute "DELETE FROM MA_CAPTAHUELLAS_PERFILES WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "' "
        ENT.BDD.Execute "DELETE FROM MA_CAPTAHUELLAS_USUARIOS WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "' "
        ENT.BDD.Execute "DELETE FROM TR_CAPTAHUELLAS_PERFILES WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "' "
        
    End If
    
    LimpiarDatos
    HabilitarDatos False
    
    Call Mensaje(True, "Los datos han sido eliminados.")
    
    Exit Sub
    
ErrEliminar:
    
    Call Mensaje(True, "Ha ocurrido un error, no se ha podido eliminar los Datos. Reporte lo siguiente: " & Err.Description)
    
End Sub

Private Sub CANCELAR()
    
    Form_BotonPresionado Me.Toolbar1, "Cancelar"
    LimpiarDatos
    HabilitarDatos False
    HabilitarFrameUsuarios False
    Me.txt_Codigo.Locked = False
    blnModificar = False
    
End Sub

Private Sub Grabar()
    
    On Error GoTo ErrGrabar
    
    If ValidarDatos() Then
        
        Dim PerfilSql As String
        Dim PerfilRs As New ADODB.Recordset
        
        PerfilSql = " SELECT * FROM MA_CAPTAHUELLAS_PERFILES " & _
        "WHERE ID_DISPOSITIVO = '" & Trim(Me.txt_Codigo) & "' "
        
        ENT.BDD.BeginTrans
        
        PerfilRs.Open PerfilSql, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
        
        If Not blnModificar Then
            
            If Not PerfilRs.EOF Then
                
                Call Mensaje(True, "Atencion, ya existe un perfil con el mismo ID de Dispositivo." & vbNewLine & _
                "Debe establecer uno nuevo o modificar el anterior.")
                ENT.BDD.RollbackTrans
                
                Exit Sub
                
            Else
                
                PerfilRs.AddNew
                    
                    PerfilRs!ID_Dispositivo = Trim(Me.txt_Codigo)
                    PerfilRs!c_Descripcion = Trim(Me.txt_Descripcion)
                    PerfilRs!c_Modelo = Me.Cmb_Modelo.Text 'Me.txt_Modelo
                    PerfilRs!c_Serial = Me.txt_Serial
                    PerfilRs!nu_NumMuestreos = CInt(Me.txt_Muestreos)
                    PerfilRs!NU_NUMVERIFICACIONES = CInt(Me.txt_Verificaciones)
                    PerfilRs!nu_MaxHuellasXUsuario = CInt(Me.txt_LimiteHuellas)
                    PerfilRs!nu_MaxErroresVerificacion = CInt(Me.txt_LimiteErrores)
                    PerfilRs!BU_ACTIVO = Me.chk_Activo.value
                    PerfilRs!du_FechaActivacion = IIf(PerfilRs!BU_ACTIVO, FechaBD(Now, FBD_FULL, True), Null)
                    PerfilRs!du_FechaDesactivacion = Null
                    
                PerfilRs.UpdateBatch
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_PERFILES", PerfilRs, ENT.BDD, ENT.BDD, _
                Array("ID"), Array(Array("Tipo_Cambio", 0))
                
            End If
            
        Else
            
            PerfilRs.Update
                
                PerfilRs!c_Descripcion = Trim(Me.txt_Descripcion)
                PerfilRs!c_Modelo = Me.Cmb_Modelo.Text 'Me.txt_Modelo
                PerfilRs!c_Serial = Me.txt_Serial
                PerfilRs!nu_NumMuestreos = CInt(Me.txt_Muestreos)
                PerfilRs!NU_NUMVERIFICACIONES = CInt(Me.txt_Verificaciones)
                PerfilRs!nu_MaxHuellasXUsuario = CInt(Me.txt_LimiteHuellas)
                PerfilRs!nu_MaxErroresVerificacion = CInt(Me.txt_LimiteErrores)
                
                Select Case PerfilRs!BU_ACTIVO
                    Case vbChecked
                        If Me.chk_Activo <> vbChecked Then
                            PerfilRs!du_FechaDesactivacion = FechaBD(Now, FBD_FULL, True)
                        End If
                    Case vbUnchecked
                        If Me.chk_Activo <> vbUnchecked Then
                            PerfilRs!du_FechaActivacion = FechaBD(Now, FBD_FULL, True)
                        End If
                End Select
                
                PerfilRs!BU_ACTIVO = Me.chk_Activo
                
            PerfilRs.UpdateBatch
            
        End If
        
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_PERFILES", PerfilRs, ENT.BDD, ENT.BDD, _
        Array("ID"), Array(Array("Tipo_Cambio", 0)), False
        
        PerfilRs.Close
        
        ENT.BDD.CommitTrans
        
        Call CANCELAR
        
        Call Mensaje(True, "Perfil de dispositivo grabado con exito!.")
        
    End If
    
    Exit Sub
    
ErrGrabar:
    
    ENT.BDD.RollbackTrans
    Call Mensaje(True, "Ha ocurrido un error al grabar los datos. Reporte lo siguiente: " & Err.Description)
    
End Sub

Private Function ValidarDatos() As Boolean
    
    ValidarDatos = True
    
    Dim DatosCorrectos As Boolean
    
    DatosCorrectos = False
    
    If Len(Me.txt_Codigo) <= 0 Then
        Call Mensaje(True, "Debe ingresar un C�digo para el Perfil.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_Descripcion) <= 0 Then
        Call Mensaje(True, "Debe ingresar una Descripci�n para el Perfil.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_Modelo) <= 0 Then
        Call Mensaje(True, "Debe ingresar un Modelo de dispositivo para el Perfil.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_Muestreos) <= 0 Or Not IsNumeric(Me.txt_Muestreos) Then
        Call Mensaje(True, "Debe ingresar el numero de muestreos para la asociacion de huellas.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_Verificaciones) <= 0 Or Not IsNumeric(Me.txt_Verificaciones) Then
        Call Mensaje(True, "Debe ingresar la cantidad de solicitud de huellas" & vbNewLine & _
        "durante el proceso de verificaci�n.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_LimiteHuellas) <= 0 Or Not IsNumeric(Me.txt_LimiteHuellas) Then
        Call Mensaje(True, "Debe ingresar el limite de activaci�n" & vbNewLine & _
        "para la configuraci�n de huellas por usuario.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txt_LimiteErrores) <= 0 Or Not IsNumeric(Me.txt_LimiteErrores) Then
        Call Mensaje(True, "Debe ingresar el limite de errores por el usuario" & vbNewLine & _
        "durante el proceso de verificaci�n.")
        ValidarDatos = False
        Exit Function
    End If
    
    DatosCorrectos = True
    
    If Not DatosCorrectos Then
        ValidarDatos = False
    End If
    
End Function

Private Sub LimpiarDatos()
    
    'FRAME DATOS PERFIL
    
    Me.txt_Codigo = Empty
    Me.txt_Descripcion = Empty
    Me.txt_Serial = Empty
    Me.Cmb_Modelo.ListIndex = -1 'Me.txt_Modelo = ""
    Me.txt_Muestreos = Empty
    Me.txt_Verificaciones = Empty
    Me.txt_LimiteHuellas = Empty
    Me.txt_LimiteErrores = Empty
    
    Me.chk_Activo = vbUnchecked
    
    'FRAME DATOS USUARIO
    
    Me.txt_CodUsuario = Empty
    Me.lbl_DescripcionUsuario = Empty
    Me.txt_Plantilla = Empty
    
End Sub

Private Sub buscar()
    
    Dim miConsulta As New ClsConsultas
    
    miConsulta.strTitulo = " P E R F I L E S  C A P T A H U E L L A S "
    
    miConsulta.strCadBusCod = "ID_DISPOSITIVO" 'campo de la tabla para buscar
    miConsulta.strCadBusDes = "c_Descripcion" 'campo de la tabla para buscar
    miConsulta.strOrderBy = "ID_DISPOSITIVO"
    miConsulta.Consulta_Inicializar _
    "Select ID_Dispositivo, c_Descripcion, c_Modelo, c_Serial " & _
    "FROM MA_CAPTAHUELLAS_PERFILES", ENT.BDD
    
    miConsulta.Consulta_AgregarCol "Dispositivo", 2000, 0
    miConsulta.Consulta_AgregarCol "Descripcion", 2000, 0
    miConsulta.Consulta_AgregarCol "Modelo", 2000, 0
    miConsulta.Consulta_AgregarCol "Serial", 2000, 0
    
    'FrmConsultas.txtDato.Text = "%"
    
    miConsulta.Consulta_Show
    
    Select Case miConsulta.strBotonPresionado
        
        Case "Aceptar"
            
            DoEvents
            
            Form_BotonPresionado Me.Toolbar1, "Buscar"
            
            LimpiarDatos
            
            Me.txt_Codigo = miConsulta.strItemC1
            buscarDetalle (miConsulta.strItemC1)
            
            HabilitarDatos False
            HabilitarFrameUsuarios False
            
        'Case "Informacion"
        'Case "Proveedores"
        'Case "Salir"
        
    End Select
    
    miConsulta.Consulta_Hide
    
End Sub

Private Sub buscarDetalle(CodigoMain)
    
    Dim Sql As String
    Dim mRs As New ADODB.Recordset
    
    Sql = "SELECT * FROM MA_CAPTAHUELLAS_PERFILES " & _
    "WHERE ID_DISPOSITIVO = '" & CodigoMain & "' "
    
    mRs.Open Sql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        
        Me.txt_Codigo = CodigoMain
        Me.txt_Descripcion = mRs!c_Descripcion
        
        Call CargarModelo(mRs!c_Modelo)  'Me.txt_modelo = mRs!c_Modelo
        
        Me.txt_Serial = mRs!c_Serial
        Me.txt_Muestreos = CStr(mRs!nu_NumMuestreos)
        Me.txt_Verificaciones = CStr(mRs!NU_NUMVERIFICACIONES)
        Me.txt_LimiteHuellas = CStr(mRs!nu_MaxHuellasXUsuario)
        Me.txt_LimiteErrores = CStr(mRs!nu_MaxErroresVerificacion)
        
        Me.chk_Activo.value = IIf(mRs!BU_ACTIVO, vbChecked, vbUnchecked)
        
    End If
    
    mRs.Close
    
End Sub

Private Sub CargarModelo(ByVal pText As String)
    
    On Error GoTo Error
    
    Me.Cmb_Modelo.Text = pText
    
    Exit Sub
    
Error:
    
    Me.Cmb_Modelo.ListIndex = -1
    
End Sub

Private Sub txt_codigo_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txt_Descripcion.SetFocus
    End If
End Sub

Private Sub txt_codigo_LostFocus()
    txt_Codigo.Text = Funciones.QuitarComillasDobles(Me.txt_Codigo.Text)
    txt_Codigo.Text = Funciones.QuitarComillasSimples(Me.txt_Codigo.Text)
End Sub

Private Sub txt_CodUsuario_LostFocus()
    VerificarUsuario
End Sub

Private Sub txt_descripcion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.Cmb_Modelo.SetFocus 'Me.txt_Modelo.SetFocus
    End If
End Sub

Private Sub txt_descripcion_LostFocus()
    txt_Descripcion.Text = Funciones.QuitarComillasDobles(txt_Descripcion)
    txt_Descripcion.Text = Funciones.QuitarComillasSimples(txt_Descripcion)
End Sub

Private Sub txt_LimiteErrores_Change()
    
    MaxVer = txt_Verificaciones.Text
    
    If IsNumeric(MaxVer) Then
        
        MaxVer = IIf(CInt(MaxVer > 0), MaxVer, 1)
        
    Else
        
        txt_Verificaciones.Text = "1"
        txt_LimiteErrores.Text = "1"
        
        Exit Sub
        
    End If
    
    Var = CheckCad(txt_LimiteErrores, 0, , False)
    
    If IsNumeric(txt_LimiteErrores.Text) Then
        If CInt(txt_LimiteErrores) > CInt(MaxVer) Then
            txt_LimiteErrores.Text = CStr(MaxVer)
        End If
    Else
        txt_LimiteErrores.Text = "1"
    End If
    
End Sub

Private Sub txt_LimiteErrores_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.chk_Activo.SetFocus
    End If
End Sub

Private Sub txt_LimiteHuellas_Change()
    
    Var = CheckCad(txt_LimiteHuellas, 0, , False)
    
    If IsNumeric(txt_LimiteHuellas.Text) Then
        If CInt(txt_LimiteHuellas.Text) > 10 Or CInt(txt_LimiteHuellas.Text) < 1 Then
            txt_LimiteHuellas.Text = "10"
        End If
    Else
        txt_LimiteHuellas.Text = "10"
    End If
    
End Sub

Private Sub txt_LimiteHuellas_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.txt_LimiteErrores.SetFocus
    End If
End Sub

Private Sub txt_Modelo_Change()
'    Num = DeterminarNumMuestreos
'    Me.txt_Muestreos = CStr(Num)
'    Me.txt_Muestreos.Locked = True
End Sub

Private Function DeterminarNumMuestreos() As Integer
    Select Case (txt_Modelo)
        Case Is = "Digital Persona", "Digital Persona U.are.U Compatible FP Reader", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
            DeterminarNumMuestreos = 4
        Case Is = "BioTrack", "BioTrack BioUsb"
            DeterminarNumMuestreos = 3
        Case Else
            DeterminarNumMuestreos = 1
    End Select
End Function

Private Sub txt_Modelo_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyReturn Then
'        Me.txt_Serial.SetFocus
'    End If
End Sub

Private Sub txt_Muestreos_Change()
    
    Var = CheckCad(txt_Muestreos, 0, , False)
    
    If IsNumeric(txt_Muestreos.Text) Then
        If Val(txt_Muestreos.Text) > 32767 Then
            txt_Muestreos.Text = "1"
        ElseIf CInt(txt_Muestreos.Text) < 1 Then
            txt_Muestreos.Text = "1"
        End If
    Else
        txt_Muestreos.Text = "1"
    End If
    
End Sub

Private Sub txt_Muestreos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.txt_Verificaciones.SetFocus
    End If
End Sub

Private Sub txt_Serial_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.txt_Muestreos.SetFocus
    End If
End Sub

Private Sub txt_Verificaciones_Change()
    
    Var = CheckCad(txt_Verificaciones, 0, , False)
    
    If IsNumeric(txt_Verificaciones) Then
        If Val(txt_Verificaciones.Text) > 32767 Then
            txt_Verificaciones.Text = "1"
        ElseIf CInt(txt_Verificaciones.Text) < 1 Then
            txt_Verificaciones.Text = "1"
        End If
    Else
        txt_Verificaciones = "1"
    End If
    
    txt_LimiteErrores_Change
    
End Sub

Private Sub HabilitarDatos(Decision As Boolean)
    
    Me.txt_Codigo.Locked = Not Decision
    Me.txt_LimiteHuellas.Locked = Not Decision
    Me.txt_Descripcion.Locked = Not Decision
    Me.Cmb_Modelo.Locked = Not Decision 'Me.txt_Modelo.Locked = Not Decision
    Me.txt_Serial.Locked = Not Decision
    Me.txt_Verificaciones.Locked = Not Decision
    Me.txt_LimiteErrores.Locked = Not Decision
    
End Sub

Private Sub HabilitarFrameUsuarios(Decision As Boolean)
    
    Me.frame_Usuario.Enabled = Decision
    Me.txt_CodUsuario.Locked = Not Decision
    Me.cmd_Usuario.Enabled = Decision
    
    Me.cmd_Configuracion.Enabled = False
    Me.txt_Plantilla.Locked = True
    
End Sub

Private Sub txt_CodUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyF2
            Set Forma = Me
            Set Campo_Txt = Me.txt_CodUsuario
            Set Campo_Lbl = Me.lbl_DescripcionUsuario
            'Call MAKE_VIEW("MA_USUARIOS", "CODUSUARIO", "descripcion", "U S U A R I O S", Me, "GENERICO", True)
            'Call FrmAppLink.MAKE_VIEW("MA_USUARIOS", "CODUSUARIO", "descripcion", "U S U A R I O S", Me, "GENERICO", True)
            Resultado = FrmAppLink.BuscarInfo_Usuario(isQuery:=True)
            
            If Not IsEmpty(Resultado) Then
                'Me.lbl_DescripcionUsuario.Caption = Resultado(1)
                Me.txt_CodUsuario.Text = Resultado(0)
            End If
            
            VerificarUsuario
        Case Is = vbKeyReturn
            VerificarUsuario
    End Select
End Sub

Private Sub VerificarUsuario()
    
    Dim Sql As String
    
    Sql = "SELECT * FROM MA_USUARIOS " & _
    "WHERE CODUSUARIO = '" & Me.txt_CodUsuario.Text & "' " & _
    "AND bs_Activo = 1 "
    
    Dim mRs As New ADODB.Recordset
    
    mRs.Open Sql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        
        SucursalUsuario = mRs!localidad
        
        ObtenerPerfilUsuario
        
        Me.lbl_DescripcionUsuario = mRs!descripcion
        cmd_Configuracion.Enabled = True
        
        Toolbar1.Buttons("Agregar").Enabled = False
        Toolbar1.Buttons("Modificar").Enabled = False
        Toolbar1.Buttons("Buscar").Enabled = False
        Toolbar1.Buttons("Cancelar").Enabled = True
        Toolbar1.Buttons("Eliminar").Enabled = False
        Toolbar1.Buttons("Grabar").Enabled = False
        Toolbar1.Buttons("Salir").Enabled = True
        
    Else
        
        If txt_CodUsuario.Text <> Empty Then
            Mensaje True, "Atenci�n, el usuario no existe, o se encuentra inactivo." & vbNewLine
        End If
        
        Me.txt_CodUsuario = Empty
        Me.lbl_DescripcionUsuario = Empty
        Me.txt_Plantilla = "0"
        
        cmd_Configuracion.Enabled = False
        
    End If
    
    mRs.Close
    
End Sub

Private Sub ObtenerPerfilUsuario()
    
    Dim PerfilUsuarioRs As New ADODB.Recordset
    Dim PerfilUsuarioSQL As String
    
    PerfilUsuarioSQL = "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS WHERE c_CodUsuario = '" & Me.txt_CodUsuario & _
    "' AND ID_DISPOSITIVO = '" & Me.txt_Codigo & "' AND c_CodLocalidad = '" & SucursalUsuario & "'"
    
    PerfilUsuarioRs.Open PerfilUsuarioSQL, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    If Not PerfilUsuarioRs.EOF Then
        Me.txt_Plantilla = CStr(CDec(PerfilUsuarioRs!NU_PLANTILLA))
    Else
        IniciarPerfilUsuario
        Me.txt_Plantilla = "0"
    End If
    
End Sub

Private Sub IniciarPerfilUsuario()
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS WHERE ID_DISPOSITIVO = '" & Me.txt_Codigo & "'" & _
    " AND C_CODUSUARIO = '" & Me.txt_CodUsuario & "'" & _
    " AND c_CodLocalidad = '" & SucursalUsuario & "'"
    
    mRs.Open mSql, ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If mRs.EOF Then
        
        mRs.AddNew
        
        mRs!ID_Dispositivo = Me.txt_Codigo
        mRs!C_CODUSUARIO = Me.txt_CodUsuario
        mRs!c_CodLocalidad = SucursalUsuario
        mRs!NU_PLANTILLA = 0
        mRs!C_NOMBRESUSUARIO = Me.lbl_DescripcionUsuario
        
        mRs.UpdateBatch
        
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS", mRs, ENT.BDD, ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
        
        mRs.Close
        
    End If
    
    'Ent.BDD.Execute "INSERT INTO MA_CAPTAHUELLAS_USUARIOS (ID_DISPOSITIVO, C_CODUSUARIO, " & _
    '"C_NOMBRESUSUARIO, NU_PLANTILLA, c_CodLocalidad) VALUES ('" & Me.txt_Codigo & "', '" & Me.txt_CodUsuario & "', " & _
    '"'" & Me.lbl_DescripcionUsuario & "', 0, '" & Sucursal & "')"
    
End Sub

'Private Sub buscarPlantillaUsuario()

'End Sub

Private Sub txt_Verificaciones_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.txt_LimiteHuellas.SetFocus
    End If
End Sub
