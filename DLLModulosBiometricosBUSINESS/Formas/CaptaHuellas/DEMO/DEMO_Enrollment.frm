VERSION 5.00
Begin VB.Form DEMO_Enrollment 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de CaptaHuellas"
   ClientHeight    =   5460
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer TimerDeleteReg 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   720
      Top             =   0
   End
   Begin VB.Timer TimerDemoReg 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "    Mano Izquierda"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoMe�iqueIzquierdo 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularIzquierdo 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioIzquierdo 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceIzquierdo 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoPulgarIzquierdo 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   360
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.Frame FrameManoDerecha 
      Caption         =   "     Mano Derecha"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoPulgarDerecho 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2760
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceDerecho 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioDerecho 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularDerecho 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMe�iqueDerecho 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   360
         Width           =   1935
      End
      Begin VB.Line LineRellenoFrameIzquierdo 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   375
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.CommandButton Close 
      BackColor       =   &H80000003&
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      MaskColor       =   &H80000003&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4920
      Width           =   1455
   End
   Begin VB.Label lblAccionDispositivo 
      BackStyle       =   0  'Transparent
      Caption         =   "Accion del Dispositivo: Listo para Detectar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   14
      Top             =   4440
      Width           =   5115
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblStatus 
      BackStyle       =   0  'Transparent
      Caption         =   "Registrando..."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   13
      Top             =   3960
      Width           =   5115
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "DEMO_Enrollment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Public Enum BioTrack_CH_Dedos
'
'Me�iqueIzquierdo = 1
'AnularIzquierdo = 2
'MedioIzquierdo = 4
'IndiceIzquierdo = 8
'PulgarIzquierdo = 16
'
'PulgarDerecho = 32
'IndiceDerecho = 64
'MedioDerecho = 128
'AnularDerecho = 256
'Me�iqueDerecho = 512
'
'End Enum

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public MaxEnrollFingerCount As Integer
Public EnrolledFingersMask As Integer
Public CurrentEnrollFingerMask As Integer

Public RegistrandoMe�iqueIzquierdo As Boolean, BorrandoMe�iqueIzquierdo As Boolean
Public RegistrandoAnularIzquierdo As Boolean, BorrandoAnularIzquierdo As Boolean
Public RegistrandoMedioIzquierdo As Boolean, BorrandoMedioIzquierdo As Boolean
Public RegistrandoIndiceIzquierdo As Boolean, BorrandoIndiceIzquierdo As Boolean
Public RegistrandoPulgarIzquierdo As Boolean, BorrandoPulgarIzquierdo As Boolean

Public RegistrandoMe�iqueDerecho As Boolean, BorrandoMe�iqueDerecho As Boolean
Public RegistrandoAnularDerecho As Boolean, BorrandoAnularDerecho As Boolean
Public RegistrandoMedioDerecho As Boolean, BorrandoMedioDerecho As Boolean
Public RegistrandoIndiceDerecho As Boolean, BorrandoIndiceDerecho As Boolean
Public RegistrandoPulgarDerecho As Boolean, BorrandoPulgarDerecho As Boolean

Public Cargando As Boolean
Public EvitarActivate As Boolean

Private UniTemplateFull() As Byte
Private FileNumber As Long

Option Explicit

Private Sub Close_Click()
    
    Unload Me
    Set DEMO_Enrollment = Nothing
    Exit Sub
    
End Sub

Private Sub cmdDedoAnularDerecho_Click()
    
    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularDerecho.value = vbChecked And Not RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'Para que no haga nada
        BorrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbChecked And RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoAnularDerecho = False
        Exit Sub
    End If
    
    If cmdDedoAnularDerecho.value = vbUnchecked And Not BorrandoAnularDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'para que no haga nada
        RegistrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbUnchecked And BorrandoAnularDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoAnularDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoAnularIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularIzquierdo.value = vbChecked And Not RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'Para que no haga nada
        BorrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbChecked And RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoAnularIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoAnularIzquierdo.value = vbUnchecked And Not BorrandoAnularIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'para que no haga nada
        RegistrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbUnchecked And BorrandoAnularIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoAnularIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoIndiceDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceDerecho.value = vbChecked And Not RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'Para que no haga nada
        BorrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbChecked And RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoIndiceDerecho = False
        Exit Sub
    End If
    
    If cmdDedoIndiceDerecho.value = vbUnchecked And Not BorrandoIndiceDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'para que no haga nada
        RegistrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbUnchecked And BorrandoIndiceDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoIndiceDerecho = False
        Exit Sub
    End If
    
End Sub

Private Sub cmdDedoIndiceIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceIzquierdo.value = vbChecked And Not RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'Para que no haga nada
        BorrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbChecked And RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoIndiceIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoIndiceIzquierdo.value = vbUnchecked And Not BorrandoIndiceIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'para que no haga nada
        RegistrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbUnchecked And BorrandoIndiceIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoIndiceIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioDerecho.value = vbChecked And Not RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'Para que no haga nada
        BorrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbChecked And RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoMedioDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMedioDerecho.value = vbUnchecked And Not BorrandoMedioDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'para que no haga nada
        RegistrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbUnchecked And BorrandoMedioDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoMedioDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioIzquierdo.value = vbChecked And Not RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'Para que no haga nada
        BorrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbChecked And RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoMedioIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMedioIzquierdo.value = vbUnchecked And Not BorrandoMedioIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'para que no haga nada
        RegistrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbUnchecked And BorrandoMedioIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoMedioIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMe�iqueDerecho.value = vbChecked And Not RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'Para que no haga nada
        BorrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbChecked And RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoMe�iqueDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueDerecho.value = vbUnchecked And Not BorrandoMe�iqueDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'para que no haga nada
        RegistrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbUnchecked And BorrandoMe�iqueDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoMe�iqueDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueIzquierdo.value = vbChecked And Not RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'Para que no haga nada
        BorrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbChecked And RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoMe�iqueIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueIzquierdo.value = vbUnchecked And Not BorrandoMe�iqueIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'para que no haga nada
        RegistrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbUnchecked And BorrandoMe�iqueIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoMe�iqueIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarDerecho_Click()
    
    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If
    
    If cmdDedoPulgarDerecho.value = vbChecked And Not RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'Para que no haga nada
        BorrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbChecked And RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoPulgarDerecho = False
        Exit Sub
    End If
    
    If cmdDedoPulgarDerecho.value = vbUnchecked And Not BorrandoPulgarDerecho Then
        CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'para que no haga nada
        RegistrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbUnchecked And BorrandoPulgarDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoPulgarDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoPulgarIzquierdo.value = vbChecked And Not RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        TimerDemoReg.Enabled = False
        TimerDemoReg.Enabled = 2500
        TimerDemoReg.Enabled = True
        CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'Para que no haga nada
        BorrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando..."
        lblAccionDispositivo.Caption = "Listo para detectar."
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbChecked And RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = Empty
        RegistrandoPulgarIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoPulgarIzquierdo.value = vbUnchecked And Not BorrandoPulgarIzquierdo Then
        CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'para que no haga nada
        RegistrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbChecked
        TimerDeleteReg.Enabled = False
        TimerDeleteReg.Enabled = 1000
        TimerDeleteReg.Enabled = True
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbUnchecked And BorrandoPulgarIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = Empty
        BorrandoPulgarIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub Form_Activate()
    
    ' Set properties to ZKEngine object.
    
    If EvitarActivate Then
        EvitarActivate = False
        Exit Sub
    End If
    
    On Error Resume Next
    
    MaxEnrollFingerCount = CInt(ficha_PerfilesCaptaHuellas.txt_LimiteHuellas)
    
    EnrolledFingersMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    'Marcar los que Apliquen.
    
    SetUpMask
    
End Sub

Private Sub SetUpMask()
    
    Dim Mask As Integer
    Mask = EnrolledFingersMask
    
    'With CH_Dedos
    
        If Mask - CH_Dedos.Me�iqueDerecho >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueDerecho
            Cargando = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularDerecho >= 0 Then
            Mask = Mask - CH_Dedos.AnularDerecho
            Cargando = True
            cmdDedoAnularDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioDerecho >= 0 Then
            Mask = Mask - CH_Dedos.MedioDerecho
            Cargando = True
            cmdDedoMedioDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceDerecho >= 0 Then
            Mask = Mask - CH_Dedos.IndiceDerecho
            Cargando = True
            cmdDedoIndiceDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.PulgarDerecho >= 0 Then
            Mask = Mask - CH_Dedos.PulgarDerecho
            Cargando = True
            cmdDedoPulgarDerecho.value = vbChecked
        End If
        
        '-------------------------------------------'
        
        If Mask - CH_Dedos.PulgarIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.PulgarIzquierdo
            Cargando = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.IndiceIzquierdo
            Cargando = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.MedioIzquierdo
            Cargando = True
            cmdDedoMedioIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.AnularIzquierdo
            Cargando = True
            cmdDedoAnularIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.Me�iqueIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueIzquierdo
            Cargando = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        End If
        
    'End With
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
     ' Show new fingerprint mask.
    ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(EnrolledFingersMask)
    
    'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & _
    CInt(DPFPEnrollmentCtrl.EnrolledFingersMask) & "WHERE c_CodUsuario = '" & _
    ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND ID_DISPOSITIVO = '" & _
    ficha_PerfilesCaptaHuellas.txt_Codigo & "'"
    
End Sub

Private Sub ZKEngine_OnEnroll(ByVal ActionResult As Boolean, ByVal ATemplate As Variant)
    
    On Error GoTo ErrorEnroll
    
    If ActionResult = False Then
        DoEvents
        lblStatus.Caption = "El Registro ha fallado."
        lblAccionDispositivo.Caption = "Por favor espere..."
        DoEvents
        Sleep 2500
        ResetearStatus
        Exit Sub
    End If
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Ingreso_Enrollment, CurrentMask, EnrolledFingersMask)
    
    EnrolledFingersMask = EnrolledFingersMask + CurrentEnrollFingerMask
    
    Dim TmpTemplate(5) As Byte, I As Integer
    
    For I = 0 To UBound(TmpTemplate)
        TmpTemplate(I) = ATemplate(I)
    Next
    
    If ActualizarPerfilUsuario(Ingreso_Enrollment, CurrentEnrollFingerMask, TmpTemplate) Then
        Call ActualizarPlantillaUsuario(EnrolledFingersMask)
        'Exit Sub
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            RegistrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        Case CH_Dedos.AnularIzquierdo
            RegistrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbChecked
        Case CH_Dedos.MedioIzquierdo
            RegistrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbChecked
        Case CH_Dedos.IndiceIzquierdo
            RegistrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarIzquierdo
            RegistrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarDerecho
            RegistrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbChecked
        Case CH_Dedos.IndiceDerecho
            RegistrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbChecked
        Case CH_Dedos.MedioDerecho
            RegistrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbChecked
        Case CH_Dedos.AnularDerecho
            RegistrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbChecked
        Case CH_Dedos.Me�iqueDerecho
            RegistrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    CurrentEnrollFingerMask = 0
    
    Exit Sub
    
ErrorEnroll:
    
    'Debug.Print Err.Description
    
    IgnorarActivate
    'KillShot TmpArchivo
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    ResetearStatus
    
End Sub

Private Sub ActualizarPlantillaUsuario(ByVal Valor As Integer)
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT * FROM MA_CAPTAHUELLAS_USUARIOS " & _
    "WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    mRs.Open mSql, ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs.Update
        mRs!NU_PLANTILLA = Valor
        mRs.UpdateBatch
        PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS", mRs, _
        ENT.BDD, ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
        mRs.Close
    End If
    
   'Ent.BDD.Execute "UPDATE MA_CAPTAHUELLAS_USUARIOS SET NU_PLANTILLA = " & valor & " WHERE " & _
   '"ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' AND C_CODUSUARIO = '" & _
   'ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' AND c_CodLocalidad = '" & Sucursal & "'"
   
   ficha_PerfilesCaptaHuellas.txt_Plantilla = CStr(Valor)
    
End Sub

Private Function ActualizarPerfilUsuario(TipoOperacion As TipoOperacion_CH, _
MaskUpdate As Integer, Optional CHData As Variant) As Boolean
    
    'On Error GoTo ErrorOperacion
    
    Dim ID_Dedo As String
    Dim RsOperacion As New ADODB.Recordset
    Dim mSql As String
    Dim Data As Variant
    Dim MiConexion As New ADODB.Connection
    Dim CadConexion As String
    
    MiConexion.ConnectionString = ENT.BDD.ConnectionString
    
    MiConexion.Open
   
    ActualizarPerfilUsuario = True
    
    Select Case MaskUpdate
        Case CH_Dedos.Me�iqueIzquierdo
            ID_Dedo = "Me�ique Izquierdo"
        Case CH_Dedos.AnularIzquierdo
            ID_Dedo = "Anular Izquierdo"
        Case CH_Dedos.MedioIzquierdo
            ID_Dedo = "Medio Izquierdo"
        Case CH_Dedos.IndiceIzquierdo
            ID_Dedo = "Indice Izquierdo"
        Case CH_Dedos.PulgarIzquierdo
            ID_Dedo = "Pulgar Izquierdo"
        Case CH_Dedos.PulgarDerecho
            ID_Dedo = "Pulgar Derecho"
        Case CH_Dedos.IndiceDerecho
            ID_Dedo = "Indice Derecho"
        Case CH_Dedos.MedioDerecho
            ID_Dedo = "Medio Derecho"
        Case CH_Dedos.AnularDerecho
            ID_Dedo = "Anular Derecho"
        Case CH_Dedos.Me�iqueDerecho
            ID_Dedo = "Me�ique Derecho"
        Case Else
            ID_Dedo = "Desconocido"
    End Select
    
    If ID_Dedo = "Desconocido" Then
        
        IgnorarActivate
        
        Mensaje True, "La huella digital no se ha podido grabar." & vbNewLine & _
        "El reconocimiento ha sido invalido." & vbNewLine & _
        "Intente con otra posici�n."
        
        ActualizarPerfilUsuario = False
        
        Exit Function
        
    End If
    
    If Not IsMissing(CHData) Then
        Data = CHData
    End If
    
    mSql = "SELECT * FROM TR_CAPTAHUELLAS_PERFILES " & _
    "WHERE ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND C_CODUSUARIO = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND C_DEDO = '" & ID_Dedo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    RsOperacion.Open mSql, MiConexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            If RsOperacion.EOF Then
                
                RsOperacion.AddNew
                RsOperacion!ID_Dispositivo = ficha_PerfilesCaptaHuellas.txt_Codigo
                RsOperacion!C_CODUSUARIO = ficha_PerfilesCaptaHuellas.txt_CodUsuario
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_CodLocalidad = ficha_PerfilesCaptaHuellas.SucursalUsuario
                RsOperacion!BIN_DATA = Data 'StrConv(CHData, vbUnicode)
                RsOperacion!bin_DataArray = Data
                RsOperacion.UpdateBatch
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
                
            Else
                
                RsOperacion.Update
                RsOperacion!ID_Dispositivo = ficha_PerfilesCaptaHuellas.txt_Codigo
                RsOperacion!C_CODUSUARIO = ficha_PerfilesCaptaHuellas.txt_CodUsuario
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_CodLocalidad = ficha_PerfilesCaptaHuellas.SucursalUsuario
                RsOperacion!BIN_DATA = Data 'StrConv(CHData, vbUnicode)
                RsOperacion!bin_DataArray = Data
                RsOperacion.UpdateBatch
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 0)), False
                
            End If
            
        Case TipoOperacion_CH.Borrado_Deletion
            
            If Not RsOperacion.EOF Then
                
                PasarTrPend "TR_PEND_ADM_CAPTAHUELLAS_USUARIOS_HUELLAS", RsOperacion, ENT.BDD _
                , ENT.BDD, Array("ID"), Array(Array("TIPO_CAMBIO", 10)), True
                RsOperacion.MoveFirst
                RsOperacion.Delete
                RsOperacion.UpdateBatch
                
            End If
            
    End Select
    
    RsOperacion.Close
    MiConexion.Close
    
    ActualizarPerfilUsuario = True
    
    Exit Function
    
ErrorOperacion:
    
    ActualizarPerfilUsuario = False
    Debug.Print Err.Description
    Debug.Print Data
    
End Function

Private Function DiferenciaBytes(TipoOperacion As TipoOperacion_CH, _
CurrentMask As Integer, NewMask As Integer) As Integer
    
    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            DiferenciaBytes = NewMask - CurrentMask
            
        Case TipoOperacion_CH.Borrado_Deletion
            
            DiferenciaBytes = CurrentMask - NewMask
            
    End Select
    
End Function

Private Function GetCurrentUserMask() As Integer
    
    On Error GoTo Error
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT nu_Plantilla FROM MA_CAPTAHUELLAS_USUARIOS " & _
    "WHERE c_CodUsuario = '" & ficha_PerfilesCaptaHuellas.txt_CodUsuario & "' " & _
    "AND ID_DISPOSITIVO = '" & ficha_PerfilesCaptaHuellas.txt_Codigo & "' " & _
    "AND c_CodLocalidad = '" & ficha_PerfilesCaptaHuellas.SucursalUsuario & "' "
    
    mRs.Open mSql, ENT.BDD, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        GetCurrentUserMask = mRs!NU_PLANTILLA
    Else
        GetCurrentUserMask = 0
    End If
    
    mRs.Close
    
    Exit Function
    
Error:
    
    GetCurrentUserMask = 0
    
End Function

Private Sub ZKEngine_Delete(ByVal Template As Variant)
    
    On Error GoTo ErrorDelete
    
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(ficha_PerfilesCaptaHuellas.txt_Plantilla)
    
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Borrado_Deletion, CurrentMask, CurrentEnrollFingerMask)
    
    If ActualizarPerfilUsuario(Borrado_Deletion, CurrentEnrollFingerMask) Then
        Call ActualizarPlantillaUsuario(Diferencia)
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            BorrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        Case CH_Dedos.AnularIzquierdo
            BorrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbUnchecked
        Case CH_Dedos.MedioIzquierdo
            BorrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbUnchecked
        Case CH_Dedos.IndiceIzquierdo
            BorrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarIzquierdo
            BorrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarDerecho
            BorrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbUnchecked
        Case CH_Dedos.IndiceDerecho
            BorrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbUnchecked
        Case CH_Dedos.MedioDerecho
            BorrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbUnchecked
        Case CH_Dedos.AnularDerecho
            BorrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbUnchecked
        Case CH_Dedos.Me�iqueDerecho
            BorrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbUnchecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    EnrolledFingersMask = EnrolledFingersMask - CurrentEnrollFingerMask
    
    CurrentEnrollFingerMask = 0
    
    Exit Sub
    
ErrorDelete:
    
    IgnorarActivate
    
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    
End Sub

Private Sub RellenarTemplate()

End Sub

Private Function IgnorarActivate() As Boolean
    IgnorarActivate = True
    EvitarActivate = True
End Function

Private Sub ResetearStatus()
    lblStatus.Caption = "Pulse un bot�n disponible para comenzar."
    lblAccionDispositivo.Caption = "Accion del Dispositivo: Listo para Detectar"
End Sub

Private Sub TimerDeleteReg_Timer()
    TimerDeleteReg.Enabled = False
    ZKEngine_Delete Empty
End Sub

Private Sub TimerDemoReg_Timer()
    TimerDemoReg.Enabled = False
    Dim TmpNum As Long
    TmpNum = RandomInt(1, 10)
    Select Case TmpNum
        Case Is <= 9
            ZKEngine_OnEnroll True, Array(CByte(RandomInt(0, 255)), CByte(RandomInt(0, 255)), _
            CByte(RandomInt(0, 255)), CByte(RandomInt(0, 255)), _
            CByte(RandomInt(0, 255)), CByte(RandomInt(0, 255)))
        Case Else
            ZKEngine_OnEnroll False, Empty
    End Select
End Sub
