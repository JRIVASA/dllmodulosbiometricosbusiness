VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmRacionamiento_Registro_Suprema_Config 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de CaptaHuellas"
   ClientHeight    =   5460
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5835
   Icon            =   "frmRacionamiento_Registro_Suprema_Config.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame FrameManoIzquierda 
      Caption         =   "    Mano Izquierda"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoMe�iqueIzquierdo 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularIzquierdo 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioIzquierdo 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceIzquierdo 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoPulgarIzquierdo 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   360
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.Frame FrameManoDerecha 
      Caption         =   "     Mano Derecha"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   3375
      Left            =   3120
      TabIndex        =   1
      Top             =   240
      Width           =   2415
      Begin VB.CheckBox cmdDedoPulgarDerecho 
         Caption         =   "Pulgar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2760
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoIndiceDerecho 
         Caption         =   "�ndice"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMedioDerecho 
         Caption         =   "Medio"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoAnularDerecho 
         Caption         =   "Anular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   960
         Width           =   1935
      End
      Begin VB.CheckBox cmdDedoMe�iqueDerecho 
         Caption         =   "Me�ique"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   360
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   360
         Width           =   1935
      End
      Begin VB.Line LineRellenoFrameIzquierdo 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   375
         Y1              =   120
         Y2              =   120
      End
   End
   Begin VB.CommandButton Close 
      BackColor       =   &H80000003&
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2160
      MaskColor       =   &H8000000D&
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4920
      Width           =   1455
   End
   Begin MSComctlLib.ProgressBar PgB 
      Height          =   165
      Left            =   360
      TabIndex        =   15
      Top             =   4710
      Visible         =   0   'False
      Width           =   5205
      _ExtentX        =   9181
      _ExtentY        =   291
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Label lblAccionDispositivo 
      BackStyle       =   0  'Transparent
      Caption         =   "Accion del Dispositivo: Listo para Detectar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   14
      Top             =   4440
      Width           =   5115
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblStatus 
      BackStyle       =   0  'Transparent
      Caption         =   "Pulse un bot�n disponible para comenzar."
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   270
      Left            =   360
      TabIndex        =   13
      Top             =   3960
      Width           =   5115
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmRacionamiento_Registro_Suprema_Config"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Public Enum BioTrack_CH_Dedos
'
'Me�iqueIzquierdo = 1
'AnularIzquierdo = 2
'MedioIzquierdo = 4
'IndiceIzquierdo = 8
'PulgarIzquierdo = 16
'
'PulgarDerecho = 32
'IndiceDerecho = 64
'MedioDerecho = 128
'AnularDerecho = 256
'Me�iqueDerecho = 512
'
'End Enum

Option Explicit

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Sub CopyMemory Lib "Kernel32.dll" Alias "RtlMoveMemory" (ByRef Destination As Any, ByRef Source As Any, ByVal Length As Long)

Public MaxEnrollFingerCount As Integer
Public EnrolledFingersMask As Integer
Public CurrentEnrollFingerMask As Integer
Public EnrollCount As Integer

Private isRegister As Boolean
Private EnrollSuccessCount As Integer
Private CancelEnroll As Boolean

Public RegistrandoMe�iqueIzquierdo As Boolean, BorrandoMe�iqueIzquierdo As Boolean
Public RegistrandoAnularIzquierdo As Boolean, BorrandoAnularIzquierdo As Boolean
Public RegistrandoMedioIzquierdo As Boolean, BorrandoMedioIzquierdo As Boolean
Public RegistrandoIndiceIzquierdo As Boolean, BorrandoIndiceIzquierdo As Boolean
Public RegistrandoPulgarIzquierdo As Boolean, BorrandoPulgarIzquierdo As Boolean

Public RegistrandoMe�iqueDerecho As Boolean, BorrandoMe�iqueDerecho As Boolean
Public RegistrandoAnularDerecho As Boolean, BorrandoAnularDerecho As Boolean
Public RegistrandoMedioDerecho As Boolean, BorrandoMedioDerecho As Boolean
Public RegistrandoIndiceDerecho As Boolean, BorrandoIndiceDerecho As Boolean
Public RegistrandoPulgarDerecho As Boolean, BorrandoPulgarDerecho As Boolean

Public Cargando As Boolean
Public Cerrar As Boolean
Public EvitarActivate As Boolean

Public ModoReconocimiento As Boolean
Public ModoReconocimiento_Respuesta As String

Private sStat As UFS_STATUS
Private mStat As UFM_STATUS
Private BioScanner As Long
Private BioID As Long
Private Matcher As Long
Private Const MaxTemplateSize = 1024
Private UniTemplate() As Byte, UniTemplatePart() As Byte
Private UniTemplateSize As Long
Private MultiTemplate(0, MaxTemplateSize - 1) As Byte
Private MultiTemplateSize() As Long
Private MinQuality As Long
Private Quality As Long
Private FileNumber As Long

Private Info_UltimaIdentificacion As String
Private Identificando As Boolean

Private Sub AccionesIdentificacion(Optional pRestablecer As Boolean = False)
    If pRestablecer Then
        Identificando = False
        CancelEnroll = False
        Me.Close.Caption = "Cerrar"
        Me.FrameManoIzquierda.Enabled = True
        Me.FrameManoDerecha.Enabled = True
    Else
        Identificando = True
        CancelEnroll = False
        Me.Close.Caption = "Cancelar"
        Me.FrameManoIzquierda.Enabled = False
        Me.FrameManoDerecha.Enabled = False
        Info_UltimaIdentificacion = ""
    End If
End Sub

Private Sub Close_Click()

    On Error GoTo Err

    If Not ModoReconocimiento Then
        Cerrar = False
        Unload Me
    Else
        CancelEnroll = True
    End If
 
    Exit Sub
    
Err:
    
    Resume Next
    
End Sub

Private Sub Close_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            CancelEnroll = True
    End Select
End Sub

Private Sub cmdDedoAnularDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularDerecho.value = vbChecked And Not RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoAnularDerecho = True: cmdDedoAnularDerecho.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'Para que no haga nada
        BorrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbChecked And RegistrandoAnularDerecho And Not BorrandoAnularDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoAnularDerecho = False
        Exit Sub
    End If
    
    If cmdDedoAnularDerecho.value = vbUnchecked And Not BorrandoAnularDerecho Then
        Me.CurrentEnrollFingerMask = CH_Dedos.AnularDerecho
        'para que no haga nada
        RegistrandoAnularDerecho = True
        cmdDedoAnularDerecho.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoAnularDerecho.value = vbUnchecked And BorrandoAnularDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoAnularDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoAnularIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoAnularIzquierdo.value = vbChecked And Not RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoAnularIzquierdo = True: cmdDedoAnularIzquierdo.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'Para que no haga nada
        BorrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbChecked And RegistrandoAnularIzquierdo And Not BorrandoAnularIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoAnularIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoAnularIzquierdo.value = vbUnchecked And Not BorrandoAnularIzquierdo Then
        Me.CurrentEnrollFingerMask = CH_Dedos.AnularIzquierdo
        'para que no haga nada
        RegistrandoAnularIzquierdo = True
        cmdDedoAnularIzquierdo.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoAnularIzquierdo.value = vbUnchecked And BorrandoAnularIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoAnularIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoIndiceDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceDerecho.value = vbChecked And Not RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoIndiceDerecho = True: cmdDedoIndiceDerecho.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'Para que no haga nada
        BorrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbChecked And RegistrandoIndiceDerecho And Not BorrandoIndiceDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoIndiceDerecho = False
        Exit Sub
    End If
    
    If cmdDedoIndiceDerecho.value = vbUnchecked And Not BorrandoIndiceDerecho Then
        Me.CurrentEnrollFingerMask = CH_Dedos.IndiceDerecho
        'para que no haga nada
        RegistrandoIndiceDerecho = True
        cmdDedoIndiceDerecho.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoIndiceDerecho.value = vbUnchecked And BorrandoIndiceDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoIndiceDerecho = False
        Exit Sub
    End If
    
End Sub

Private Sub cmdDedoIndiceIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoIndiceIzquierdo.value = vbChecked And Not RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoIndiceIzquierdo = True: cmdDedoIndiceIzquierdo.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'Para que no haga nada
        BorrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbChecked And RegistrandoIndiceIzquierdo And Not BorrandoIndiceIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoIndiceIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoIndiceIzquierdo.value = vbUnchecked And Not BorrandoIndiceIzquierdo Then
        Me.CurrentEnrollFingerMask = CH_Dedos.IndiceIzquierdo
        'para que no haga nada
        RegistrandoIndiceIzquierdo = True
        cmdDedoIndiceIzquierdo.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoIndiceIzquierdo.value = vbUnchecked And BorrandoIndiceIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoIndiceIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioDerecho.value = vbChecked And Not RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoMedioDerecho = True: cmdDedoMedioDerecho.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'Para que no haga nada
        BorrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbChecked And RegistrandoMedioDerecho And Not BorrandoMedioDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMedioDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMedioDerecho.value = vbUnchecked And Not BorrandoMedioDerecho Then
        Me.CurrentEnrollFingerMask = CH_Dedos.MedioDerecho
        'para que no haga nada
        RegistrandoMedioDerecho = True
        cmdDedoMedioDerecho.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMedioDerecho.value = vbUnchecked And BorrandoMedioDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMedioDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMedioIzquierdo_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMedioIzquierdo.value = vbChecked And Not RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoMedioIzquierdo = True: cmdDedoMedioIzquierdo.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'Para que no haga nada
        BorrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbChecked And RegistrandoMedioIzquierdo And Not BorrandoMedioIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMedioIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMedioIzquierdo.value = vbUnchecked And Not BorrandoMedioIzquierdo Then
        Me.CurrentEnrollFingerMask = CH_Dedos.MedioIzquierdo
        'para que no haga nada
        RegistrandoMedioIzquierdo = True
        cmdDedoMedioIzquierdo.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMedioIzquierdo.value = vbUnchecked And BorrandoMedioIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMedioIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueDerecho_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMe�iqueDerecho.value = vbChecked And Not RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoMe�iqueDerecho = True: cmdDedoMe�iqueDerecho.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'Para que no haga nada
        BorrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbChecked And RegistrandoMe�iqueDerecho And Not BorrandoMe�iqueDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMe�iqueDerecho = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueDerecho.value = vbUnchecked And Not BorrandoMe�iqueDerecho Then
        Me.CurrentEnrollFingerMask = CH_Dedos.Me�iqueDerecho
        'para que no haga nada
        RegistrandoMe�iqueDerecho = True
        cmdDedoMe�iqueDerecho.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMe�iqueDerecho.value = vbUnchecked And BorrandoMe�iqueDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMe�iqueDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoMe�iqueIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoMe�iqueIzquierdo.value = vbChecked And Not RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoMe�iqueIzquierdo = True: cmdDedoMe�iqueIzquierdo.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'Para que no haga nada
        BorrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbChecked And RegistrandoMe�iqueIzquierdo And Not BorrandoMe�iqueIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoMe�iqueIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoMe�iqueIzquierdo.value = vbUnchecked And Not BorrandoMe�iqueIzquierdo Then
        Me.CurrentEnrollFingerMask = CH_Dedos.Me�iqueIzquierdo
        'para que no haga nada
        RegistrandoMe�iqueIzquierdo = True
        cmdDedoMe�iqueIzquierdo.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoMe�iqueIzquierdo.value = vbUnchecked And BorrandoMe�iqueIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoMe�iqueIzquierdo = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarDerecho_Click()

    Me.Close.SetFocus
    
    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoPulgarDerecho.value = vbChecked And Not RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoPulgarDerecho = True: cmdDedoPulgarDerecho.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'Para que no haga nada
        BorrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbChecked And RegistrandoPulgarDerecho And Not BorrandoPulgarDerecho Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoPulgarDerecho = False
        Exit Sub
    End If
    
    If cmdDedoPulgarDerecho.value = vbUnchecked And Not BorrandoPulgarDerecho Then
        Me.CurrentEnrollFingerMask = CH_Dedos.PulgarDerecho
        'para que no haga nada
        RegistrandoPulgarDerecho = True
        cmdDedoPulgarDerecho.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoPulgarDerecho.value = vbUnchecked And BorrandoPulgarDerecho Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoPulgarDerecho = False
        Exit Sub
    End If

End Sub

Private Sub cmdDedoPulgarIzquierdo_Click()

    Me.Close.SetFocus

    If Cargando Then
        Cargando = False
        Exit Sub
    End If

    If cmdDedoPulgarIzquierdo.value = vbChecked And Not RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        If Not VerificarMaxHuellas Then IgnorarActivate: Mensaje True, "Ha alcanzado el l�mite de huellas que puede registrar.": BorrandoPulgarIzquierdo = True: cmdDedoPulgarIzquierdo.value = vbUnchecked: lblStatus.Caption = "": Exit Sub
        Me.CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'Para que no haga nada
        BorrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbUnchecked
        lblStatus.Caption = "Registrando... Presione ESC si desea cancelar."
        lblAccionDispositivo.Caption = "Listo para detectar."
        InitEnrollment
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbChecked And RegistrandoPulgarIzquierdo And Not BorrandoPulgarIzquierdo Then
        'Permanecer Chequeado
        lblStatus.Caption = "Huella registrada correctamente."
        lblAccionDispositivo.Caption = ""
        RegistrandoPulgarIzquierdo = False
        Exit Sub
    End If
    
    If cmdDedoPulgarIzquierdo.value = vbUnchecked And Not BorrandoPulgarIzquierdo Then
        Me.CurrentEnrollFingerMask = CH_Dedos.PulgarIzquierdo
        'para que no haga nada
        RegistrandoPulgarIzquierdo = True
        cmdDedoPulgarIzquierdo.value = vbChecked
        If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pPermiteBorrarHuellasRegistradas Then ZKEngine_Delete (Me.CurrentEnrollFingerMask)
        Exit Sub
    ElseIf cmdDedoPulgarIzquierdo.value = vbUnchecked And BorrandoPulgarIzquierdo Then
        'Permanecer Deschequeado
        lblStatus.Caption = "Registro Eliminado."
        lblAccionDispositivo.Caption = ""
        BorrandoPulgarIzquierdo = False
        Exit Sub
    End If

End Sub

Private Function GetCurrentScannerHandle(ByRef hScanner As Long) As Boolean
    
    sStat = UFS_GetScannerHandle(0, hScanner)
    If (sStat = UFS_STATUS.OK) Then
        GetCurrentScannerHandle = True
        Exit Function
    Else
        GetCurrentScannerHandle = False
        Exit Function
    End If
    
End Function

Private Sub SetMatcherSettings(ByVal hMatcher As Long)

    Dim value As Long
    value = 6 'Especificaci�n SIVPRE.
    
    ' Security level ranges from 1 to 7
    mStat = UFM_SetParameter(hMatcher, UFM_PARAM.SECURITY_LEVEL, value)
    
    value = 1
    
    mStat = UFM_SetParameter(hMatcher, UFM_PARAM.FAST_MODE, value)

End Sub

Private Sub SetScannerStandard()
    Select Case frmRacionamiento_Registro_de_Clientes_con_Biometrico.pFormatoHuella
        Case 0
            sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_SUPREMA)
        Case 1
            sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_ANSI378)
        Case 2
            sStat = UFS_SetTemplateType(BioScanner, UFS_TEMPLATE_TYPE.UFS_TEMPLATE_TYPE_ISO19794_2)
    End Select
End Sub

Private Sub SetMatcherStandard()
    Select Case frmRacionamiento_Registro_de_Clientes_con_Biometrico.pFormatoHuella
        Case 0
            mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_SUPREMA)
        Case 1
            mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ANSI378)
        Case 2
            mStat = UFM_SetTemplateType(Matcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ISO19794_2)
    End Select
End Sub

Private Sub Form_Activate()

    ' Set properties to UFS object.
    
    If EvitarActivate Then EvitarActivate = False: Exit Sub
     
    On Error Resume Next
    
    sStat = UFS_Init
    
    If sStat <> UFS_STATUS.OK Then
        Mensaje True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificaci�n."
        If ModoReconocimiento Then Cerrar = False: ModoReconocimiento_Respuesta = "Error de inicializacion"
        Unload Me
        Exit Sub
    End If
    
    sStat = UFS_GetScannerNumber(BioID)
    
    If sStat <> UFS_STATUS.OK Then
        Mensaje True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificaci�n."
        If ModoReconocimiento Then Cerrar = False: ModoReconocimiento_Respuesta = "Error de inicializacion"
        Unload Me
        Exit Sub
    End If
    
    If Not GetCurrentScannerHandle(BioScanner) Then
        Mensaje True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificaci�n."
        If ModoReconocimiento Then Cerrar = False: ModoReconocimiento_Respuesta = "Error de inicializacion"
        Unload Me
        Exit Sub
    End If
    
    UFS_ClearCaptureImageBuffer (BioScanner)
    
    Me.EnrollCount = frmRacionamiento_Registro_de_Clientes_con_Biometrico.pNroIntentosRegistro
    MinQuality = frmRacionamiento_Registro_de_Clientes_con_Biometrico.pCalidadMinimaHuella
    
    sStat = UFS_SetParameter(BioScanner, UFS_PARAM.TIMEOUT, 3500)
    
    SetScannerStandard
    
    mStat = UFM_Create(Matcher)
    
    If mStat <> UFM_STATUS.OK Then
        Mensaje True, "Existe un problema con el dispositivo, no se pudo iniciar el motor de verificaci�n."
        If ModoReconocimiento Then Cerrar = False: ModoReconocimiento_Respuesta = "Error de inicializacion"
        Unload Me
        Exit Sub
    End If
    
    SetMatcherStandard
    SetMatcherSettings (Matcher)
     
    If Not ModoReconocimiento Then
        'Me.MaxEnrollFingerCount = CInt(ficha_PerfilesCaptaHuellas.txt_LimiteHuellas)
        
        Me.EnrolledFingersMask = CInt(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtPlantilla.Text)
        
        'Marcar los que Apliquen.
        SetUpMask
    Else
        Call ZKEngine_OnCaptureToFile(True)
    End If
    
End Sub

Private Function IgnorarActivate() As Boolean
    IgnorarActivate = True: EvitarActivate = True
End Function

Private Sub ResetearStatus()
    lblStatus.Caption = "Pulse un bot�n disponible para comenzar."
    lblAccionDispositivo.Caption = "Accion del Dispositivo: Listo para Detectar"
End Sub

Private Sub SetUpMask()

    Dim Mask As Integer
    Mask = Me.EnrolledFingersMask
    
    'With CH_Dedos
    
        If Mask - CH_Dedos.Me�iqueDerecho >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueDerecho
            Cargando = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularDerecho >= 0 Then
            Mask = Mask - CH_Dedos.AnularDerecho
            Cargando = True
            cmdDedoAnularDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioDerecho >= 0 Then
            Mask = Mask - CH_Dedos.MedioDerecho
            Cargando = True
            cmdDedoMedioDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceDerecho >= 0 Then
            Mask = Mask - CH_Dedos.IndiceDerecho
            Cargando = True
            cmdDedoIndiceDerecho.value = vbChecked
        End If
        
        If Mask - CH_Dedos.PulgarDerecho >= 0 Then
            Mask = Mask - CH_Dedos.PulgarDerecho
            Cargando = True
            cmdDedoPulgarDerecho.value = vbChecked
        End If
        
        '-------------------------------------------'
        
        If Mask - CH_Dedos.PulgarIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.PulgarIzquierdo
            Cargando = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.IndiceIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.IndiceIzquierdo
            Cargando = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.MedioIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.MedioIzquierdo
            Cargando = True
            cmdDedoMedioIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.AnularIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.AnularIzquierdo
            Cargando = True
            cmdDedoAnularIzquierdo.value = vbChecked
        End If
        
        If Mask - CH_Dedos.Me�iqueIzquierdo >= 0 Then
            Mask = Mask - CH_Dedos.Me�iqueIzquierdo
            Cargando = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        End If
    
        Cargando = False
    
    'End With

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            CancelEnroll = True
    End Select
End Sub

Private Sub Form_Load()

    If ModoReconocimiento Then
        Me.FrameManoDerecha.Enabled = False: Me.FrameManoDerecha.Visible = False
        Me.FrameManoIzquierda.Enabled = False: Me.FrameManoIzquierda.Visible = False
        Me.lblStatus.Caption = "Coloque la huella. Presione ESC para cancelar."
        Me.lblAccionDispositivo.Caption = "Accion del Dispositivo: Listo para Capturar."
        Me.Height = Me.Height - Me.FrameManoDerecha.Height - 200
        Me.lblStatus.Top = Me.lblStatus.Top - Me.FrameManoDerecha.Height - 200
        Me.lblAccionDispositivo.Top = Me.lblAccionDispositivo.Top - Me.FrameManoDerecha.Height - 200
        Me.PgB.Top = Me.lblAccionDispositivo.Top - 200
        Me.Close.Top = Me.Close.Top - Me.FrameManoDerecha.Height - 200
        Exit Sub
    End If
    
    If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pDedosPermitidos <> "" And _
    frmRacionamiento_Registro_de_Clientes_con_Biometrico.pDedosPermitidos Like "*|*" Then
    
        cmdDedoMe�iqueIzquierdo.Enabled = False
        cmdDedoAnularIzquierdo.Enabled = False
        cmdDedoMedioIzquierdo.Enabled = False
        cmdDedoIndiceIzquierdo.Enabled = False
        cmdDedoPulgarIzquierdo.Enabled = False
        
        cmdDedoMe�iqueDerecho.Enabled = False
        cmdDedoAnularDerecho.Enabled = False
        cmdDedoMedioDerecho.Enabled = False
        cmdDedoIndiceDerecho.Enabled = False
        cmdDedoPulgarDerecho.Enabled = False
        
        Dim Tmp As Variant, Dedo As Variant
         
        Tmp = Split(frmRacionamiento_Registro_de_Clientes_con_Biometrico.pDedosPermitidos, "|")
        
        For Each Dedo In Tmp
            
            If Trim(UCase(Dedo)) = UCase("Me�ique Izquierdo") Then
                cmdDedoMe�iqueIzquierdo.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Anular Izquierdo") Then
                cmdDedoAnularIzquierdo.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Medio Izquierdo") Then
                cmdDedoMedioIzquierdo.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Indice Izquierdo") Then
                cmdDedoIndiceIzquierdo.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Pulgar Izquierdo") Then
                cmdDedoPulgarIzquierdo.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Me�ique Derecho") Then
                cmdDedoMe�iqueDerecho.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Anular Derecho") Then
                cmdDedoAnularDerecho.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Medio Derecho") Then
                cmdDedoMedioDerecho.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Indice Derecho") Then
                cmdDedoIndiceDerecho.Enabled = True
            ElseIf Trim(UCase(Dedo)) = UCase("Pulgar Derecho") Then
                cmdDedoPulgarDerecho.Enabled = True
            End If
            
        Next
    
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
 
    If Identificando Then CancelEnroll = True: Cancel = 1: Exit Sub
 
    If ModoReconocimiento Then
        If Not Cerrar Then
            Cancel = 1
            
            If ModoReconocimiento_Respuesta = "" Then
                ModoReconocimiento_Respuesta = "Captura de datos cancelada"
            End If
            
            If Me.Visible Then Me.Hide
        Else
        
            UFS_Uninit
    
            UFM_Delete (Matcher)
        
            Set frmRacionamiento_Registro_Suprema_Config = Nothing
            
        End If
    Else
        'Show new fingerprint mask.
        frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtPlantilla.Text = CStr(Me.EnrolledFingersMask)
        
        UFS_Uninit
    
        UFM_Delete (Matcher)
        
        Set frmRacionamiento_Registro_Suprema_Config = Nothing
    End If
    
    CancelEnroll = True

End Sub

Private Function InitEnrollment() As Boolean

    isRegister = True
    
    Do While Not EnrollProc
        DoEvents
        If CancelEnroll Then CancelEnroll = False: Exit Do
        DoEvents
    Loop
    
    isRegister = False: EnrollSuccessCount = 0: ResetearStatus
        
End Function

Private Function VerificarCliente(pID_A_Verificar As String, Optional PgB As ProgressBar, Optional Aparentar As Boolean = False) As String
    
    On Error GoTo ErrIdent
    
    Dim mRsHuellas As New ADODB.Recordset
    Dim mSql As String
    
    'Dim MatchInfo As New Collection
    Dim MatchCount As Long
    Dim Match As Long, I As Long
    
    Dim TmpTemplate() As Byte, TmpTemplateSize As Long
    
    SetMatcherStandard
    
    'Test
    Dim TimeIni As Date, TimeEnd As Date, TimeSegs As Long
    TimeIni = DateTime.Now
    TimeEnd = TimeIni
    TimeSegs = 0
    
    'If Aparentar Then 'C�digo mara�a experimental.
    
        'lblStatus.Caption = "Identificando... Por favor espere."
    
        'Dim MaxTiempoEstimado As Double
        
        'MaxTiempoEstimado = UniTemplateSize * (0.00000001142 * _
        'CDbl(frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute( _
        '"SELECT SUM(n_TemplateSize) as Bytes FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS")!Bytes))
    
        'MaxTiempoEstimado = Fix(MaxTiempoEstimado / 5)
              
    'End If
    
    'If Not frmRacionamiento_Registro_de_Clientes_con_Biometrico.InterfazOperador Then
        'If frmRacionamiento_Registro_de_Clientes_con_Biometrico.VerificandoOperador Then
            'Dim mRsOperador As ADODB.Recordset
            
            'Set mRsOperador = frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute( _
            '"SELECT * FROM MA_RACIONALIZACIOn_ClienteS WHERE c_Rif = '" & pID_A_Verificar & "'")
            
            'If mRsOperador.EOF Then
                'VerificarCliente = "No es Operador"
                'Exit Function
            'Else
                'If Not CBool(mRsOperador!b_Operador) Then
                    'VerificarCliente = "No es Operador"
                    'Exit Function
                'End If
            'End If
            
            'mRsOperador.Close
        'End If
    'End If
    
    mSql = "SELECT Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS WHERE c_CodCliente = '" & pID_A_Verificar & "'"
    
    DoEvents
    
    mRsHuellas.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenStatic, adLockReadOnly, adCmdText
    
    DoEvents

    'Set MatchInfo = New Collection
    
    If Not mRsHuellas.EOF Then
        
        If Not IsMissing(PgB) Then
            'If Not Aparentar Then
                PgB.Max = mRsHuellas.RecordCount
            'Else
                'PgB.max = MaxTiempoEstimado
            'End If
            PgB.mIn = 0
            PgB.value = 0
            PgB.Appearance = cc3D
            PgB.Visible = True
        End If
        
        While Not mRsHuellas.EOF
            
            DoEvents
            
            TmpTemplateSize = mRsHuellas!n_TemplateSize 'Len(mRsHuellas!BIN_DATA)
            TmpTemplate = mRsHuellas!bin_DataArray 'ReDim TmpTemplate(MaxTemplateSize - 1)
            
            'For I = 1 To MaxTemplateSize
               'Debug.Print TmpTemplate(I - 1) 'TmpTemplate(I - 1) = Asc(Mid(mRsHuellas!BIN_DATA, I, 1))
            'Next I
            
            mStat = UFM_Verify(Matcher, UniTemplatePart(0), UniTemplateSize, TmpTemplate(0), TmpTemplateSize, Match)
            
            If CBool(Match) Then
                'If Not Collection_ExisteKey(MatchInfo, mRsHuellas!c_CodCliente) Then MatchInfo.add CStr(mRsHuellas!c_CodCliente), CStr(mRsHuellas!c_CodCliente)
                MatchCount = MatchCount + 1
            End If
            
            If Not IsMissing(PgB) Then
                'If Not Aparentar Then
                    PgB.value = PgB.value + 1
                'End If
            End If
            
            mRsHuellas.MoveNext
        Wend
    Else
        VerificarCliente = "Proceder a Registrar" 'No hay registros a�n.
    End If
    
    'If Aparentar Then
        'For i = 1 To MaxTiempoEstimado
            'DoEvents
            'Sleep 1000
            'DoEvents
            'PgB.value = PgB.value + 1
        'Next i
    'End If
    
    mRsHuellas.Close
    
    'Test
    TimeEnd = DateTime.Now
    TimeSegs = DateDiff("s", TimeIni, TimeEnd)
    Debug.Print TimeSegs
    
    If MatchCount <= 0 Then
        VerificarCliente = "Proceder a Registrar"
    ElseIf MatchCount = 1 Then 'And MatchInfo.Count = 1 Then
        'If UCase(MatchInfo.Item(1)) <> UCase(pID_A_Verificar) Then
            'IdentificarCliente = "Robo de Identidad" 'Chequear
        'Else
            VerificarCliente = "Cliente confirmado"
        'End If
    'ElseIf MatchCount <= 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente = "Robo de Identidad" 'Chequear
    'ElseIf MatchCount >= 2 And MatchInfo.Count = 1 Then
        'IdentificarCliente = "Registros multiples" ' Chequear...
    'ElseIf MatchCount > 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente = "Registros Multiples y Robo de Identidad" 'Chequear
    ElseIf MatchCount > 1 Then
        VerificarCliente = "Registros Multiples"
    End If
    
HidePgB:

    If Not IsMissing(PgB) Then
        PgB.Max = 100
        PgB.mIn = 0
        PgB.value = 0
        PgB.Visible = False
    End If
    
    Exit Function
    
ErrIdent:
    
    'Debug.Print Err.Description
    
    MsjErrorRapido Err.Description, "Ha ocurrido un error en la rutina de verificaci�n, por favor reporte lo siguiente:" & vbNewLine & vbNewLine
    
    VerificarCliente = "Error en la verificacion"
    
    GoTo HidePgB
    
End Function

Private Function IdentificarCliente_Secuencial(pID_A_Verificar As String, Optional PgB As ProgressBar) As String
    
    On Error GoTo ErrIdent
    
    lblStatus.Caption = "Identificando... Por favor espere."
    
    AccionesIdentificacion
    
    Dim mRsHuellas As New ADODB.Recordset
    Dim mSql As String
    
    Dim MatchInfo As New Collection, MatchCount As Long
    Dim Match As Long, I As Long
    
    Dim TmpTemplate() As Byte, TmpTemplateSize As Long
    
    SetMatcherStandard
    
    'Test
    Dim TimeIni As Date, TimeEnd As Date, TimeSegs As Long
    TimeIni = DateTime.Now
    TimeEnd = TimeIni
    TimeSegs = 0
    
    'mRsHuellas.CursorLocation = adUseClient
    
    Select Case frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion
        Case 0
            mSql = "SELECT c_CodCliente, Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS"
        Case 1
            mSql = "SELECT c_CodCliente, Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS ORDER BY c_CodCliente ASC"
        Case 2
            mSql = "SELECT c_CodCliente, Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS ORDER BY NewID()"
        Case 3
            mSql = "SELECT c_CodCliente, Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS ORDER BY RandomOrder ASC"
    End Select
    
    DoEvents
    
    mRsHuellas.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenStatic, adLockReadOnly, adCmdText
    
    DoEvents
    
    'Set mRsHuellas.ActiveConnection = Nothing
    
    Set MatchInfo = New Collection
    
    If Not mRsHuellas.EOF Then
        
        If Not IsMissing(PgB) Then
            PgB.Max = mRsHuellas.RecordCount
            PgB.mIn = 0
            PgB.value = 0
            PgB.Appearance = cc3D
            PgB.Visible = True
        End If
        
        Do While Not mRsHuellas.EOF
            
            DoEvents
            
            If CancelEnroll Then Exit Do
            
            TmpTemplateSize = mRsHuellas!n_TemplateSize 'Len(mRsHuellas!BIN_DATA)
            TmpTemplate = mRsHuellas!bin_DataArray 'ReDim TmpTemplate(MaxTemplateSize - 1)
            
            'For I = 1 To MaxTemplateSize
               'Debug.Print TmpTemplate(I - 1) 'TmpTemplate(I - 1) = Asc(Mid(mRsHuellas!BIN_DATA, I, 1))
            'Next I
            
            mStat = UFM_Verify(Matcher, UniTemplate(0), UniTemplateSize, TmpTemplate(0), TmpTemplateSize, Match)
            
            If CBool(Match) Then
                If Not Collection_ExisteKey(MatchInfo, mRsHuellas!c_CodCliente) Then MatchInfo.Add CStr(mRsHuellas!c_CodCliente), CStr(mRsHuellas!c_CodCliente)
                MatchCount = MatchCount + 1
                GoTo encontrado
            End If
            
            If Not IsMissing(PgB) Then
                PgB.value = PgB.value + 1
            End If
            
            mRsHuellas.MoveNext
            
        Loop
    Else
        IdentificarCliente_Secuencial = "Proceder a Registrar" 'No hay registros a�n.
    End If

encontrado:
    
    If CancelEnroll Then
        GoTo HidePgB
    End If
    
    'Test
    TimeEnd = DateTime.Now
    TimeSegs = DateDiff("s", TimeIni, TimeEnd)
    Debug.Print TimeSegs
    
    If MatchCount <= 0 Then
        IdentificarCliente_Secuencial = "Proceder a Registrar"
    ElseIf MatchCount = 1 And MatchInfo.Count = 1 Then
        If UCase(MatchInfo.Item(1)) <> UCase(pID_A_Verificar) Then
            IdentificarCliente_Secuencial = "Robo de Identidad" 'Chequear
        Else
            IdentificarCliente_Secuencial = "Cliente confirmado"
        End If
    'ElseIf MatchCount <= 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_Secuencial = "Robo de Identidad" 'Chequear
    'ElseIf MatchCount >= 2 And MatchInfo.Count = 1 Then
        'IdentificarCliente_Secuencial = "Registros multiples" ' Chequear...
    'ElseIf MatchCount > 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_Secuencial = "Registros Multiples y Robo de Identidad" 'Chequear
    End If
    
HidePgB:

    'Dejar todo en orden.

    If Not IsMissing(PgB) Then
        PgB.Max = 100
        PgB.mIn = 0
        PgB.value = 0
        PgB.Visible = False
    End If
    
    If mRsHuellas.State = adStateOpen Then mRsHuellas.Close
    
    AccionesIdentificacion True
    
    Exit Function
    
ErrIdent:
    
    'Debug.Print Err.Description
    
    MsjErrorRapido Err.Description, "Ha ocurrido un error en la rutina de identificaci�n, por favor reporte lo siguiente:" & vbNewLine & vbNewLine
    
    IdentificarCliente_Secuencial = "Error en la identificacion"
    
    GoTo HidePgB
    
End Function

Private Function IdentificarCliente_PorPartes(pID_A_Verificar As String, PgB As ProgressBar) As String
    
    On Error GoTo ErrIdent
    
    DoEvents
    
    lblStatus.Caption = "Identificando... Por favor espere."
    
    AccionesIdentificacion
    
    Dim mRsHuellas As New ADODB.Recordset
    Dim mSql As String, mOrden As String
    
    Dim MatchInfo As New Collection, MatchCount As Long
    Dim Match As Long, I As Long
    Dim nRegistros As Double, TopReg As Double, CurrentReg As Double
    Dim TmpTemplate() As Byte, TmpTemplateSize As Long
    Dim PrimerGrupo As Boolean
    
    Dim ArrMatrix() As Byte
    Dim tmpArr() As Byte
    'Dim ArrHuellas() As Variant
    Dim ArrTemplateSizes() As Long
    Dim ArrMemoryAddresses() As Long
    
    'Test
    Dim TimeIni As Date, TimeEnd As Date, TimeSegs As Long
    TimeIni = DateTime.Now
    TimeEnd = TimeIni
    TimeSegs = 0
    
    mRsHuellas.CursorLocation = adUseClient
    
    TopReg = frmRacionamiento_Registro_de_Clientes_con_Biometrico.NumRegxLote_Identificacion
    
    'If Not IsMissing(PgB) Then
        nRegistros = frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute("SELECT COUNT(ID) AS nReg FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS")!nReg
        PgB.Max = IIf(nRegistros <= 0, 1, nRegistros)
        PgB.mIn = 0
        PgB.value = 0
        PgB.Appearance = cc3D
        PgB.Visible = True
    'End If
    
    Dim ColumnasRequeridas As String
    ColumnasRequeridas = "c_CodCliente, Bin_Data, n_TemplateSize"
    Dim ColumnaID As String
    ColumnaID = "ID"
    Dim TablaTemporal As String
    TablaTemporal = "HUELLAS_PROCESADAS_" & Replace(gRutinas.NombreDelComputador, "-", "_")
    
    PrimerGrupo = True
    
    Set MatchInfo = New Collection
    
    SetMatcherSettings (Matcher)
    SetMatcherStandard
    
    Debug.Print vbNewLine & "Size de Comparaci�n: " & UniTemplateSize
    
    Do While Not CurrentReg >= nRegistros And nRegistros > 0
    
        If PrimerGrupo Then
        
            If ExisteCampoTabla("ID", , "SELECT ID FROM " & TablaTemporal, , frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion) Then
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ("DROP TABLE " & TablaTemporal)
            End If

            Select Case frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion
                Case 0
                    mOrden = ""
                    mSql = "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS" & mOrden
                Case 1
                    mOrden = "ORDER BY c_CodCliente ASC"
                    mSql = "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " & mOrden
                Case 2
                    mOrden = "ORDER BY NewID()"
                    mSql = "SELECT " & ColumnasRequeridas & " FROM " & TablaTemporal & "_TMP_ID"
                Case 3
                    mOrden = "ORDER BY RandomOrder ASC"
                    mSql = "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " & mOrden
            End Select
            
            'Debug.Print mSql
            
            If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion = 2 Then
            
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "SELECT " & ColumnaID & " INTO " & TablaTemporal & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS WHERE 1 = 2")
    
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "SET IDENTITY_INSERT " & TablaTemporal & " ON")
            
                If ExisteCampoTabla("ID", , "SELECT ID FROM " & TablaTemporal & "_TMP_ID", , frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion) Then
                    frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ("DROP TABLE " & TablaTemporal & "_TMP_ID")
                End If
            
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "SELECT " & ColumnasRequeridas & ", " & ColumnaID & "  INTO " & TablaTemporal & "_TMP_ID" & " FROM (" _
                & "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & ", " & ColumnaID & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " _
                & "WHERE " & ColumnaID & " NOT IN (SELECT " & ColumnaID & " FROM " & TablaTemporal & ") " & mOrden & ") TB")
   
            End If
            
        Else
        
            If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion = 2 Then
                
                If ExisteCampoTabla("ID", , "SELECT ID FROM " & TablaTemporal & "_TMP_ID", , frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion) Then
                    frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ("DROP TABLE " & TablaTemporal & "_TMP_ID")
                End If
            
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "SELECT " & ColumnasRequeridas & ", " & ColumnaID & "  INTO " & TablaTemporal & "_TMP_ID" & " FROM (" _
                & "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & ", " & ColumnaID & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " _
                & "WHERE " & ColumnaID & " NOT IN (SELECT " & ColumnaID & " FROM " & TablaTemporal & ") " & mOrden & ") TB")
                
            Else
        
                mSql = "SELECT TOP (" & TopReg & ") " & ColumnasRequeridas & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " _
                & "WHERE " & ColumnaID & " NOT IN (SELECT " & ColumnaID & " FROM " & TablaTemporal & ") " & mOrden
            
            End If
        
        End If
        
        DoEvents
    
        lblAccionDispositivo.Caption = "Obteniendo registros..."
    
        DoEvents
        
        mRsHuellas.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenStatic, adLockReadOnly, adCmdText
        
        DoEvents
  
        nRegistros = frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute("SELECT COUNT(ID) AS nReg FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS")!nReg
        PgB.Max = IIf(nRegistros <= 0, 1, nRegistros)

        Set mRsHuellas.ActiveConnection = Nothing
        
        If Not mRsHuellas.EOF Then
        
            ReDim ArrMatrix(MaxTemplateSize - 1, mRsHuellas.RecordCount - 1) As Byte
            'ReDim ArrHuellas(mRsHuellas.RecordCount - 1) As Variant
            ReDim ArrTemplateSizes(mRsHuellas.RecordCount - 1) As Long
            ReDim ArrMemoryAddresses(mRsHuellas.RecordCount - 1) As Long
            
            Dim TmpCount As Long
            
            mRsHuellas.MoveFirst
        
            For TmpCount = 0 To mRsHuellas.RecordCount - 1
                
                DoEvents
                
                If CancelEnroll Then Exit Do
                
                tmpArr = StrConv(mRsHuellas!BIN_DATA, vbFromUnicode) 'ArrHuellas(TmpCount) = mRsHuellas!bin_DataArray
                ArrTemplateSizes(TmpCount) = mRsHuellas!n_TemplateSize
                Call CopyMemory(ByVal VarPtr(ArrMatrix(0, TmpCount)), ByVal VarPtr(tmpArr(0)), ArrTemplateSizes(TmpCount))
                ArrMemoryAddresses(TmpCount) = VarPtr(ArrMatrix(0, TmpCount))
                   
                'If Not IsMissing(PgB) Then
                    PgB.value = PgB.value + 1
                    'Debug.Print PgB.value
                'End If
                
                mRsHuellas.MoveNext
                
            Next
            
            DoEvents
            
            lblAccionDispositivo.Caption = "Comparando huellas dactilares..."
            
            DoEvents
            
            mStat = UFM_IdentifyMT(Matcher, UniTemplate(0), UniTemplateSize, ArrMemoryAddresses(0), ArrTemplateSizes(0), UBound(ArrMemoryAddresses) + 1, 0, Match)
            
            If Match <> -1 Then
                mRsHuellas.Move Match, 1
                If Not Collection_ExisteKey(MatchInfo, mRsHuellas!c_CodCliente) Then MatchInfo.Add CStr(mRsHuellas!c_CodCliente), CStr(mRsHuellas!c_CodCliente)
                MatchCount = MatchCount + 1
                GoTo encontrado
            End If
            
            CurrentReg = CurrentReg + mRsHuellas.RecordCount
            
        End If
        
        mRsHuellas.Close
        
        DoEvents
        
        lblAccionDispositivo.Caption = "Procesando pr�ximo lote de registros..."
        
        DoEvents
        
        If PrimerGrupo Then
        
            If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion = 2 Then

                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "INSERT INTO " & TablaTemporal & " (" & ColumnaID & ") (" _
                & "SELECT " & ColumnaID & " FROM " & TablaTemporal & "_TMP_ID)")

            Else
                
                'frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                '"INSERT INTO " & TablaTemporal & " (" & ColumnaID & ") (" _
                '& "SELECT " & ColumnaID & " FROM " & TablaTemporal & "_TMP_ID)")

                'frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                '"SET IDENTITY_INSERT " & TablaTemporal & " ON")

                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "CREATE TABLE [dbo].[" & TablaTemporal & "](" _
                & vbNewLine & vbTab & "[" & ColumnaID & "] [NUMERIC](18,0) NOT NULL," _
                & ")")
                
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "INSERT INTO " & TablaTemporal & " (" & ColumnaID & ") (" _
                & "SELECT TOP (" & TopReg & ") " & ColumnaID & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " _
                & "WHERE " & ColumnaID & " NOT IN (SELECT " & ColumnaID & " FROM " & TablaTemporal & ")) " & mOrden)
                    
            End If
            
            PrimerGrupo = False
            
        Else
            
            If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pOrdenIdentificacion = 2 Then

                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "INSERT INTO " & TablaTemporal & " (" & ColumnaID & ") (" _
                & "SELECT " & ColumnaID & " FROM " & TablaTemporal & "_TMP_ID)")

            Else
        
                'frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                '"SET IDENTITY_INSERT " & TablaTemporal & " ON")
        
                frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
                "INSERT INTO " & TablaTemporal & " (" & ColumnaID & ") (" _
                & "SELECT TOP (" & TopReg & ") " & ColumnaID & " FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS " _
                & "WHERE " & ColumnaID & " NOT IN (SELECT " & ColumnaID & " FROM " & TablaTemporal & ")) " & mOrden)
                
            End If
            
        End If
    
    Loop

encontrado:
    
    If CancelEnroll Then
        GoTo HidePgB
    End If
    
    'Test
    TimeEnd = DateTime.Now
    TimeSegs = DateDiff("s", TimeIni, TimeEnd)
    Debug.Print vbNewLine & "Tiempo transcurrido: " & TimeSegs & " Segundos (Alrededor de " & Fix(TimeSegs / 60) & " minutos)."
    
    Info_UltimaIdentificacion = "Calidad de la huella procesada: " & Quality & "." _
    & vbNewLine & "Tama�o de la huella procesada: " & UniTemplateSize & " Bytes." _
    & vbNewLine & "Tiempo transcurrido: " & TimeSegs & " Segundos (" & Fix(TimeSegs / 60) & " minutos y " & Fix((TimeSegs Mod 60)) & " segundos)."
    
    If MatchCount <= 0 Then
        IdentificarCliente_PorPartes = "Proceder a Registrar"
        Info_UltimaIdentificacion = Info_UltimaIdentificacion & vbNewLine & "Identidad encontrada: Ninguna. Huella registrada."
    ElseIf MatchCount = 1 And MatchInfo.Count = 1 Then
        If UCase(MatchInfo.Item(1)) <> UCase(pID_A_Verificar) Then
            IdentificarCliente_PorPartes = "Robo de Identidad" 'Chequear
            Info_UltimaIdentificacion = Info_UltimaIdentificacion & vbNewLine & "Identidad encontrada: " & MatchInfo.Item(1) & ". La huella no coincide con la c�dula ingresada."
        Else
            IdentificarCliente_PorPartes = "Cliente confirmado"
            Info_UltimaIdentificacion = Info_UltimaIdentificacion & vbNewLine & "Identidad encontrada: " & pID_A_Verificar & ". Cliente confirmado."
        End If
    'ElseIf MatchCount <= 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_PorPartes = "Robo de Identidad" 'Chequear
    'ElseIf MatchCount >= 2 And MatchInfo.Count = 1 Then
        'IdentificarCliente_PorPartes = "Registros multiples" ' Chequear...
    'ElseIf MatchCount > 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_PorPartes = "Registros Multiples y Robo de Identidad" 'Chequear
    End If
    
HidePgB:

    'Dejar todo en orden.

    'If Not IsMissing(PgB) Then
        PgB.Max = 100
        PgB.mIn = 0
        PgB.value = 0
        PgB.Visible = False
    'End If
    
    'Liberar la Memoria
    
    If mRsHuellas.State = adStateOpen Then mRsHuellas.Close
    
    ReDim ArrMatrix(0, 0) As Byte
    'ReDim ArrHuellas(0) As Variant
    ReDim ArrTemplateSizes(0) As Long
    ReDim ArrMemoryAddresses(0) As Long

    AccionesIdentificacion True
    
    Exit Function
    
ErrIdent:
    
    'Debug.Print Err.Description
    
    IgnorarActivate
    
    MsjErrorRapido Err.Description, "Ha ocurrido un error en la rutina de identificaci�n por partes, por favor reporte lo siguiente:" & vbNewLine & vbNewLine
    
    On Error GoTo ErrDropTable
    frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ("DROP TABLE " & TablaTemporal)
    
ErrDropTable:

    IdentificarCliente_PorPartes = "Error en la identificacion"
    
    Info_UltimaIdentificacion = Info_UltimaIdentificacion & vbNewLine & "Error en el proceso de identificaci�n: " & vbNewLine & vbNewLine & Err.Description
    
    GoTo HidePgB
    
End Function

Private Function IdentificarCliente_EnTienda(pID_A_Verificar As String, Optional PgB As ProgressBar) As String
    
    On Error GoTo ErrIdent
    
    lblStatus.Caption = "Identificando... Por favor espere."
    
    AccionesIdentificacion
    
    Dim mRsHuellas As New ADODB.Recordset
    Dim mSql As String
    
    Dim MatchInfo As New Collection, MatchCount As Long
    Dim Match As Long, I As Long
    
    Dim TmpTemplate() As Byte, TmpTemplateSize As Long
    
    SetMatcherStandard
    
    'Test
    Dim TimeIni As Date, TimeEnd As Date, TimeSegs As Long
    TimeIni = DateTime.Now
    TimeEnd = TimeIni
    TimeSegs = 0
    
    mRsHuellas.CursorLocation = adUseClient
    
    mSql = "SELECT c_CodCliente, Bin_DataArray, n_TemplateSize FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS" & vbNewLine & _
    "WHERE c_Cadena_Tienda = '" & frmRacionamiento_Registro_de_Clientes_con_Biometrico.pCadenaComercio & "'"
    
    DoEvents
    
    mRsHuellas.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenStatic, adLockReadOnly, adCmdText
    
    DoEvents
    
    Set mRsHuellas.ActiveConnection = Nothing
    
    Set MatchInfo = New Collection
    
    If Not mRsHuellas.EOF Then
        
        If Not IsMissing(PgB) Then
            PgB.Max = mRsHuellas.RecordCount
            PgB.mIn = 0
            PgB.value = 0
            PgB.Appearance = cc3D
            PgB.Visible = True
        End If
        
        Do While Not mRsHuellas.EOF
            
            DoEvents
            
            If CancelEnroll Then Exit Do
            
            TmpTemplateSize = mRsHuellas!n_TemplateSize 'Len(mRsHuellas!BIN_DATA)
            TmpTemplate = mRsHuellas!bin_DataArray 'ReDim TmpTemplate(MaxTemplateSize - 1)
            
            'For I = 1 To MaxTemplateSize
               'Debug.Print TmpTemplate(I - 1) 'TmpTemplate(I - 1) = Asc(Mid(mRsHuellas!BIN_DATA, I, 1))
            'Next I
            
            mStat = UFM_Verify(Matcher, UniTemplate(0), UniTemplateSize, TmpTemplate(0), TmpTemplateSize, Match)
            
            If CBool(Match) Then
                If Not Collection_ExisteKey(MatchInfo, mRsHuellas!c_CodCliente) Then MatchInfo.Add CStr(mRsHuellas!c_CodCliente), CStr(mRsHuellas!c_CodCliente)
                MatchCount = MatchCount + 1
                GoTo encontrado
            End If
            
            If Not IsMissing(PgB) Then
                PgB.value = PgB.value + 1
            End If
            
            mRsHuellas.MoveNext
            
        Loop
    Else
        IdentificarCliente_EnTienda = "Proceder a Registrar" 'No hay registros a�n.
    End If

encontrado:

    If CancelEnroll Then
        GoTo HidePgB
    End If
    
    'Test
    TimeEnd = DateTime.Now
    TimeSegs = DateDiff("s", TimeIni, TimeEnd)
    Debug.Print TimeSegs
    
    If MatchCount <= 0 Then
        IdentificarCliente_EnTienda = "Proceder a Registrar"
    ElseIf MatchCount = 1 And MatchInfo.Count = 1 Then
        If UCase(MatchInfo.Item(1)) <> UCase(pID_A_Verificar) Then
            IdentificarCliente_EnTienda = "Robo de Identidad" 'Chequear
        Else
            IdentificarCliente_EnTienda = "Cliente confirmado"
        End If
    'ElseIf MatchCount <= 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_EnTienda = "Robo de Identidad" 'Chequear
    'ElseIf MatchCount >= 2 And MatchInfo.Count = 1 Then
        'IdentificarCliente_EnTienda = "Registros multiples" ' Chequear...
    'ElseIf MatchCount > 2 And MatchInfo.Count > 1 Then
        'IdentificarCliente_EnTienda = "Registros Multiples y Robo de Identidad" 'Chequear
    End If
    
HidePgB:

    'Dejar todo en orden.

    If Not IsMissing(PgB) Then
        PgB.Max = 100
        PgB.mIn = 0
        PgB.value = 0
        PgB.Visible = False
    End If
    
    If mRsHuellas.State = adStateOpen Then mRsHuellas.Close
    
    AccionesIdentificacion True
    
    Exit Function
    
ErrIdent:
    
    'Debug.Print Err.Description
    
    MsjErrorRapido Err.Description, "Ha ocurrido un error en la rutina de identificaci�n, por favor reporte lo siguiente:" & vbNewLine & vbNewLine
    
    IdentificarCliente_EnTienda = "Error en la identificacion"
    
    GoTo HidePgB
    
End Function

Private Sub ZKEngine_OnCaptureToFile(ByVal ActionResult As Boolean)
    If ModoReconocimiento And ActionResult Then
        
        DoEvents
        
        Do While Not CaptureProc
            DoEvents
            If CancelEnroll Then CancelEnroll = False:  Me.Cerrar = False: Unload Me: Exit Sub
            DoEvents
        Loop
    
        'Dim TmpArchivoHuellaCapturada As String
    
        'TmpArchivoHuellaCapturada = App.path & "\HuellaCapturada.Tpl"
        
        'Dim HuellaCapturada As String
    
        'HuellaCapturada = ReadFileIntoString(TmpArchivoHuellaCapturada)
        'HuellaCapturada = LoadFile(TmpArchivoHuellaCapturada)
    
        'Debug.Print "Calidad: " & Quality & " | " & HuellaCapturada
    
        'HuellaCapturada = Replace(HuellaCapturada, Chr(13), "")
        'Debug.Print HuellaCapturada
        
        ModoReconocimiento_Respuesta = VerificarCliente(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB)
        
        Me.Cerrar = False: Unload Me
        
    End If
End Sub

Private Sub RellenarTemplate()
    ReDim UniTemplatePart(UniTemplateSize - 1) As Byte
    Dim I As Long
    For I = 0 To UniTemplateSize - 1
        UniTemplatePart(I) = UniTemplate(I)
    Next I
End Sub

Private Function CaptureProc() As Boolean
    
    On Error GoTo ErrorCapture
    
    CaptureProc = False
    
    Me.lblAccionDispositivo.Caption = "Detectando Huella..."
    
    sStat = UFS_ClearCaptureImageBuffer(BioScanner)
    
    SetScannerStandard
    
    sStat = UFS_CaptureSingleImage(BioScanner)
    
    If (sStat <> UFS_STATUS.OK) Then
        DoEvents
        lblStatus.Caption = "No se ha podido obtener la huella."
        lblAccionDispositivo.Caption = "Por favor intente nuevamente."
        Sleep 1000
        Exit Function
    End If
    
    ReDim UniTemplate(MaxTemplateSize - 1): UniTemplateSize = 0
    
    sStat = UFS_ExtractEx(BioScanner, MaxTemplateSize, UniTemplate(0), UniTemplateSize, Quality)
    
    If sStat <> UFS_STATUS.OK Then
        IgnorarActivate
        Mensaje True, "No se ha podido obtener la huella. Por favor intente nuevamente."
        Sleep 1000
        Exit Function
    End If
    
    If Not OnFeatureInfo Then
        Exit Function
    End If
    
    RellenarTemplate
    
    CaptureProc = True
    
    Exit Function
    
ErrorCapture:
    
    CaptureProc = False
    
End Function

Private Function EnrollProc() As Boolean

    On Error GoTo ErrorEnroll
    
    EnrollProc = False
    
    Me.lblAccionDispositivo.Caption = "Detectando Huella..."
    DoEvents
    
    sStat = UFS_ClearCaptureImageBuffer(BioScanner)
    
    SetScannerStandard
    
    sStat = UFS_CaptureSingleImage(BioScanner)
    DoEvents
    If (sStat <> UFS_STATUS.OK) Then
        DoEvents
        lblStatus.Caption = "No ha ingresado huella. Intente nuevamente."
        lblAccionDispositivo.Caption = "Por favor espere..."
        DoEvents
        Sleep 1000 ': ResetearStatus
        Exit Function
    End If
    
    ReDim UniTemplate(MaxTemplateSize - 1): UniTemplateSize = 0
    
    sStat = UFS_ExtractEx(BioScanner, MaxTemplateSize, UniTemplate(0), UniTemplateSize, Quality)
    
    If sStat <> UFS_STATUS.OK Then
        IgnorarActivate
        Mensaje True, "No se ha podido obtener la huella. Por favor intente nuevamente."
        'ResetearStatus
        Exit Function
    End If
    
    'Debug.Print vbNewLine
    
    If Not OnFeatureInfo Then
        Exit Function
    End If
    
    RellenarTemplate
    
    ' Guardar la Plantilla de la Huella.
    
    FileNumber = FreeFile
    
    Dim TmpArchivo As String
    TmpArchivo = App.path & "\" & "HuellaRegistro.tmp"
    
    Call KillSecure(TmpArchivo)
    
    Open TmpArchivo For Binary As #FileNumber
        Dim I As Long
        For I = 0 To UniTemplateSize - 1
            Put #FileNumber, , UniTemplate(I)
        Next
    Close #FileNumber
        
    Dim TmpHuella As String
        
    TmpHuella = LoadFile(TmpArchivo)            'Funci�n Marynel-Proof
    
    If Trim(TmpHuella) = "" Then
        GoTo ErrorEnroll
    End If
        
    'Debug.Print Quality & ":" & TmpHuella
    
    ' Guardar la Plantilla de la Huella.
    
    ' Reconocimiento.
        
        ModoReconocimiento_Respuesta = ""
        
        Select Case frmRacionamiento_Registro_de_Clientes_con_Biometrico.pAcciondeRegistro
            Case 0
                ModoReconocimiento_Respuesta = VerificarCliente(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB, False)
            Case 1
                ModoReconocimiento_Respuesta = VerificarCliente(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB, True)
            Case 2
                ModoReconocimiento_Respuesta = IdentificarCliente_Secuencial(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB)
            Case 3
                ModoReconocimiento_Respuesta = IdentificarCliente_PorPartes(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB)
            Case 4
                ModoReconocimiento_Respuesta = IdentificarCliente_EnTienda(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text, PgB)
        End Select
        
        'Debug.Print vbNewLine & ModoReconocimiento_Respuesta
        
        KillShot TmpArchivo
        
        Select Case UCase(ModoReconocimiento_Respuesta)
            Case UCase("Proceder a Registrar")
                'Continuar registrando la huella.
                'IgnorarActivate
                'mensaje True, "No se ha podido obtener la huella. Por favor intente nuevamente."
                'ResetearStatus
                'Exit Sub
            Case UCase("Cliente confirmado")
                'La huella ya se hab�a registrado en otro Slot.
                IgnorarActivate
                Mensaje True, "La huella ya se encuentra registrada para la c�dula introducida, pero en otra posici�n. No es posible registrarla nuevamente."
                ResetearStatus
                CancelEnroll = True: EnrollSuccessCount = 0
                Exit Function
            Case UCase("Robo de Identidad")
                IgnorarActivate
                Mensaje True, "La huella se encuentra registrada con una c�dula distinta. No es posible registrarla."
                ResetearStatus
                CancelEnroll = True: EnrollSuccessCount = 0
                Exit Function
            Case UCase("Registros Multiples")
                IgnorarActivate
                Mensaje True, "La huella existe y est� duplicada para la c�dula introducida. No es posible registrarla."
                ResetearStatus
                CancelEnroll = True: EnrollSuccessCount = 0
                Exit Function
            'Case UCase("Registros Multiples y Robo de Identidad")
                'IgnorarActivate
                'mensaje True, "La huella existe y est� duplicada para m�ltiples c�dulas. No es posible registrarla."
                'ResetearStatus
                'CancelEnroll = True: EnrollSuccessCount = 0
                'Exit Function
            Case Else 'Errores, etc.
                ResetearStatus
                CancelEnroll = True: EnrollSuccessCount = 0
                Exit Function
        End Select
        
    ' Reconocimiento.
 
    Dim CurrentMask As Integer
    
    CurrentMask = CInt(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtPlantilla.Text)
 
    Dim Diferencia As Integer
    
    Diferencia = DiferenciaBytes(Ingreso_Enrollment, CurrentMask, Me.EnrolledFingersMask)
    
    Me.EnrolledFingersMask = Me.EnrolledFingersMask + Me.CurrentEnrollFingerMask
    
    
    
    If ActualizarPerfilUsuario(Ingreso_Enrollment, Me.CurrentEnrollFingerMask, TmpHuella) Then
        ActualizarPlantillaUsuario (Me.EnrolledFingersMask)
        'Exit Sub
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            RegistrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbChecked
        Case CH_Dedos.AnularIzquierdo
            RegistrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbChecked
        Case CH_Dedos.MedioIzquierdo
            RegistrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbChecked
        Case CH_Dedos.IndiceIzquierdo
            RegistrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarIzquierdo
            RegistrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbChecked
        Case CH_Dedos.PulgarDerecho
            RegistrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbChecked
        Case CH_Dedos.IndiceDerecho
            RegistrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbChecked
        Case CH_Dedos.MedioDerecho
            RegistrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbChecked
        Case CH_Dedos.AnularDerecho
            RegistrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbChecked
        Case CH_Dedos.Me�iqueDerecho
            RegistrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbChecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    Me.CurrentEnrollFingerMask = 0
    
    EnrollProc = True: EnrollSuccessCount = 0
    
    Exit Function
    
ErrorEnroll:
    
    Debug.Print Err.Description
    
    EnrollProc = False
    
    IgnorarActivate
    KillShot TmpArchivo
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    ResetearStatus
    CancelEnroll = True: EnrollSuccessCount = 0

End Function

Private Sub ActualizarPlantillaUsuario(Valor As Integer)

    Dim mRs As New ADODB.Recordset

    Dim mSql As String
    
    mSql = "SELECT * FROM MA_RACIONALIZACIOn_ClienteS WHERE c_Rif = '" _
    & frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text & "'"
    
    mRs.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs.Update
        mRs!n_PlantillaBiometrico = Valor
        mRs.UpdateBatch
        mRs.Close
    End If
   
   frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtPlantilla.Text = CStr(Valor)

End Sub

Private Function ActualizarPerfilUsuario(TipoOperacion As TipoOperacion_CH, MaskUpdate As Integer, Optional CHData As Variant) As Boolean

    'On Error GoTo ErrorOperacion

    Dim ID_Dedo As String
    Dim RsOperacion As New ADODB.Recordset
    Dim mSql As String
    Dim Data As Variant
   
    ActualizarPerfilUsuario = True

    Select Case MaskUpdate
        Case CH_Dedos.Me�iqueIzquierdo
            ID_Dedo = "Me�ique Izquierdo"
        Case CH_Dedos.AnularIzquierdo
            ID_Dedo = "Anular Izquierdo"
        Case CH_Dedos.MedioIzquierdo
            ID_Dedo = "Medio Izquierdo"
        Case CH_Dedos.IndiceIzquierdo
            ID_Dedo = "Indice Izquierdo"
        Case CH_Dedos.PulgarIzquierdo
            ID_Dedo = "Pulgar Izquierdo"
        Case CH_Dedos.PulgarDerecho
            ID_Dedo = "Pulgar Derecho"
        Case CH_Dedos.IndiceDerecho
            ID_Dedo = "Indice Derecho"
        Case CH_Dedos.MedioDerecho
            ID_Dedo = "Medio Derecho"
        Case CH_Dedos.AnularDerecho
            ID_Dedo = "Anular Derecho"
        Case CH_Dedos.Me�iqueDerecho
            ID_Dedo = "Me�ique Derecho"
        Case Else
            ID_Dedo = "Desconocido"
    End Select
    
    If ID_Dedo = "Desconocido" Then
        IgnorarActivate
        
        Mensaje True, "La huella digital no se ha podido grabar." & vbNewLine & _
        "El reconocimiento ha sido invalido." & vbNewLine & _
        "Intente con otra posici�n."
        
        ResetearStatus
        
        ActualizarPerfilUsuario = False
    
        Exit Function
    End If
    
    'If (Not CHData = "") Then Data = CHData 'CHData.Serialize
    If Not IsMissing(CHData) Then Data = CHData
    
    mSql = "SELECT * FROM MA_RACIONALIZACIOn_ClienteS_HUELLAS WHERE (c_CodCliente = '" & _
            frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text & "'" & _
            " AND C_DEDO = '" & ID_Dedo & "')"
            
    RsOperacion.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    Select Case TipoOperacion
    
        Case TipoOperacion_CH.Ingreso_Enrollment
        
            If RsOperacion.EOF Then
            
                RsOperacion.AddNew
                RsOperacion!ID_Dispositivo = frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDDisp.Text
                RsOperacion!c_CodCliente = frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_TipoHuella = "Est�ndar ISO 19794-2" 'Testing...
                RsOperacion!BIN_DATA = Data 'Replace(StringToHex(Data), " ", "")
                RsOperacion!bin_DataArray = UniTemplatePart
                'RsOperacion!Bin_DataArrayFull = UniTemplate
                RsOperacion!n_TemplateSize = UniTemplateSize
                RsOperacion!b_Enviado = 0
                RsOperacion!c_Archivo = ""
                RsOperacion!d_Fecha = frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute("Select getDate() as Fecha")!FECHA
                RsOperacion!c_Cadena_Tienda = frmRacionamiento_Registro_de_Clientes_con_Biometrico.pCadenaComercio
                RsOperacion!C_CODUSUARIO = LcCodUsuario
                RsOperacion.UpdateBatch
                
            Else
                
                RsOperacion.Update
                RsOperacion!ID_Dispositivo = frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDDisp.Text
                RsOperacion!c_CodCliente = frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text
                RsOperacion!C_DEDO = ID_Dedo
                RsOperacion!c_TipoHuella = "Est�ndar ISO 19794-2" 'Testing...
                RsOperacion!BIN_DATA = Data 'Replace(StringToHex(Data), " ", "")
                RsOperacion!bin_DataArray = UniTemplatePart
                'RsOperacion!Bin_DataArrayFull = UniTemplate
                RsOperacion!n_TemplateSize = UniTemplateSize
                RsOperacion!b_Enviado = 0
                RsOperacion!c_Archivo = ""
                RsOperacion!d_Fecha = frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute("Select getDate() as Fecha")!FECHA
                RsOperacion!c_Cadena_Tienda = frmRacionamiento_Registro_de_Clientes_con_Biometrico.pCadenaComercio
                RsOperacion!C_CODUSUARIO = LcCodUsuario
                RsOperacion.UpdateBatch
        
            End If
             
        Case TipoOperacion_CH.Borrado_Deletion
        
            If Not RsOperacion.EOF Then
                
                RsOperacion.MoveFirst
                RsOperacion.Delete
                RsOperacion.UpdateBatch
                
            End If
    
    End Select
    
    RsOperacion.Close
    'MiConexion.Close
    
    frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion.Execute ( _
    "UPDATE MA_RACIONALIZACIOn_ClienteS SET c_Archivo = '', b_Enviado = 0 WHERE c_Rif = '" _
    & frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text & "'")
    
    ActualizarPerfilUsuario = True
    
    Exit Function
        
ErrorOperacion:

    ActualizarPerfilUsuario = False
    Debug.Print Err.Description
    Debug.Print Data
    
End Function

Private Function DiferenciaBytes(TipoOperacion As TipoOperacion_CH, CurrentMask As Integer, NewMask As Integer) As Integer

    Select Case TipoOperacion
        
        Case TipoOperacion_CH.Ingreso_Enrollment
            
            DiferenciaBytes = NewMask - CurrentMask
            
        Case TipoOperacion_CH.Borrado_Deletion
    
            DiferenciaBytes = CurrentMask - NewMask
    
    End Select

End Function

Private Function GetCurrentUserMask() As Integer
    
    On Error GoTo Error
    
    Dim mRs As New ADODB.Recordset
    
    Dim mSql As String
    
    mSql = "SELECT n_PlantillaBiometrico FROM MA_RACIONALIZACIOn_ClienteS " & _
    "WHERE c_Rif = '" & frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtIDUsuario.Text & "' "
    
    mRs.Open mSql, frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac.conexion, adOpenStatic, adLockReadOnly
    
    If Not mRs.EOF Then
        GetCurrentUserMask = mRs!n_PlantillaBiometrico
    Else
        GetCurrentUserMask = 0
    End If
    
    mRs.Close
    
    Exit Function
    
Error:
    
    GetCurrentUserMask = 0
    
End Function

Private Sub ZKEngine_Delete(ByVal Template As Variant)

On Error GoTo ErrorDelete

    Dim CurrentMask As Integer

    CurrentMask = CInt(frmRacionamiento_Registro_de_Clientes_con_Biometrico.txtPlantilla.Text)

    Dim Diferencia As Integer

    Diferencia = DiferenciaBytes(Borrado_Deletion, CurrentMask, Me.CurrentEnrollFingerMask)

    If ActualizarPerfilUsuario(Borrado_Deletion, Me.CurrentEnrollFingerMask) Then
        ActualizarPlantillaUsuario (Diferencia)
    End If
    
    Select Case CurrentEnrollFingerMask
        Case CH_Dedos.Me�iqueIzquierdo
            BorrandoMe�iqueIzquierdo = True
            cmdDedoMe�iqueIzquierdo.value = vbUnchecked
        Case CH_Dedos.AnularIzquierdo
            BorrandoAnularIzquierdo = True
            cmdDedoAnularIzquierdo.value = vbUnchecked
        Case CH_Dedos.MedioIzquierdo
            BorrandoMedioIzquierdo = True
            cmdDedoMedioIzquierdo.value = vbUnchecked
        Case CH_Dedos.IndiceIzquierdo
            BorrandoIndiceIzquierdo = True
            cmdDedoIndiceIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarIzquierdo
            BorrandoPulgarIzquierdo = True
            cmdDedoPulgarIzquierdo.value = vbUnchecked
        Case CH_Dedos.PulgarDerecho
            BorrandoPulgarDerecho = True
            cmdDedoPulgarDerecho.value = vbUnchecked
        Case CH_Dedos.IndiceDerecho
            BorrandoIndiceDerecho = True
            cmdDedoIndiceDerecho.value = vbUnchecked
        Case CH_Dedos.MedioDerecho
            BorrandoMedioDerecho = True
            cmdDedoMedioDerecho.value = vbUnchecked
        Case CH_Dedos.AnularDerecho
            BorrandoAnularDerecho = True
            cmdDedoAnularDerecho.value = vbUnchecked
        Case CH_Dedos.Me�iqueDerecho
            BorrandoMe�iqueDerecho = True
            cmdDedoMe�iqueDerecho.value = vbUnchecked
        'Case Else
            'ID_Dedo = "Desconocido"
    End Select
    
    Me.EnrolledFingersMask = Me.EnrolledFingersMask - Me.CurrentEnrollFingerMask
    
    Me.CurrentEnrollFingerMask = 0
    
    Exit Sub

ErrorDelete:

    IgnorarActivate
    Mensaje True, "No se puedo actualizar el perfil del usuario." & vbNewLine & _
    "Contacte al departamento de Soporte T�cnico."
    ResetearStatus

End Sub

Private Function OnFeatureInfo() As Boolean
    
    OnFeatureInfo = CBool(Quality >= MinQuality)
    
    If isRegister Then
        If OnFeatureInfo Then EnrollSuccessCount = EnrollSuccessCount + 1
        
        lblAccionDispositivo.Caption = "Calidad: " & Quality & "%. Intento " & IIf(Not OnFeatureInfo, "Fallido", "Acertado") & ". Prosiga."
        lblStatus.Caption = IIf(Not OnFeatureInfo, lblStatus.Caption, "Registrando... Aciertos Restantes: " & Me.EnrollCount - EnrollSuccessCount)
        
        OnFeatureInfo = EnrollSuccessCount >= Me.EnrollCount
    ElseIf ModoReconocimiento And Not isRegister Then
        lblAccionDispositivo.Caption = "Calidad: " & Quality & "%. Intento " & IIf(Not OnFeatureInfo, "Fallido", "Acertado") & "."
    End If
    
    DoEvents
    
    Sleep 1000
    
End Function

Private Function VerificarMaxHuellas() As Boolean

    Dim HuellasRegistradas As Integer

    If frmRacionamiento_Registro_de_Clientes_con_Biometrico.pMaxHuellasRegistro <= 0 Then
        VerificarMaxHuellas = True
    Else
    
        If Me.cmdDedoMe�iqueIzquierdo.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoAnularIzquierdo.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoMedioIzquierdo.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoIndiceIzquierdo.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoPulgarIzquierdo.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoMe�iqueDerecho.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoAnularDerecho.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoMedioDerecho.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoIndiceDerecho.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1
        If Me.cmdDedoPulgarDerecho.value = vbChecked Then HuellasRegistradas = HuellasRegistradas + 1

        VerificarMaxHuellas = HuellasRegistradas <= frmRacionamiento_Registro_de_Clientes_con_Biometrico.pMaxHuellasRegistro

    End If

End Function

Private Sub lblAccionDispositivo_Click()
    If Trim(Info_UltimaIdentificacion) <> "" Then
        IgnorarActivate
        Mensaje True, Info_UltimaIdentificacion
    End If
End Sub
