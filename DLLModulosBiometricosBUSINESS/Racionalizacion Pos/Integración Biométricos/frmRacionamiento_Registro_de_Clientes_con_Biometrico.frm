VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmRacionamiento_Registro_de_Clientes_con_Biometrico 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Clientes (Sistema Biom�trico)"
   ClientHeight    =   4005
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5745
   Icon            =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   5745
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameDatosUsuario 
      Caption         =   "Datos del Cliente"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   2295
      Left            =   120
      TabIndex        =   7
      Top             =   1560
      Width           =   5535
      Begin VB.OptionButton OptCliente 
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   2040
         TabIndex        =   24
         Top             =   1650
         Width           =   1455
      End
      Begin VB.OptionButton OptOperador 
         Caption         =   "Operador"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   1650
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.CommandButton cmdReconocimiento 
         Caption         =   "Modificar"
         CausesValidation=   0   'False
         DisabledPicture =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":628A
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   3720
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":6596
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1080
         Width           =   800
      End
      Begin VB.CommandButton CmdLimpiar 
         Caption         =   "Limpiar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   4560
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":7262
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   800
      End
      Begin VB.CommandButton CmdPlantilla 
         Caption         =   "Registro"
         CausesValidation=   0   'False
         DisabledPicture =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":7F2E
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   4560
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":85E5
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1080
         Width           =   800
      End
      Begin VB.TextBox txtPlantilla 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   4680
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   1920
         Width           =   600
      End
      Begin VB.CheckBox chk_Activo 
         Caption         =   "Cliente Activo para compra (PR)"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1920
         Value           =   1  'Checked
         Width           =   3615
      End
      Begin VB.CommandButton CmdGuardarDatosUsuario 
         Caption         =   "Grabar"
         CausesValidation=   0   'False
         DisabledPicture =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":8C9C
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   3720
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":9968
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   800
      End
      Begin VB.TextBox txtApellidoUsuario 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1200
         MaxLength       =   80
         TabIndex        =   13
         Top             =   1320
         Width           =   2400
      End
      Begin VB.TextBox txtNombreUsuario 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1200
         MaxLength       =   80
         TabIndex        =   11
         Top             =   840
         Width           =   2400
      End
      Begin VB.TextBox txtIDUsuario 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1200
         MaxLength       =   50
         TabIndex        =   9
         Top             =   360
         Width           =   2400
      End
      Begin VB.CommandButton CmdUsuario 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   480
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":A634
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2010
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Label lblRegistrado 
         AutoSize        =   -1  'True
         Caption         =   "Registrado"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   1560
         TabIndex        =   20
         Top             =   1680
         Width           =   1050
      End
      Begin VB.Label lblPlantilla 
         AutoSize        =   -1  'True
         Caption         =   "Plantilla:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   3720
         TabIndex        =   18
         Top             =   1950
         Width           =   855
      End
      Begin VB.Label lblApellido 
         AutoSize        =   -1  'True
         Caption         =   "Apellido:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label lblNombres 
         AutoSize        =   -1  'True
         Caption         =   "* Nombre:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   1005
      End
      Begin VB.Label lblIDUsuario 
         AutoSize        =   -1  'True
         Caption         =   "* C.I / ID:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame frameDispositivo 
      Caption         =   "Datos del Dispositivo"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8655
      Begin VB.TextBox txtModelo 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   825
         Width           =   6135
      End
      Begin VB.TextBox txtDescripcion 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   3480
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   360
         Width           =   5055
      End
      Begin VB.CommandButton cmd_IDDisp 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   3120
         Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":A71E
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   330
         Width           =   315
      End
      Begin VB.TextBox txtIDDisp 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   1680
         MaxLength       =   50
         TabIndex        =   0
         Top             =   360
         Width           =   1440
      End
      Begin VB.Label lblModelo 
         AutoSize        =   -1  'True
         Caption         =   "Modelo de Dispositivo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   2190
      End
      Begin VB.Label lblIDDisp 
         AutoSize        =   -1  'True
         Caption         =   "ID Dispositivo:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1425
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   10080
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":A808
            Key             =   "Agrergar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":B4E4
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":C1C0
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":CE9C
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":DB78
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":E854
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":F530
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1020C
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":10EE8
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":11204
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   8880
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":11EE0
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":12BBC
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":12ED8
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":131F4
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":13ED0
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":14BAC
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":15888
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":15BA4
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":16880
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":16B9C
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   9480
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":17878
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":18554
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":19230
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1954C
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1A228
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1AF04
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1BBE0
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1C8BC
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1D598
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmRacionamiento_Registro_de_Clientes_con_Biometrico.frx":1D8B4
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmRacionamiento_Registro_de_Clientes_con_Biometrico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Private isNew As Boolean 'Ingresando o Modificando
Private pPermiteDesactivarClientes As Boolean 'N/A: Se har� por otra ficha. Permite Activar / Desactivar la compra de productos regulados para una determinada c�dula.
Public pDedosPermitidos As String 'String de los dedos permitidos para guardar huella separado por "|".
Public pFormatoHuella As Integer '0 Formato Propietario, 1 Formato Est�ndar ANSI 378, 2 Formato Est�ndar ISO 19794-2
Public pNroIntentosRegistro As Integer 'N�mero de veces que hay que colocar la huella al momento del registro para ser validado.
Public pCalidadMinimaHuella As Integer
Public pPermiteBorrarHuellasRegistradas As Boolean
Public pOrdenIdentificacion As Integer '0 Por orden de registro, 1 Por orden Alfab�tico de C�dulas, 2 Por Orden Aleatorio.
Public pAcciondeRegistro As Integer '0 Verificaci�n Real, 1 Verificacion Falsa con Tiempo Estimado A�adido, 2 Identificaci�n, 3 Identificaci�n por Partes, 4 Identificaci�n con las huellas registradas en el Comercio.
Public NumRegxLote_Identificacion As Long
Public pMaxHuellasRegistro  As Integer
Public pCadenaComercio As String
Public pTiendaComercio As String

Private pBuscando As Boolean
Private pCambioProgramado As Boolean
Private ValidarClienteSIVPRE As Boolean

Private CambiosInterfazAplicados As Boolean
Private DispositivoSeleccionado As String

Public fClsRac As Object
Public MyMainClass As MainClass

Public EvitarActivate As Boolean

Public InterfazOperador As Boolean 'Sin uso.
Public VerificandoOperador As Boolean 'Sin uso.

Private SIVPRENumeroMinimoIDV                 As Double
Private SIVPRENumeroMaximoIDV                 As Double
Private SIVPRELimiteCaracteresNumericosIDV    As Double
Private SIVPRENumeroMinimoIDE                 As Double
Private SIVPRENumeroMaximoIDE                 As Double
Private SIVPRELimiteCaracteresNumericosIDE    As Double
Private SIVPRENumeroMinimoIDJ                 As Double
Private SIVPRENumeroMaximoIDJ                 As Double
Private SIVPRELimiteCaracteresNumericosIDJ    As Double
Private SIVPRENumeroMinimoIDG                 As Double
Private SIVPRENumeroMaximoIDG                 As Double
Private SIVPRELimiteCaracteresNumericosIDG    As Double
Private SIVPRENumeroMinimoIDP                 As Double
Private SIVPRENumeroMaximoIDP                 As Double
Private SIVPRELimiteCaracteresNumericosIDP    As Double

Private mRifValidado As Boolean

Private Function IgnorarActivate() As Boolean
    IgnorarActivate = True: EvitarActivate = True
End Function

Private Sub cmd_IDDisp_Click()
    
    pBuscando = True
    
    Dim DISP As Variant: DISP = BuscarDispositivoBiometrico
    
    If Not IsEmpty(DISP) Then
        If Not Trim(DISP(0)) = "" Then
            Call CargarDatosDispositivo(CStr(DISP(0)), DISP)
        End If
    End If
    
    pBuscando = False
    
End Sub

Private Sub CargarDatosDispositivo(pID As String, Optional pArrDatos As Variant)
    
    On Error GoTo Err
    
    If Not IsMissing(pArrDatos) Then
        
        Me.txtIDDisp = Trim(pArrDatos(0))
        Me.txtDescripcion = Trim(pArrDatos(1)) & " - " & Trim(pArrDatos(3))
        Me.txtModelo = Trim(pArrDatos(2))
        pNroIntentosRegistro = ValidarNumeroIntervalo(pArrDatos(4), 32767, 1)
        
        'Me.TxtModelo.SetFocus 'El dispositivo ahora se pre-selecciona desde el Setup y esto est� oculto.
        
    ElseIf Not pBuscando Then
        
        Dim mRs As ADODB.Recordset
        
        Set mRs = Me.fClsRac.conexion.Execute("Select ID_Dispositivo, c_Descripcion, c_Modelo, c_Serial, nu_NumMuestreos " _
        & "FROM MA_RACIONALIZACION_DISPOSITIVOSBIOMETRICOS WHERE ID_Dispositivo = '" & pID & "' AND bu_Activo = 1")
        
        If CBool(mRs.RecordCount) Then
            Me.txtIDDisp = mRs!ID_DISPOSITIVO
            Me.txtDescripcion = mRs!c_Descripcion & " - " & mRs!c_Serial
            pNroIntentosRegistro = ValidarNumeroIntervalo(Val(mRs!nu_NumMuestreos), 32767, 1)
            Me.txtModelo = mRs!c_Modelo
            'Me.TxtModelo.SetFocus 'El dispositivo ahora se pre-selecciona desde el Setup y esto est� oculto.
        Else
            Me.txtDescripcion = "Dispositivo no encontrado."
            Me.txtIDDisp = "": Me.txtModelo = ""
        End If
    
    End If
    
    Exit Sub
    
Err:
    
    Me.txtDescripcion.Text = "Error. Haga doble clic aqu� para mas detalles."
    Me.txtDescripcion.Tag = Err.Description
    Me.txtIDDisp = "": Me.txtModelo = ""
    pNroIntentosRegistro = 0
    
End Sub

Private Sub CargarDatosCliente(pID As String, Optional pArrDatos As Variant)
    
    On Error GoTo Err
    
    If Not IsMissing(pArrDatos) Then
        
        pCambioProgramado = True: Me.txtIDUsuario.Text = Trim(pArrDatos(0)): pCambioProgramado = False
        Me.txtNombreUsuario.Text = Trim(pArrDatos(2)) & Space(1) & Trim(pArrDatos(1))
        'Me.txtApellidoUsuario.Text = Trim(pArrDatos(2))
        Me.chk_Activo.value = IIf(CBool(pArrDatos(3)), 1, 0)
        Me.txtPlantilla.Text = Trim(pArrDatos(4))
        
        Me.CmdPlantilla.Enabled = True
        
        'If InterfazOperador Then
            'If CBool(pArrDatos(5)) Then
                'OptOperador.value = True
            'Else
                'OptCliente.value = True
            'End If
            
            'Me.cmdReconocimiento.Enabled = False
            'Me.CmdPlantilla.Enabled = True
            'Me.CmdGuardarDatosUsuario.Enabled = True
        'Else
            Me.cmdReconocimiento.Enabled = True
            Me.CmdGuardarDatosUsuario.Enabled = False
        'End If
        
    ElseIf Not pBuscando Then
        
        Dim mRs As ADODB.Recordset
        
        Set mRs = Me.fClsRac.conexion.Execute("SELECT * FROM MA_RACIONALIZACIOn_ClienteS " _
        & "WHERE c_Rif = '" & pID & "'")
        
        If CBool(mRs.RecordCount) Then
            pCambioProgramado = True: Me.txtIDUsuario.Text = mRs!c_Rif: pCambioProgramado = False
            Me.txtNombreUsuario.Text = mRs!c_Apellido & Space(1) & mRs!c_Nombre
            'Me.txtApellidoUsuario.Text = 'mRs!c_Apellido
            Me.txtPlantilla.Text = CStr(mRs!n_PlantillaBiometrico)
            
            Me.lblRegistrado.Caption = "Datos cargados."
            Me.chk_Activo.value = IIf(CBool(mRs!b_Activo), 1, 0)
            
            'If InterfazOperador Then
                'If CBool(mRs!b_Operador) Then
                    'OptOperador.value = True
                'Else
                    'OptCliente.value = True
                'End If
                
                'Me.cmdReconocimiento.Enabled = False
                'Me.CmdPlantilla.Enabled = True
                'Me.CmdGuardarDatosUsuario.Enabled = True
            'Else
                Me.cmdReconocimiento.Enabled = True
                Me.CmdPlantilla.Enabled = False
                Me.CmdGuardarDatosUsuario.Enabled = False
            'End If
            
            If Me.chk_Activo.value = vbUnchecked Then
                Me.lblRegistrado.Caption = "Registro Bloqueado."
                Me.CmdGuardarDatosUsuario.Enabled = False
                Me.cmdReconocimiento.Enabled = False
                Me.CmdPlantilla.Enabled = False
            End If
        Else
            Dim Tmp As String: Tmp = Me.txtIDUsuario.Text
            Call limpiar: pCambioProgramado = True: Me.txtIDUsuario.Text = Tmp: pCambioProgramado = False
        End If

    End If
    
    Exit Sub
    
Err:
    
    Tmp = Me.txtIDUsuario.Text
    Call limpiar: pCambioProgramado = True: Me.txtIDUsuario.Text = Tmp: pCambioProgramado = False
    
End Sub

Private Sub CmdGuardarDatosUsuario_Click()
    
    On Error GoTo Err
    
    If Not ValidarDatos Then Exit Sub
    
    Dim mRs As New ADODB.Recordset
    Dim mSql As String, Tmp As Variant
    
    mSql = "SELECT * FROM MA_RACIONALIZACIOn_ClienteS WHERE c_Rif = '" & Trim(Me.txtIDUsuario) & "'"
    
    mRs.Open mSql, Me.fClsRac.conexion, adOpenStatic, adLockOptimistic
    
    If CBool(mRs.RecordCount) Then
        mRs.Update
            'mRs!c_Nombre = Me.txtNombreUsuario.Text
            'mRs!c_Apellido = Me.txtApellidoUsuario.Text
            Tmp = DescripcionCliente(Me.txtNombreUsuario.Text)
            mRs!c_Nombre = Tmp(1)
            mRs!c_Apellido = Tmp(0)
            mRs!b_Activo = Me.chk_Activo.value
            mRs!n_PlantillaBiometrico = CInt(Me.txtPlantilla.Text)
            mRs!c_Archivo = ""
            mRs!b_Enviado = 0
            'If InterfazOperador Then mRs!b_Operador = IIf(OptOperador.value = True, 1, 0)
        mRs.UpdateBatch
        
        IgnorarActivate
        mensaje True, "Datos actualizados."
    Else
        mRs.AddNew
            mRs!c_Rif = Me.txtIDUsuario.Text
            'mRs!c_Nombre = Me.txtNombreUsuario.Text
            'mRs!c_Apellido = Me.txtApellidoUsuario.Text
            Tmp = DescripcionCliente(Me.txtNombreUsuario.Text)
            mRs!c_Nombre = Tmp(1)
            mRs!c_Apellido = Tmp(0)
            mRs!b_Activo = Me.chk_Activo.value
            mRs!n_PlantillaBiometrico = 0
            mRs!c_Archivo = ""
            mRs!b_Enviado = 0
            'If InterfazOperador Then mRs!b_Operador = IIf(OptOperador.value = True, 1, 0)
        mRs.UpdateBatch
        
        IgnorarActivate
        mensaje True, "Cliente registrado."
        Me.lblRegistrado.Caption = "Puede proceder al registro de huellas.          -->"
        
        Me.txtPlantilla.Text = "0": Me.CmdPlantilla.Enabled = True: CmdPlantilla_Click 'Me.cmdReconocimiento.Enabled = True
    End If
    
    Exit Sub
    
Err:
    
    MsjErrorRapido Err.Description, "No se pudo registrar o actualizar al cliente, por favor reporte lo siguiente:" & vbNewLine & vbNewLine
    
End Sub

Private Sub limpiar()
    pCambioProgramado = True: Me.txtIDUsuario.Text = "": pCambioProgramado = False
    Me.txtNombreUsuario.Text = ""
    Me.txtApellidoUsuario.Text = ""
    Me.chk_Activo.value = vbChecked
    isnew = True
    Me.txtPlantilla.Text = ""
    Me.CmdPlantilla.Enabled = False
    Me.cmdReconocimiento.Enabled = False
    Me.lblRegistrado.Caption = "La c�dula no se encuentra registrada."
    Me.CmdGuardarDatosUsuario.Enabled = True
    'OptOperador.value = True
End Sub

Private Sub cmdLimpiar_Click()
    Call limpiar
End Sub

Private Sub CmdPlantilla_Click()
    If Me.txtIDDisp.Text <> "" And Me.txtModelo.Text <> "" And Me.txtIDUsuario.Text <> "" And Me.txtPlantilla.Text <> "" Then
        
        Select Case (Me.txtModelo.Text)
        
            'Case Is = "Digital Persona U.are.U Other Compatible FP Readers", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
    
                'DPFP_UAUConfig.Show vbModal 'Para compatibilizar se necesitar�a el SDK y se usar�a otro Form / Procedimiento.
                
            Case Is = "BioTrack BioUsb"
            
                 frmRacionamiento_Registro_BioTrack_BioUsb_Config.Show vbModal 'Pruebas 'BioTrack_BioUsb_Config.Show vbModal
            
            Case Is = "Suprema Device"
            
                frmRacionamiento_Registro_Suprema_Config.Show vbModal 'Pruebas 'Suprema
            
            Case Else
                
                IgnorarActivate
                mensaje True, "Disculpe, en estos momentos no existe compatibilidad para este dispositivo."
            
        End Select
        
    Else
        IgnorarActivate
        mensaje True, "Debe seleccionar un cliente registrado y un perfil de Dispositivo Biom�trico antes de continuar."
    End If
End Sub

Private Sub cmdReconocimiento_Click()

    Dim Respuesta As String

    If Me.txtIDDisp.Text <> "" And Me.txtModelo.Text <> "" And Me.txtIDUsuario.Text <> "" And Me.txtPlantilla.Text <> "" Then
        
        lblRegistrado.Caption = ""
        
        Select Case (Me.txtModelo.Text)
        
            'Case Is = "Digital Persona U.are.U Other Compatible FP Readers", "Digital Persona U.are.U 4000B FP Reader", "Digital Persona U.are.U 4500 FP Reader"
    
                'DPFP_UAUConfig.Show vbModal 'Para compatibilizar se necesitar�a el SDK y se usar�a otro Form / Procedimiento.
                
            Case Is = "BioTrack BioUsb"
                
                frmRacionamiento_Registro_BioTrack_BioUsb_Config.ModoReconocimiento = True
                frmRacionamiento_Registro_BioTrack_BioUsb_Config.Show vbModal
                Respuesta = frmRacionamiento_Registro_BioTrack_BioUsb_Config.ModoReconocimiento_Respuesta
                frmRacionamiento_Registro_BioTrack_BioUsb_Config.Cerrar = True
                Unload frmRacionamiento_Registro_BioTrack_BioUsb_Config
                
            Case Is = "Suprema Device"
            
                frmRacionamiento_Registro_Suprema_Config.ModoReconocimiento = True
                frmRacionamiento_Registro_Suprema_Config.Show vbModal
                Respuesta = frmRacionamiento_Registro_Suprema_Config.ModoReconocimiento_Respuesta
                frmRacionamiento_Registro_Suprema_Config.Cerrar = True
                Unload frmRacionamiento_Registro_Suprema_Config
            
            Case Else
                
                IgnorarActivate
                mensaje True, "Disculpe, en estos momentos no existe compatibilidad para este dispositivo."
            
        End Select
        
    Else
        IgnorarActivate
        mensaje True, "Debe seleccionar un cliente registrado y un perfil de Dispositivo Biom�trico antes de continuar."
    End If

    'If VerificandoOperador Then
        'txtPlantilla.Text = Respuesta
    'Else

        Select Case UCase(Respuesta)
            Case UCase("Error de inicializacion"), UCase("Captura de datos cancelada"), UCase("Error en la verificacion")
                'No se realiz� ninguna acci�n por lo tanto todo permanece deshabilitado
            Case UCase("Proceder a Registrar")
                CmdGuardarDatosUsuario.Enabled = False
                CmdPlantilla.Enabled = True
                lblRegistrado.Caption = "La huella introducida no est� registrada."
            Case UCase("Cliente confirmado")
                CmdPlantilla.Enabled = True: CmdGuardarDatosUsuario.Enabled = True
                lblRegistrado.Caption = "Cliente Confirmado."
            'Case UCase("Robo de Identidad")
                'CmdPlantilla.Enabled = False: CmdGuardarDatosUsuario.Enabled = False
                'mensaje True, "La huella se encuentra registrada con una c�dula distinta."
            Case UCase("Registros multiples")
                CmdPlantilla.Enabled = False: CmdGuardarDatosUsuario.Enabled = False
                IgnorarActivate
                mensaje True, "La huella est� duplicada para la c�dula introducida."
            'Case UCase("Registros Multiples y Robo de Identidad")
                'CmdPlantilla.Enabled = False: CmdGuardarDatosUsuario.Enabled = False
                'mensaje True, "La huella existe y est� duplicada en m�ltiples c�dulas."
        End Select
    
    'End If

End Sub

Private Sub cmdUsuario_Click()
    
    pBuscando = True
    
    Dim Client As Variant: Client = BuscarDatosCliente
    
    If Not IsEmpty(Client) Then
        If Not Trim(Client(0)) = "" Then
            Call CargarDatosCliente(CStr(Client(0)), Client)
        End If
    End If
    
    pBuscando = False
    
End Sub

Private Sub Form_Activate()

    If EvitarActivate Then Exit Sub
    
    If Not CambiosInterfazAplicados Then
    
        If Me.fClsRac Is Nothing Then
            IgnorarActivate
            mensaje True, "No se ha inicializado la configuraci�n de racionamiento. Por favor verifique la conexi�n y los valores, o contacte al soporte t�cnico."
            Unload Me: Exit Sub
        End If
        
        If pCadenaComercio = "" Or pTiendaComercio = "" Then
            IgnorarActivate
            mensaje True, "No se ha definido el n�mero correspondiente a la Cadena o Tienda. Verifique su configuraci�n."
            Unload Me: Exit Sub
        End If
        
        lblPlantilla.Visible = False: txtPlantilla.Visible = False
        
        txtIDDisp.Text = DispositivoSeleccionado: Call CargarDatosDispositivo(txtIDDisp.Text)
        
        If Trim(txtIDDisp.Text) = "" Then
            IgnorarActivate
            mensaje True, "La configuraci�n del dispositivo biom�trico seleccionado es inv�lida. " _
            & "Por favor asegurese de definir el dispositivo correctamente desde la " _
            & "herramienta de configuracion Setup.ini."
        
            Unload Me: Exit Sub
        End If
        
        frameDispositivo.Visible = False
        
        Me.Height = Me.Height - frameDispositivo.Height - 200
        Me.FrameDatosUsuario.Top = Me.FrameDatosUsuario.Top - frameDispositivo.Height - 200
        
        CambiosInterfazAplicados = True
        
        Racionalizacion_ManejaBiometrico = BuscarReglaNegocioBoolean("Racionalizacion_ManejaBiometrico", "1")

        If Not Racionalizacion_ManejaBiometrico Then
            IgnorarActivate
            mensaje True, "La configuraci�n de racionamiento a trav�s de biom�trico ha sido desactivada." _
            & vbNewLine & "Para mayor informaci�n contacte al Departamento de Administraci�n." _
            & vbNewLine & "La forma se cerrar� a continuaci�n."
        
            Unload Me: Exit Sub
        End If
        
        'Verificar Operador.
        'If Not InterfazOperador Then
        
            'VerificandoOperador = True
        
            'Dim Id As String
            
            'Id = InputBox("Indique su C.I, R.I.F o Documento de Identidad.", "Stellar isBUSINESS", , Me.Left + 200, Me.Top + 300)
            
            'Me.txtIDUsuario.Text = Id
            'Me.txtPlantilla.Text = "0"
            
            'cmdReconocimiento_Click
            
            'If UCase(txtPlantilla.Text) = UCase("No es operador") Then
                'IgnorarActivate
                'mensaje True, "No posee los permisos para acceder al m�dulo de registro."
                'Unload Me: Exit Sub
            'ElseIf UCase(txtPlantilla.Text) <> UCase("Cliente confirmado") Then
                'IgnorarActivate
                'mensaje True, "Verificaci�n Fallida."
                'Unload Me: Exit Sub
            'Else
                'Me.txtIDUsuario.Text = ""
                'Me.txtPlantilla.Text = ""
                
                'VerificandoOperador = False
                
                'Opcional. Llamar a una funci�n que registre en un Log la entrada del Operador
                    'Funcion.
                '
                
            'End If
            
        'End If
        
    End If
    
End Sub

Private Sub Form_Load()
    
    HabilitarFrameDispositivo True
    
    Dim Setup As String: Setup = MyMainClass.Setup
    
    Racionalizacion_ManejaBiometrico = BuscarReglaNegocioBoolean("Racionalizacion_ManejaBiometrico", "1")
    
    'pPermiteDesactivarClientes = BuscarReglaNegocioBoolean("Racionalizacion_ManejaStatusCliente", "1")
    pPermiteDesactivarClientes = False 'CBool(Val(sgetini(Setup, "Racionalizacion", "ManejaStatusCliente", "0")) = 1)
    chk_Activo.Visible = pPermiteDesactivarClientes
    
    Me.lblRegistrado.Left = Me.chk_Activo.Left: Me.lblRegistrado.Top = Me.chk_Activo.Top
    
    ValidarClienteSIVPRE = BuscarReglaNegocioBoolean("Racionalizacion_ValidarClienteSivpre", "1")
    
    If ValidarClienteSIVPRE Then
        SIVPRELimiteCaracteresNumericosIDV = Val(sgetini(Setup, "Racionalizacion", "LimiteCaracteresNumericosIDV", "8"))
        SIVPRENumeroMaximoIDV = Val(sgetini(Setup, "Racionalizacion", "NumeroMaximoIDV", "0"))
        SIVPRENumeroMinimoIDV = Val(sgetini(Setup, "Racionalizacion", "NumeroMinimoIDV", "0"))
        SIVPRELimiteCaracteresNumericosIDE = Val(sgetini(Setup, "Racionalizacion", "LimiteCaracteresNumericosIDE", "8"))
        SIVPRENumeroMaximoIDE = Val(sgetini(Setup, "Racionalizacion", "NumeroMaximoIDE", "0"))
        SIVPRENumeroMinimoIDE = Val(sgetini(Setup, "Racionalizacion", "NumeroMinimoIDE", "0"))
        SIVPRELimiteCaracteresNumericosIDJ = Val(sgetini(Setup, "Racionalizacion", "LimiteCaracteresNumericosIDJ", "0"))
        SIVPRENumeroMaximoIDJ = Val(sgetini(Setup, "Racionalizacion", "NumeroMaximoIDJ", "0"))
        SIVPRENumeroMinimoIDJ = Val(sgetini(Setup, "Racionalizacion", "NumeroMinimoIDJ", "0"))
        SIVPRELimiteCaracteresNumericosIDG = Val(sgetini(Setup, "Racionalizacion", "LimiteCaracteresNumericosIDG", "0"))
        SIVPRENumeroMaximoIDG = Val(sgetini(Setup, "Racionalizacion", "NumeroMaximoIDG", "0"))
        SIVPRENumeroMinimoIDG = Val(sgetini(Setup, "Racionalizacion", "NumeroMinimoIDG", "0"))
        SIVPRELimiteCaracteresNumericosIDP = Val(sgetini(Setup, "Racionalizacion", "LimiteCaracteresNumericosIDP", "0"))
        SIVPRENumeroMaximoIDP = Val(sgetini(Setup, "Racionalizacion", "NumeroMaximoIDP", "0"))
        SIVPRENumeroMinimoIDP = Val(sgetini(Setup, "Racionalizacion", "NumeroMinimoIDP", "0"))
    End If

    pDedosPermitidos = BuscarReglaNegocioStr("Racionalizacion_Biometrico_DedosPermitidos", "")
    
    pFormatoHuella = ValidarNumeroIntervalo(Val(BuscarReglaNegocioStr("Racionalizacion_Biometrico_FormatoHuella", "2")), 2, 0)
    
    pOrdenIdentificacion = ValidarNumeroIntervalo(Val(BuscarReglaNegocioStr("Racionalizacion_Biometrico_OrdenIdentificacion", "2")), 2, 0)
    
    pAcciondeRegistro = ValidarNumeroIntervalo(Val(BuscarReglaNegocioStr("Racionalizacion_Biometrico_AcciondeRegistro", "3")), 4, 0)
    
    NumRegxLote_Identificacion = ValidarNumeroIntervalo(Fix(Val(sgetini(Setup, "Racionalizacion", "NumRegxLote_Identificacion", "250000"))), , 2000)
    
    pMaxHuellasRegistro = ValidarNumeroIntervalo(Val(BuscarReglaNegocioStr("Racionalizacion_Biometrico_MaxHuellasRegistradas", "0")), 2, 0)
    
    pCalidadMinimaHuella = ValidarNumeroIntervalo(Val(BuscarReglaNegocioStr("Racionalizacion_Biometrico_CalidadMinimaHuella", "50")), 100, 0)
    
    DispositivoSeleccionado = CStr(sgetini(Setup, "Racionalizacion", "Biometrico_ID_Dispositivo", ""))
    
    pPermiteBorrarHuellasRegistradas = CBool(Val(sgetini(Setup, "Racionalizacion", "PermiteBorrarHuellas", "0")) = 1)
    
    'If InterfazOperador Then
        'OptOperador.Visible = True
        'OptCliente.Visible = True
    'Else
        OptOperador.Visible = False
        OptCliente.Visible = False
    'End If
    
    pCadenaComercio = BuscarReglaNegocioStr("Racionalizacion_Biometrico_Cadena")
    pTiendaComercio = BuscarReglaNegocioStr("Racionalizacion_Biometrico_Tienda")
    
    Call limpiar
    
    'Cambios de interfaz debido a problemas con estructurar nombre y apellido por separado.
    
    lblIDUsuario.Caption = " C.I / ID:"
    txtIDUsuario.Top = txtIDUsuario.Top + 350
    lblApellido.Visible = False: txtApellidoUsuario.Visible = False
    lblNombres.Caption = " Apellidos y Nombres:": lblNombres.Top = lblNombres.Top + 250
    txtNombreUsuario.Top = lblApellido.Top + 100: txtNombreUsuario.Left = lblApellido.Left - 25
    txtNombreUsuario.MaxLength = 50: txtNombreUsuario.Width = txtNombreUsuario.Width + 1025
    txtIDUsuario.Left = txtNombreUsuario.Left: txtIDUsuario.Width = txtNombreUsuario.Width

End Sub

Public Function BuscarDispositivoBiometrico() As Variant
    
    Dim Sql As String
    
    Sql = "Select ID_Dispositivo, c_Descripcion, c_Modelo, c_Serial, nu_NumMuestreos FROM MA_RACIONALIZACION_DISPOSITIVOSBIOMETRICOS WHERE bu_Activo = 1"
    
    Titulo = "D I S P O S I T I V O S   B I O M � T R I C O S"
    
    MyMainClass.Frm_Super_Consultas.Inicializar Sql, Titulo, Me.fClsRac.conexion
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "ID", "ID_Dispositivo", 1250, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "DESCRIPCION", "c_Descripcion", 3000, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "MODELO", "c_Modelo", 3000, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "SERIAL", "c_Serial", 1500, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "MUESTREOS", "nu_NumMuestreos", 0, 0
    
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "ID", "ID_Dispositivo"
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "DESCRIPCION", "c_Descripcion"
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "MODELO", "c_Modelo"
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "SERIAL", "c_Serial"
    
    MyMainClass.Frm_Super_Consultas.txtDato = "%"
    MyMainClass.Frm_Super_Consultas.cmdBuscar(0) = True
    
    MyMainClass.Frm_Super_Consultas.Show vbModal
    
    BuscarDispositivoBiometrico = MyMainClass.Frm_Super_Consultas.arrResultado
    
    Unload MyMainClass.Frm_Super_Consultas
    
End Function

Public Function BuscarDatosCliente() As Variant
    
    Dim Sql As String
    
    'Sql = "SELECT c_Rif, c_Nombre, c_Apellido, b_Activo, n_PlantillaBiometrico, b_Operador FROM MA_RACIONALIZACIOn_ClienteS"
    Sql = "SELECT c_Rif, c_Nombre, c_Apellido, b_Activo, n_PlantillaBiometrico FROM MA_RACIONALIZACIOn_ClienteS"
    
    Titulo = "R A C I O N A M I E N T O  -  C L I E N T E S"
    
    MyMainClass.Frm_Super_Consultas.Inicializar Sql, Titulo, Me.fClsRac.conexion
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "ID", "c_Rif", 1750, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "NOMBRE", "c_Nombre", 3500, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "APELLIDO", "c_Apellido", 3500, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "ACTIVO", "b_Activo", 0, 0
    MyMainClass.Frm_Super_Consultas.Add_ItemLabels "PLANTILLA", "n_PlantillaBiometrico", 0, 0
    'Frm_Super_Consultas.Add_ItemLabels "OPERADOR", "b_Operador", 0, 0
    
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "ID", "c_Rif"
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "NOMBRE", "c_Nombre"
    MyMainClass.Frm_Super_Consultas.Add_ItemSearching "APELLIDO", "c_Apellido"
    
    If Me.txtIDUsuario.Text = "" Then
        MyMainClass.Frm_Super_Consultas.txtDato = "%"
        MyMainClass.Frm_Super_Consultas.cmdBuscar(0) = False
    Else
        MyMainClass.Frm_Super_Consultas.txtDato = "%" & Me.txtIDUsuario.Text
        MyMainClass.Frm_Super_Consultas.cmdBuscar(0) = True
    End If
    
    MyMainClass.Frm_Super_Consultas.txtDato.SelStart = Len(MyMainClass.Frm_Super_Consultas.txtDato.Text)
    
    'MyMainClass.Frm_Super_Consultas.cmdBuscar(0) = True
    
    MyMainClass.Frm_Super_Consultas.Show vbModal
    
    BuscarDatosCliente = MyMainClass.Frm_Super_Consultas.arrResultado
    
    Unload MyMainClass.Frm_Super_Consultas
    
End Function

Private Sub HabilitarFrameDispositivo(pDecision As Boolean)
    frameDispositivo.Enabled = pDecision
    lblIDDisp.Enabled = pDecision
    txtIDDisp.Enabled = pDecision
    txtDescripcion.Enabled = pDecision
    lblModelo.Enabled = pDecision
    txtModelo.Enabled = pDecision
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmRacionamiento_Registro_de_Clientes_con_Biometrico = Nothing
End Sub

Private Sub txtApellidoUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If Me.lblRegistrado.Caption = "La c�dula no se encuentra registrada." Then
                If PuedeObtenerFoco(Me.CmdGuardarDatosUsuario) Then CmdGuardarDatosUsuario_Click
            Else
                If PuedeObtenerFoco(Me.chk_Activo) Then Me.chk_Activo.SetFocus
            End If
    End Select
End Sub

Private Sub txtApellidoUsuario_LostFocus()
    Me.txtApellidoUsuario.Text = LimitarComillasSimples(Me.txtApellidoUsuario.Text)
End Sub

Private Sub txtDescripcion_DblClick()
    If Me.txtDescripcion.Tag <> "" Then
        MsjErrorRapido Me.txtDescripcion.Tag
        Me.txtDescripcion.Tag = "": Me.txtDescripcion.Text = ""
    End If
End Sub

Private Sub txtIDDisp_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF2
            pBuscando = True
            cmd_IDDisp_Click
        Case vbKeyReturn
            txtIDDisp_LostFocus
    End Select
End Sub

Private Sub txtIDDisp_LostFocus()
    Call CargarDatosDispositivo(Me.txtIDDisp.Text)
End Sub

Private Sub txtIDUsuario_Change()
    If Not pCambioProgramado Then
        Call CargarDatosCliente(Me.txtIDUsuario.Text)
    Else
        Me.txtIDUsuario.SelStart = Len(Me.txtIDUsuario.Text)
    End If
End Sub

Private Sub txtIDUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF2
            cmdUsuario_Click
        Case vbKeyReturn
            Me.txtNombreUsuario.SetFocus
        Case vbKeyDelete
            pCambioProgramado = True: Me.txtIDUsuario.Text = "": pCambioProgramado = False
            Call limpiar
    End Select
End Sub

Private Sub txtIDUsuario_LostFocus()

    If pBuscando Then Exit Sub
    
    If ValidarClienteSIVPRE Then
        Call ClienteSIVPREValido(Me.txtIDUsuario.Text)
    Else
        Me.txtIDUsuario.Text = QuitarComillasDobles(QuitarComillasSimples(Trim(Me.txtIDUsuario.Text)))
    End If
    
End Sub

Private Sub txtNombreUsuario_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            If PuedeObtenerFoco(txtApellidoUsuario) Then
                Me.txtApellidoUsuario.SetFocus
            Else
                txtApellidoUsuario_KeyDown KeyCode, 0
            End If
    End Select
End Sub

Private Sub txtNombreUsuario_LostFocus()
    Me.txtNombreUsuario.Text = LimitarComillasSimples(Me.txtNombreUsuario.Text)
End Sub

Public Function ClienteSIVPREValido(pRif As String) As Boolean

    On Error GoTo ErrorValidar

    Dim regex As New RegExp
    Dim strPattern As String
    Dim mTempString As String
    Dim mResult As String

    Dim mErrores As String
    
    mRifValidado = False

    strPattern = "[^vejgpVEJGP0-9-]"
    regex.Global = True
    regex.Pattern = strPattern

    mTempString = pRif

    mResult = regex.Replace(mTempString, "")

    If mTempString <> mResult Then mErrores = mErrores & "La identificaci�n introducida no es v�lida." ' & vbNewLine & "Solo se acepta una letra y el resto de los numeros."

    If Len(mTempString) >= 2 Then

        Dim mTempStr As String
    
        strPattern = "[^vejgpVEJGP]"
        regex.Pattern = strPattern
        mTempStr = Left(mTempString, 1)
    
        mResult = regex.Replace(mTempStr, "")
    
        If mTempStr <> mResult Then mErrores = mErrores & vbNewLine & "El tipo de identificaci�n (Letra) debe ser" & vbNewLine & "uno de los siguientes:" & _
        vbNewLine & "V" & vbNewLine & "E" & vbNewLine & "J" & vbNewLine & "G" & vbNewLine & "P" & vbNewLine

        strPattern = "[^0-9-]"
        mTempStr = mTempString
        mTempStr = Mid(mTempStr, 2, Len(mTempStr) - 1)
        
        regex.Pattern = strPattern
    
        mResult = regex.Replace(mTempStr, "")

        If mTempStr <> mResult Then mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
    
        If IsNumeric(mTempStr) Then
            mTempStr = CStr(Val(mTempStr))
            If mTempStr <> mResult Then mErrores = mErrores & vbNewLine & "No se permiten ceros a la izquierda o car�cteres especiales."
        End If
    
        If mErrores = "" Then
        
            Select Case UCase(Left(mTempString, 1))
        
                Case Is = "V"
                
                    strPattern = "[^0-9]"
                    regex.Pattern = strPattern
                
                    If InStr(ValidarNumeroIntervalo(Len(mTempStr), , 2) - 1, mTempStr, "-") > 0 And Trim(Mid(mTempStr, ValidarNumeroIntervalo(Len(mTempStr), , 2) - 1, 1)) = "-" And regex.Replace(Right(mTempStr, 1), "") <> "" Then

                        'Debug.Print InStr(ValidarNumeroIntervalo(Len(mTempStr), , 2) - 1, mTempStr, "x") 'No es acorde a la validaci�n requerida.

                        strPattern = "[^0-9]"
                        regex.Pattern = strPattern
                                    
                        mResult = regex.Replace(mTempStr, "")

                        If IsNumeric(mResult) Then
                            mTempStr = CStr(Val(mResult))
                            If mTempStr <> mResult Then
                                mErrores = mErrores & vbNewLine & "No se permiten ceros a la izquierda o car�cteres especiales."
                            Else
                                mRifValidado = True
                            End If
                        End If

                        'Permite pasar RIF personal.
                    
                    Else

                        If SIVPRELimiteCaracteresNumericosIDV > 0 And Len(mTempStr) > SIVPRELimiteCaracteresNumericosIDV Then
                            mErrores = mErrores & vbNewLine & "L�mite de d�gitos permitidos para" & vbNewLine & "la identificaci�n (C.I Venezolana): " & SIVPRELimiteCaracteresNumericosIDV
                        End If

                        If IsNumeric(mTempStr) Then

                            If SIVPRENumeroMaximoIDV > 0 And CDbl(mTempStr) > SIVPRENumeroMaximoIDV Then
                                mErrores = mErrores & vbNewLine & "No se permiten n�meros mayores a : " & SIVPRENumeroMaximoIDV & _
                                vbNewLine & "para este tipo de identificaci�n (C.I Venezolana)."
                            End If

                            Debug.Print CDbl(mTempStr)

                            If SIVPRENumeroMinimoIDV > 0 And CDbl(mTempStr) < SIVPRENumeroMinimoIDV Then
                                mErrores = mErrores & vbNewLine & "No se permiten n�meros menores a : " & SIVPRENumeroMinimoIDV & _
                                vbNewLine & "para este tipo de identificaci�n (C.I Venezolana)."
                            End If

                        Else

                            mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."

                        End If
                    
                    End If
                
                Case Is = "E"
                
                    If SIVPRELimiteCaracteresNumericosIDE > 0 And Len(mTempStr) > SIVPRELimiteCaracteresNumericosIDE Then
                        mErrores = mErrores & vbNewLine & "L�mite de d�gitos permitidos para" & vbNewLine & "la identificaci�n (C.I Extranjera): " & SIVPRELimiteCaracteresNumericosIDE
                    End If
                    
                    If IsNumeric(mTempStr) Then
                    
                        If SIVPRENumeroMaximoIDE > 0 And CDbl(mTempStr) > SIVPRENumeroMaximoIDE Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros mayores a : " & SIVPRENumeroMaximoIDE & _
                               vbNewLine & "para este tipo de identificaci�n (C.I Extranjera)."
                        End If

                        If SIVPRENumeroMinimoIDE > 0 And CDbl(mTempStr) < SIVPRENumeroMinimoIDE Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros menores a : " & SIVPRENumeroMinimoIDE & _
                               vbNewLine & "para este tipo de identificaci�n (C.I Extranjera)."
                        End If
                    
                    Else
                    
                        mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
                    
                    End If
                
                Case Is = "J"
                
                    If SIVPRELimiteCaracteresNumericosIDJ > 0 And Len(mTempStr) > SIVPRELimiteCaracteresNumericosIDJ Then
                        mErrores = mErrores & vbNewLine & "L�mite de d�gitos permitidos para" & vbNewLine & "la identificaci�n (Jur�dica [R.I.F]): " & SIVPRELimiteCaracteresNumericosIDJ
                    End If
                    
                    If IsNumeric(mTempStr) Then
                    
                        If SIVPRENumeroMaximoIDJ > 0 And CDbl(mTempStr) > SIVPRENumeroMaximoIDJ Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros mayores a : " & SIVPRENumeroMaximoIDJ & _
                               vbNewLine & "para este tipo de identificaci�n (Jur�dica [R.I.F])."
                        End If

                        If SIVPRENumeroMinimoIDJ > 0 And CDbl(mTempStr) < SIVPRENumeroMinimoIDJ Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros menores a : " & SIVPRENumeroMinimoIDJ & _
                               vbNewLine & "para este tipo de identificaci�n (Jur�dica [R.I.F])."
                        End If
                    
                    Else
                    
                        mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
                    
                    End If
                
                Case Is = "G"
                
                    If SIVPRELimiteCaracteresNumericosIDG > 0 And Len(mTempStr) > SIVPRELimiteCaracteresNumericosIDG Then
                        mErrores = mErrores & vbNewLine & "L�mite de d�gitos permitidos para la" & vbNewLine & "identificaci�n (Gubernamental [R.I.F]): " & SIVPRELimiteCaracteresNumericosIDG
                    End If
                    
                    If IsNumeric(mTempStr) Then
                    
                        If SIVPRENumeroMaximoIDG > 0 And CDbl(mTempStr) > SIVPRENumeroMaximoIDG Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros mayores a : " & SIVPRENumeroMaximoIDG & _
                               vbNewLine & "para este tipo de identificaci�n (Gubernamental [R.I.F])."
                        End If

                        If SIVPRENumeroMinimoIDG > 0 And CDbl(mTempStr) < SIVPRENumeroMinimoIDG Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros menores a : " & SIVPRENumeroMinimoIDG & _
                               vbNewLine & "para este tipo de identificaci�n (Gubernamental [R.I.F])."
                        End If
                    
                    Else
                    
                        mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
                    
                    End If
                
                Case Is = "P"
                
                    If SIVPRELimiteCaracteresNumericosIDP > 0 And Len(mTempStr) > SIVPRELimiteCaracteresNumericosIDP Then
                        mErrores = mErrores & vbNewLine & "L�mite de d�gitos permitidos para" & vbNewLine & "la identificaci�n (Pasaporte): " & SIVPRELimiteCaracteresNumericosIDP
                    End If
                    
                    If IsNumeric(mTempStr) Then
                    
                        If SIVPRENumeroMaximoIDP > 0 And CDbl(mTempStr) > SIVPRENumeroMaximoIDP Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros mayores a : " & SIVPRENumeroMaximoIDP & _
                               vbNewLine & "para este tipo de identificaci�n (Pasaporte)."
                        End If

                        If SIVPRENumeroMinimoIDP > 0 And CDbl(mTempStr) < SIVPRENumeroMinimoIDP Then
                               mErrores = mErrores & vbNewLine & "No se permiten n�meros menores a : " & SIVPRENumeroMinimoIDP & _
                               vbNewLine & "para este tipo de identificaci�n (Pasaporte)."
                        End If
                    
                    Else
                    
                        mErrores = mErrores & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
                    
                    End If
                
                Case Else
                 
            End Select
        
        End If
        
        strPattern = "[^0-9]"
        regex.Pattern = strPattern
                    
        mResult = regex.Replace(mTempStr, "")
        
        If (mTempStr <> mResult) And Not mRifValidado Then mErrores = mErrores & vbNewLine & "No se permiten guiones ni caracteres especiales." & vbNewLine & "A continuaci�n ser�n eliminados." & vbNewLine & "Los d�gitos despu�s de la primera " & vbNewLine & "letra deben ser solo n�meros."
                        
        mTempStr = mResult
                        
        pCambioProgramado = True: Me.txtIDUsuario.Text = UCase(Left(mTempString, 1)) & mTempStr: pCambioProgramado = False
    
    Else

        mErrores = mErrores & vbNewLine & "La identificaci�n no puede contener menos de 2 d�gitos."
    
    End If

    'If Len(mTempString) > 12 Then mErrores = mErrores & vbNewLine & "La identificaci�n no puede contener" & vbNewLine & "m�s de 12 caracteres."
    'Lo anterior se maneja con los l�mites num�ricos para cada identificaci�n distinta.

    If Len(mErrores) > 0 Then
        IgnorarActivate
        mensaje True, mErrores
        ClienteSIVPREValido = False
    Else
        ClienteSIVPREValido = True
    End If

    Exit Function

ErrorValidar:

    ClienteSIVPREValido = False
    Debug.Print Err.Description

End Function

Private Function ValidarDatos() As Boolean
        
    ValidarDatos = True
    
'    Dim DatosCorrectos As Boolean
    
    If ValidarClienteSIVPRE Then
        If Not mRifValidado Then
            ValidarDatos = ClienteSIVPREValido(Me.txtIDUsuario.Text)
            If Not ValidarDatos Then Exit Function
        End If
    End If
    
    If Len(Me.txtIDUsuario.Text) <= 0 Then
        IgnorarActivate
        Call mensaje(True, "Debe ingresar la C.I / ID del cliente.")
        ValidarDatos = False
        Exit Function
    End If
    
    If Len(Me.txtNombreUsuario.Text) <= 0 Then
        IgnorarActivate
        Call mensaje(True, "Debe ingresar el Nombre(s) del cliente.")
        ValidarDatos = False
        Exit Function
    End If
    
    'If Len(Me.txtApellidoUsuario.Text) <= 0 Then
        'IgnorarActivate
        'Call mensaje(True, "Debe ingresar el Apellido(s) del cliente.")
        'ValidarDatos = False
        'Exit Function
    'End If

End Function

Private Function DescripcionCliente(pCadena As String) As Variant
 
    On Error GoTo Err
 
    Dim Temporal As Variant
    Dim nPalabras As Integer
    Dim Datos(1) As String
    Temporal = Split(pCadena, " ")
    nPalabras = UBound(Temporal) + 1
    
    If (nPalabras Mod 2 = 0) Then
        For i = 0 To (nPalabras / 2) - 1
            Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
        Next i
        For i = (nPalabras / 2) To (nPalabras - 1)
            Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
        Next i
    Else
        If (nPalabras = 1) Then
            Datos(0) = Temporal(0)
        Else
            For i = 0 To Fix((nPalabras / 2) - 1)
                Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
            Next i
            For i = Fix(nPalabras / 2) To (nPalabras - 1)
                Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
            Next i
        End If
    End If
    
    DescripcionCliente = Datos
    
    Exit Function
    
Err:
    
    DescripcionCliente = Split(".", ".")
    
End Function
