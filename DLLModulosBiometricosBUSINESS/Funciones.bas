Attribute VB_Name = "Funciones"
Public gRutinas As Object

'Para obtener el tama�o de la pantalla
Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" _
(ByVal dwMessage As Long, pData As APPBARDATA) As Long

Private Declare Function GetDeviceCaps Lib "gdi32" _
(ByVal hdc As Long, ByVal nIndex As Long) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, _
ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Type SystemTime
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Private Declare Sub GetSystemTime Lib "Kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Sub GetLocalTime Lib "Kernel32.dll" (lpSystemTime As SystemTime)
Private Declare Function GetTickCount Lib "kernel32" () As Long

Public Function CheckCad(Texto1 As Object, Optional CantDec As Integer = 2, Optional Mensaje As String, Optional Porcent As Boolean = True) As Boolean

    cadena = Texto1.Text
    nprueba = 1234.1234
    DecChar = Mid(FormatNumber(nprueba, 4), Len(FormatNumber(nprueba, 4)) - 4, 1)
    MilChar = Mid(FormatNumber(nprueba, 4), 2, 1)
    CheckCad = True
    c_mil = 0
    c_numi = 0
    c_numd = 0
    c_com = 0
    c_por = 0
    If CantDec < 0 Then
        Resp = MsgBox("Error de entrada en la Funci�n CheckCad, Par�metro [CantDec]=Cantidad de Decimales; esta debe ser mayor que 0.", vbCritical, "Error de Entrada de Datos")
        Exit Function
    End If

    
'    If Not IsEmpty(Porcent) And False Then
'        Porcent = True
'    End If
    If Len(cadena) = 0 Then
        CheckCad = False
    End If
    For n = 1 To Len(cadena)
        If Mid(cadena, n, 1) = DecChar Then
           c_com = c_com + 1
           If c_com > 1 Or CantDec = 0 Then
              If n < Len(Texto1.Text) - 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
              Else
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1)
              End If
              Texto1.SelStart = n - 1
              Texto1.SelLength = 0
              Mensaje = "Demaciados Separadores de decimales o no debe colocar decimales."
              CheckCad = False
              Exit Function
           End If
           
           If c_numi < 1 Then
              If n < Len(Texto1.Text) - 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
              Else
                  If Len(Texto1.Text) > 1 Then
                     Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                  Else
                     Texto1.Text = ""
                     Texto1.Refresh
                  End If
              End If
              Texto1.SelStart = n - 1
              Texto1.SelLength = 0
              Mensaje = "No hay numeros en la parte entera."
              CheckCad = False
              Exit Function
           End If
        End If
        If Mid(cadena, n, 1) = "%" And Porcent Then
            c_por = c_por + 1
            If c_por > 1 Then
              If n < Len(Texto1.Text) - 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
              Else
                  If Len(Texto1.Text) > 1 Then
                     Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                  Else
                     Texto1.Text = ""
                     Texto1.Refresh
                  End If
              End If
              Texto1.SelStart = n - 1
              Texto1.SelLength = 0
              Mensaje = "Demasiados Simbolos porcentuales."
              CheckCad = False
              Exit Function
            End If
            If n <> 1 And n <> Len(cadena) Then
              If n < Len(Texto1.Text) - 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
              Else
                  If Len(Texto1.Text) > 1 Then
                     Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                  Else
                     Texto1.Text = ""
                     Texto1.Refresh
                  End If
              End If
              Texto1.SelStart = n - 1
              Texto1.SelLength = 0
              Mensaje = "Simbolo Porcentual Debe colocarse al principio o al final del valor numerico."
              CheckCad = False
              Exit Function
            End If
        ElseIf Mid(cadena, n, 1) = "%" And Not Porcent Then
        
            If n < Len(Texto1.Text) - 1 Then
                  Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
            Else
                If Len(Texto1.Text) > 1 Then
                   Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                Else
                   Texto1.Text = ""
                   Texto1.Refresh
                End If
            End If
            Texto1.SelStart = n - 1
            Texto1.SelLength = 0
            Mensaje = "El texto tiene datos porcentuales."
            CheckCad = False
            Exit Function
        
        End If
        If Asc(Mid(cadena, n, 1)) >= 48 And Asc(Mid(cadena, n, 1)) <= 57 Then
           If c_com > 0 Then
                c_numd = c_numd + 1
           Else
                c_numi = c_numi + 1
           End If
        Else
            'NO SE PUEDE MODIFICAR POR QUE ESTA FUNCION EST MAS UTILIZADO
            'OR (Mid(Cadena, n, 1) = "%" And Not porcentaje)
           If (Not Mid(cadena, n, 1) = DecChar And Not Mid(cadena, n, 1) = "%" And Not Mid(cadena, n, 1) = MilChar) Then
                If n < Len(Texto1.Text) - 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
                Else
                    If Len(Texto1.Text) > 1 Then
                        Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                    Else
                        Texto1.Text = ""
                        Texto1.Refresh
                    End If
                End If
                Texto1.SelStart = n - 1
                Texto1.SelLength = 0
                Mensaje = "Caracter no Numerico"
                CheckCad = False
                Exit Function
            End If
        End If
        If c_numd > CantDec Then
            If n < Len(Texto1.Text) - 1 Then
                Texto1.Text = Mid(Texto1.Text, 1, n - 1) & Mid(Texto1.Text, n + 1, Len(Texto1.Text))
            Else
                If Len(Texto1.Text) > 1 Then
                    Texto1.Text = Mid(Texto1.Text, 1, n - 1)
                Else
                    Texto1.Text = ""
                    Texto1.Refresh
                End If
            End If
            Texto1.SelLength = 1
            Texto1.SelStart = n
            Mensaje = "Se ha introducido demasiados decimales"
        End If
    Next n
    
    'MsgBox Texto1 + "&"
    
End Function

Public Sub MAKE_VIEW(ByRef NTABLA, NCAMPO1, NCAMPO2, NTITULO, name_form As Form, COMENTARIO As String, Optional ByVal BusquedaRapida As Boolean = False)
    
    Titulo = NTITULO
    Tabla = NTABLA
    Campo_Op1 = NCAMPO1
    Campo_Op2 = NCAMPO2
    Set Forma = name_form
    Forma.Tag = COMENTARIO
    'Unload VIEW_CONSULTAS
    Set m = Forms.Add("VIEW_CONSULTAS")
    m.txt_dato = "%"
    
    m.BusquedaInstantanea = BusquedaRapida
    m.Show vbModal
    
    Set m = Nothing
    'Forms.Remove ("VIEW_CONSULTAS")

End Sub

Public Sub PasarTrPend(ByVal pTabla, ByRef pRsOringen As ADODB.Recordset, _
ByVal pCnnDestino As ADODB.Connection, ByVal pCnnVeriPosSuc As ADODB.Connection, _
Optional ByVal pCamposExepto = "", _
Optional ByVal pCamposAdicionales = "", _
Optional ByVal pEliminarOringen As Boolean = False)
    If ManejaPOS(pCnnVeriPosSuc) Or ManejaSucursales(pCnnVeriPosSuc) Then
        gClsDatos.CopiarRsaTabla pTabla, pRsOringen, pCnnDestino, pCamposExepto, pCamposAdicionales, pEliminarOringen
    End If
End Sub

Public Function ManejaSucursales(PConnection) As Boolean
    Dim mRstmp As New ADODB.Recordset
    Sql = "SELECT  COUNT(*) AS Expr1 FROM  MA_SUCURSALES WHERE  (c_estado = 1)"
    Apertura_Recordset mRstmp
    mRstmp.Open Sql, PConnection, adOpenForwardOnly, adLockReadOnly
    ManejaSucursales = IIf(mRstmp!Expr1 < 1, False, True)
    mRstmp.Close
    Set mRstmp = Nothing
End Function

Public Function ManejaPOS(PConnection) As Boolean
    Dim mRstmp As New ADODB.Recordset
    Sql = "SELECT  MANEJA_POS FROM  REGLAS_COMERCIALES"
    Apertura_Recordset mRstmp
    mRstmp.Open Sql, PConnection, adOpenForwardOnly, adLockReadOnly
    ManejaPOS = IIf(mRstmp!MANEJA_POS, True, False)
    mRstmp.Close
    Set mRstmp = Nothing
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Rec.CursorLocation = adUseServer
End Sub

Public Function campoLength(Campo As String, Tabla As String, Server_BD As ADODB.Connection, fieldType As Enum_FieldType, Optional minLength As Integer = 1) As Integer
    
    On Error GoTo FUAAA
    
    Dim rsLength As New ADODB.Recordset

    rsLength.Open "SELECT " & Campo & " FROM " & Tabla & " WHERE 1=2", Server_BD, adOpenForwardOnly, adLockReadOnly
    
    Select Case fieldType
    
    Case Enum_FieldType.Alfanumerico
        campoLength = rsLength.Fields(Campo).DefinedSize
    Case Enum_FieldType.Numerico
        campoLength = rsLength.Fields(Campo).Precision
    Case Else
        campoLength = rsLength.Fields(Campo).DefinedSize
    End Select
    
    If campoLength < minLength Then campoLength = minLength
    
    rsLength.Close
    
    Exit Function

FUAAA:

    Debug.Print Err.Description
    campoLength = minLength
    
End Function

Public Function KillSecure(pFile As String) As Boolean
    On Error GoTo KS
    Kill pFile: KillSecure = True
    Exit Function
KS:
    'Debug.Print Err.Description
    Err.Clear 'Ignorar.
End Function

Public Function KillShot(pFile As String) As Boolean
    KillShot = KillSecure(pFile)
End Function

Public Function LoadFile(StrFileName As _
String) As String

    On Error GoTo Err

'        Dim sFileText As String
'        Dim iFileNo As Integer
'        iFileNo = FreeFile
'        'open the file for reading
'        Open StrFileName For Input As #iFileNo
'         Dim Tmp
'        'read the file until we reach the end
'        I = 1
'        Do While Not EOF(iFileNo)
'          Tmp = Input(1, #iFileNo)
'          LoadFile = LoadFile & CStr(Tmp)
'          I = I + 1
'        Loop
'
'        'close the file (if you dont do this, you wont be able to open it again!)
'        Close #iFileNo
        
        Dim Tmp As New ShowRTF
        
        Tmp.Ri2.LoadFile (StrFileName)
        LoadFile = Tmp.Ri2.Text
        
        Debug.Print LoadFile
        
        Set Tmp = Nothing
        
    Exit Function
    
Err:
    
    Debug.Print Err.Description

End Function

Public Function Mensaje(Activo As Boolean, Texto As String, _
Optional pVbmodal = True) As Boolean
    
    Uno = Activo
    
    On Error GoTo Falla_Local
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & _
        IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            frm_Mensajeria.Show
            frm_Mensajeria.Aceptar.Enabled = False
            Set frm_Mensajeria = Nothing
        End If
        
    End If
    
    Mensaje = Retorno
    
Falla_Local:
    
End Function

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Set Rec = Nothing
End Sub

Public Sub apertura_recordsetc(ByRef Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Rec.CursorLocation = adUseClient
End Sub

Public Function BuscarRs(ByVal cadena As String, conexion As ADODB.Connection) As ADODB.Recordset
    Dim rsTemp As New ADODB.Recordset
    rsTemp.CursorLocation = adUseServer
    rsTemp.Open cadena, conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    'rsTemp.Open Cadena, Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    'rsTemp.ActiveConnection = Nothing
    Set BuscarRs = rsTemp
End Function

Public Function sGetIni(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sGetIni = Left$(STemp, NLength)
End Function

Public Function QuitarComillasDobles(pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function MsjErrorRapido(pDescripcion As String, Optional pMsjIntro As String = _
"Ha ocurrido un error, por favor reporte lo siguiente: ")

    Mensaje True, pMsjIntro & pDescripcion

End Function

Public Function BuscarReglaNegocioBoolean(pCampo, Optional pIgualA) As Boolean
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    On Error GoTo Errores
    mSql = "Select * from ma_reglasdenegocio where campo='" & pCampo & "' "
    mRs.Open mSql, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarReglaNegocioBoolean = CStr(mRs!Valor) = IIf(Not IsMissing(pIgualA), CStr(pIgualA), CStr(1))
    End If
    Exit Function
Errores:
    Err.Clear
End Function

Public Function BuscarReglaNegocioStr(pCampo, Optional pDefault As String = "") As String
    Dim mRs As New ADODB.Recordset
    Dim mSql As String
    
    On Error GoTo Errores
    mSql = "Select * from ma_reglasdenegocio where campo='" & pCampo & "' "
    mRs.Open mSql, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    Exit Function
Errores:
    Err.Clear
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function HexToString(ByVal HexToStr As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim I         As Long
    
    For I = 1 To Len(HexToStr) Step 2
        strTemp = Chr$(Val("&H" & Mid$(HexToStr, I, 2)))
        StrReturn = StrReturn & strTemp
    Next I
    
    HexToString = StrReturn
    
End Function

Public Function StringToHex(ByVal StrToHex As String) As String
    
    Dim strTemp   As String
    Dim StrReturn As String
    Dim I         As Long
    
    For I = 1 To Len(StrToHex)
        strTemp = Hex$(Asc(Mid$(StrToHex, I, 1)))
        If Len(strTemp) = 1 Then strTemp = "0" & strTemp
        StrReturn = StrReturn & Space$(1) & strTemp
    Next I
    
    StringToHex = StrReturn
    
End Function

Public Function LimitarComillasSimples(pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function Collection_AddKey(pCollection As Collection, pValor, pKey As String) As Boolean
    
    On Error GoTo Err
    
    pCollection.Add pValor, pKey
    
    Collection_AddKey = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_AddKey = False
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim I As Long
    
    For I = pIndiceStart To pCollection.Count
        If pCollection.Item(I) = pValor Then Collection_EncontrarValor = I: Exit Function
    Next I
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    On Error GoTo Error
    If Not IsMissing(pRs) Then
     
        mAux = pRs.Fields(pCampo).value
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        If IsMissing(pCn) Then
            mRs.Open pSql, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        Else
            mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        End If
        ExisteCampoTabla = True
        mRs.Close
    End If
    Exit Function
    
Error:
    'MsgBox Err.Description
    Err.Clear
    ExisteCampoTabla = False
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function GetTaskBarInfo() As APPBARDATA
    
    On Error Resume Next
    
    Dim ABD As APPBARDATA
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarInfo = ABD
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    
    On Error GoTo Error
    
    Dim TbInfo As APPBARDATA
    
    TbInfo = GetTaskBarInfo
    
    If TbInfo.rc.Bottom = 0 And TbInfo.rc.Top = 0 And TbInfo.rc.Right = 0 And TbInfo.rc.Left = 0 Then
        TbInfo.uEdge = -1
    End If
    
    Select Case TbInfo.uEdge
        
        Case 3
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) - (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If Forma.Top < 0 Then
                Forma.Top = 0
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left) < 0 Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 2
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) - (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < 0 Then
                Forma.Left = 0
            End If
            
        Case 1
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) + (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
            
            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left < 0) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 0
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) + (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < ((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case -1
            
            Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
            Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
            
            If Forma.Left < 0 Then Forma.Left = 0
            If Forma.Top < 0 Then Forma.Top = 0
            
    End Select
    
    ' El Form Navegador tiene un footer que siempre debe quedar en bottom.
    
    If EsFormNavegador Then
        Forma.Frame1.Top = (Forma.Height - Forma.Frame1.Height)
    End If
    
    Exit Sub
    
Error:
    
    Debug.Print Err.Description
    
End Sub

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, _
Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function SafeEquals(ByVal pVal1, ByVal pVal2) As Boolean
    On Error Resume Next
    SafeEquals = (pVal1 = pVal2) ' Attempt to Compare Values.
    If Err.Number = 0 Then Exit Function
    SafeEquals = (pVal1 Is pVal2) ' Attempt to Compare Object References.
End Function

'Funci�n que devuelve un n�mero entero aleatorio.

Public Function RandomInt(minimo As Long, Maximo As Long) As Long
    Randomize ' inicializar la semilla
    RandomInt = CLng((minimo - Maximo) * Rnd + Maximo)
End Function

Public Sub EsperaManual(ByVal pTiempoSegundos As Long)
    
    If pTiempoSegundos > 0 Then
        
        Dim A As Variant, B As Variant
        
        A = Now
        B = DateAdd("s", pTiempoSegundos, A)
        
        While A < B
            
            A = Now
            DoEvents
            
        Wend
        
    Else
        Exit Sub
    End If
    
End Sub

Public Function GetCurrentSystemTime() As SystemTime
    Dim SysT As SystemTime
    On Error Resume Next
    GetLocalTime SysT
    GetCurrentSystemTime = SysT
End Function

Public Sub EsperaManualMillis(ByVal pTiempoMilliSegundos As Long)
    
    If pTiempoMilliSegundos > 0 Then
        
        Dim mSumar As Variant, mInicio As Variant, CantCiclos As Variant
        
        'Dim A As SystemTime, B As SystemTime
        
        'A = GetCurrentSystemTime
        
        Dim c As Variant, D As Variant
        
        'C = CDec(A.wMilliseconds) + (CDec(A.wSecond) * CDec(1000)) + _
        (CDec(A.wMinute) * CDec(60000)) + (CDec(A.wHour) * CDec(3600000))
        
        c = SafeTickCount
        
        mInicio = c
        
        D = mInicio + pTiempoMilliSegundos
        
        CantCiclos = CDec(0)
        
        Do While c < D
            
            CantCiclos = CantCiclos + 1
            
            'B = GetCurrentSystemTime
            
            'C = CDec(B.wMilliseconds) + (CDec(B.wSecond) * CDec(1000)) + _
            (CDec(B.wMinute) * CDec(60000)) + (CDec(B.wHour) * CDec(3600000))
            
            c = SafeTickCount
            
            DoEvents
            
            If c = 0 Then
                Exit Do
            End If
            
        Loop
        
    Else
        Exit Sub
    End If
    
End Sub

Public Function SafeTickCount() As Long
    On Error Resume Next
    SafeTickCount = GetTickCount
End Function

Public Function GetTickDifInMillis(ByVal pTickIni As Long, ByVal pTickEnd As Long, _
Optional ByVal pIgnoreSeconds As Boolean = True, _
Optional ByRef pFixStartDate As Date, _
Optional ByRef pFixEndDate As Date) As Long
    Dim FullTickDifInMillis As Long
    FullTickDifInMillis = (pTickEnd - pTickIni)
    GetTickDifInMillis = FullTickDifInMillis
    If pIgnoreSeconds Then
        GetTickDifInMillis = GetTickDifInMillis - (Fix(CDec(GetTickDifInMillis) / CDec(1000)) * CDec(1000))
    End If
    If CDec(pFixStartDate) > 0 And CDec(pFixEndDate) Then
        If DateDiff("s", pFixStartDate, pFixEndDate) > Fix(CDec(FullTickDifInMillis) / CDec(1000)) Then
            pFixEndDate = DateAdd("s", -1, pFixEndDate)
        End If
    End If
End Function

Public Function FechaBD(ByVal Expression, _
Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function
