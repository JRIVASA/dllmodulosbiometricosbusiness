VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form VIEW_CONSULTAS 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "B�squeda de ...."
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8385
   Icon            =   "VIEW_CONSULTAS.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   8385
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox CoolBar1 
      Align           =   1  'Align Top
      Height          =   795
      Left            =   0
      ScaleHeight     =   735
      ScaleWidth      =   8325
      TabIndex        =   7
      Top             =   0
      Width           =   8385
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   735
         Left            =   30
         TabIndex        =   8
         Top             =   30
         Width           =   8265
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   705
            Left            =   60
            TabIndex        =   9
            Top             =   30
            Width           =   8190
            _ExtentX        =   14446
            _ExtentY        =   1244
            ButtonWidth     =   1905
            ButtonHeight    =   1244
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            ImageList       =   "Icono_Apagado"
            DisabledImageList=   "Icono_deshabilitado"
            HotImageList    =   "Iconos_Encendidos"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   7
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Informaci�n"
                  Key             =   "Informacion"
                  Object.ToolTipText     =   "Muestra Informaci�n del Proveedor"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Dep�sitos"
                  Key             =   "Depositos"
                  Object.ToolTipText     =   "Muestra el inventario del producto en los dep�sitos del sistema"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Proveedores"
                  Key             =   "proveedores"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Object.Visible         =   0   'False
                  Caption         =   "Clasificaci�n"
                  Key             =   "clasificacion"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de la B�squeda"
                  ImageIndex      =   9
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda del Sistema"
                  ImageIndex      =   10
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Ordenar y Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   1050
      Left            =   120
      TabIndex        =   4
      Top             =   5475
      Width           =   2970
      Begin VB.OptionButton opt_cod 
         Caption         =   "Por &C�digo"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   270
         TabIndex        =   6
         Top             =   315
         Width           =   1335
      End
      Begin VB.OptionButton opt_des 
         Caption         =   "Por &Descripci�n"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   255
         TabIndex        =   5
         Top             =   570
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos a Buscar:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   1050
      Left            =   3120
      TabIndex        =   1
      Top             =   5475
      Width           =   5145
      Begin VB.CommandButton CANCELAR 
         Caption         =   "Cancelar B�squeda"
         CausesValidation=   0   'False
         Height          =   840
         Left            =   3990
         Picture         =   "VIEW_CONSULTAS.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   150
         Width           =   1095
      End
      Begin VB.TextBox txt_dato 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3795
      End
      Begin MSComctlLib.ProgressBar barra_prg 
         Height          =   165
         Left            =   120
         TabIndex        =   2
         Top             =   765
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   291
         _Version        =   393216
         Appearance      =   1
         Max             =   1000
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1155
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":69F4
            Key             =   "Agrergar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":76D0
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":83AC
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":9088
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":9D64
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":AA40
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":B71C
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":C3F8
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":D0D4
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":D3F0
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   1845
      Top             =   630
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":E0CC
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":EDA8
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":F0C4
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":F3E0
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":100BC
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":10D98
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":11A74
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":11D90
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":12A6C
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":12D88
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_deshabilitado 
      Left            =   480
      Top             =   585
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   25
      ImageHeight     =   25
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":13A64
            Key             =   "Agregar"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":14740
            Key             =   "Buscar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":1541C
            Key             =   "Modificar"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":15738
            Key             =   "Cancelar"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":16414
            Key             =   "Eliminar"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":170F0
            Key             =   "Grabar"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":17DCC
            Key             =   "Imprimir"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":18AA8
            Key             =   "Estadisticas"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":19784
            Key             =   "Salir"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "VIEW_CONSULTAS.frx":19AA0
            Key             =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   4275
      Left            =   120
      TabIndex        =   10
      Top             =   1170
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   7541
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�DIGO"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "DESCRIPCION"
         Object.Width           =   10231
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   3060
      Left            =   2520
      Picture         =   "VIEW_CONSULTAS.frx":1A77C
      Stretch         =   -1  'True
      Top             =   1710
      Width           =   3240
   End
   Begin VB.Label ltitulo 
      Alignment       =   2  'Center
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   8115
   End
End
Attribute VB_Name = "VIEW_CONSULTAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CANCELAR_BUSQUEDA As Boolean
Dim INICIAR_BUSQUEDA As Boolean
Public mMostrarProveedores As Boolean
Public vieneDeInventario As Boolean
Public BusquedaInstantanea As Boolean

Public EvitarActivate As Boolean

Private Function IgnorarActivate() As Boolean
    EvitarActivate = True: IgnorarActivate = EvitarActivate
End Function

Private Sub cancelar_Click()
    CANCELAR_BUSQUEDA = True
    INICIAR_BUSQUEDA = False
End Sub

Private Sub Form_Activate()
    If EvitarActivate Then EvitarActivate = False: Exit Sub
    If Me.BusquedaInstantanea Then txt_dato_KeyPress vbKeyReturn
End Sub

Private Sub Form_Load()
On Error GoTo Errores
    Set Forma1 = Forma
    Forma1.Tag = Forma.Tag
    ltitulo.Caption = Titulo
    
    If UCase(Forma1.Tag) = "CLIENTES" Or UCase(Forma1.Tag) = "PROVEEDOR" Then
       Toolbar1.Buttons(1).Visible = False
    End If
    
    Select Case UCase(Tabla)
        Case Is = "MA_PRODUCTOS"
            Toolbar1.Buttons(2).Visible = True
            Toolbar1.Buttons(3).Visible = True
        Case Else
            Toolbar1.Buttons(2).Visible = False
            Toolbar1.Buttons(3).Visible = False
    End Select
    
  '  SavePicture cancelar.Picture, "C:\TEMP\imgCancelar.ico"
    
    '** NIVEL PARA VER DEPOSITOS
    'NivelUsuario
    Sql = "select valor from ma_reglasdenegocio where campo = 'NIVELDEPOSITOSENFICHAPRODUCTOS'"
    Dim RsNivel As New ADODB.Recordset
    RsNivel.Open Sql, ENT.BDD, adOpenForwardOnly, adLockReadOnly
    If Not RsNivel.EOF Then
        If RsNivel!valor > NivelUsuario Then Toolbar1.Buttons(2).Enabled = False
    End If
Exit Sub
Errores:
    Unload Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo error_formkeydown
If Toolbar1.Enabled = False Then Exit Sub
    Select Case KeyCode
        Case Is = vbKeyF2
            If Toolbar1.Buttons(1).Visible = False Then Exit Sub
            If GRID.ListItems.Count = 0 Then Exit Sub
            Select Case UCase(Tabla)
                Case Else
                    IgnorarActivate
                    Call mensaje(True, "Opci�n no disponible.")
            End Select
            
        Case vbKeyF12, vbKeyEscape
            CANCELAR_BUSQUEDA = True
            Unload Me
    End Select

Exit Sub
error_formkeydown:
    Unload Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
On Error GoTo Errores
    If Cancel = 0 Then Unload Me
Exit Sub
Errores:
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    CANCELAR_BUSQUEDA = True
    Call Cerrar_Recordset(RsEureka)
    Set VIEW_CONSULTAS = Nothing
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If GRID.SortOrder = lvwAscending Then
        GRID.SortOrder = lvwDescending
    Else
        GRID.SortOrder = lvwAscending
    End If
    GRID.SortKey = ColumnHeader.index - 1
    GRID.Sorted = True
End Sub

Private Sub grid_DblClick()
On Error GoTo error_griddbl
    With GRID
        If GRID.ListItems.Count <> 0 Then
        Debug.Print UCase(Forma.Tag)
            Select Case UCase(Forma.Tag)
                  
            '****************************************
            '*** AREA generica
            '****************************************
                 Case Is = "GENERICO"
                    
                    Campo_Txt = GRID.ListItems(GRID.SelectedItem.index)
                    Campo_Lbl = GRID.ListItems(GRID.SelectedItem.index).SubItems(1)
                    On Error Resume Next
                    strPlantilla = GRID.ListItems(GRID.SelectedItem.index).SubItems(3)
                    Err.Clear
                  
            '****************************************
            '*** AREA DE USUARIOS
            '****************************************
                 'Case Is = "USUARIOS"
                    'Forma.usuario.Text = GRID.ListItems(GRID.SelectedItem.index)
                    'Forma.lbl_Usuario.Caption = GRID.ListItems(GRID.SelectedItem.index).SubItems(1)
        
            End Select
            Unload Me
        Else
            IgnorarActivate
            Call mensaje(True, "No se ha realizado ninguna consulta.")
        End If
    End With

Exit Sub
error_griddbl:
    Unload Me
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Private Sub opt_cod_Click()
On Error GoTo Errores
    INICIAR_BUSQUEDA = True
    txt_dato.SetFocus
Exit Sub
Errores:
    Unload Me
End Sub
Private Sub opt_des_Click()
On Error GoTo Errores
    INICIAR_BUSQUEDA = True
    txt_dato.SetFocus
    Exit Sub
Errores:
    Unload Me
End Sub
Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case Is = "INFORMACION"
            Call Form_KeyDown(vbKeyF2, 0)
            
        Case Is = "DEPOSITOS"
            Call Form_KeyDown(vbKeyF3, 0)
            
        Case Is = "PROVEEDORES"
            Call Form_KeyDown(vbKeyF4, 0)
            
        Case Is = "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
    End Select
End Sub

'Private Sub txt_dato_GotFocus()
'    If Mid(txt_dato, 1, 1) = "%" Then txt_dato.SelStart = 1
'End Sub

Private Sub txt_dato_KeyPress(KeyAscii As Integer)
    Dim c_Sucursal As String, Sql As String, rs As New ADODB.Recordset, LastTxtDatos As String
    Dim mCodigos As String, mIn As Boolean
    On Error GoTo error_txtdatokeypress
    
    Select Case KeyAscii
'        Case Is = 39
'            KeyAscii = 0
        Case Is = vbKeyReturn
            
            If INICIAR_BUSQUEDA = True Then Exit Sub
            INICIAR_BUSQUEDA = True
            
'            Toolbar1.Enabled = False
            LastTxtDatos = txt_dato
            txt_dato.Text = Replace(txt_dato.Text, "'", "''")
            Call limpiar_grid
            i = 1
            If txt_dato = "" Then
                txt_dato.SetFocus
                Exit Sub
            End If
            
            Screen.MousePointer = 11
            If txt_dato = "%" And opt_cod.value = True Then
                IgnorarActivate
                Call mensaje(True, "La b�squeda por c�digo debe ser m�s espec�fica.")
                Exit Sub
            End If
            If opt_cod.value = True Then
                orden = Campo_Op1
            Else
                orden = Campo_Op2
            End If
            
            'If UCase(Tabla) = "MA_PRODUCTOS" And opt_cod.value = True Then
                        
                Criterio = "SELECT " & Campo_Op1 & "," & Campo_Op2 & IIf(UCase(Tabla) = "MA_PRODUCTOS", ", c_Modelo, c_Cod_Plantilla, c_Marca ", "") & IIf(UCase(Tabla) = "MA_PROVEEDORES", ", c_RAZON ", "") & " FROM " & Tabla & " WHERE " & orden & " like '"
                
'                If vieneDeInventario Then
'                    Dim rsInventario As New ADODB.Recordset, sql As New ADODB.Recordset
'                End If
                
                If txt_dato <> "%" Then
                    If UCase(Tabla) = "MA_BANCOS" Then
                        Aplicar_Formato "Auto_Banco", "Caracter_Banco", txt_dato
                    ElseIf UCase(Tabla) = "MA_CLIENTES" Then
                        Aplicar_Formato "Auto_Cliente", "Caracter_Cliente", txt_dato
                    ElseIf UCase(Tabla) = "MA_DEPOSITO" Then
                        Aplicar_Formato "Auto_Deposito", "Caracter_Deposito", txt_dato
                    ElseIf UCase(Tabla) = "MA_MONEDAS" Then
                        Aplicar_Formato "Auto_Moneda", "Caracter_Moneda", txt_dato
                    ElseIf UCase(Tabla) = "MA_SUCURSALES" Then
                        Aplicar_Formato "Auto_Localidad", "Caracter_Localidad", txt_dato
                    ElseIf UCase(Tabla) = "MA_PROVEEDORES" Then
                        Aplicar_Formato "Auto_Proveedor", "Caracter_Proveedor", txt_dato
                    ElseIf UCase(Tabla) = "MA_USUARIOS" Then
                        Aplicar_Formato "Auto_Usuario", "Caracter_Usuario", txt_dato
                    ElseIf UCase(Tabla) = "MA_USUARIOS" Then
                        Aplicar_Formato "Auto_Usuario", "Caracter_Usuario", txt_dato
                    ElseIf UCase(Tabla) = "MA_VENDEDORES" Then
                        Aplicar_Formato "Auto_Vendedor", "Caracter_Vendedor", txt_dato
                    End If
                    Criterio = Criterio & txt_dato & "%'"
                Else
                    Criterio = Criterio & txt_dato & "'"
                End If
             
                Select Case Tabla
                    Case "ma_deposito"
                        
                        If lcTipoUsuario = True And UCase(Forma.Tag) <> "RECEPCION" And Left(UCase(Forma.Tag), 8) <> "TRASLADO" Then
                            
                        Else
                            If Not lcTipoUsuario Then Criterio = Criterio & " and c_CodLocalidad = '" & lcLocalidad_User & "'"
                        End If
                        
                End Select
               
            'End If

            Call apertura_recordsetc(RsBingo)
                        
            If UCase(Tabla) = "MA_ETIQUETAS" Then
                RsBingo.Open Criterio, ENT.POS, adOpenForwardOnly, adLockReadOnly, adCmdText
            Else
                RsBingo.Open Criterio, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
            End If
                        
            CANCELAR_BUSQUEDA = False
            Set RsBingo.ActiveConnection = Nothing
            If Not RsBingo.EOF Then
                Call apertura_recordsetc(RsEureka)

                barra_prg.Max = RsBingo.RecordCount ' RsEureka!cant
                Dim X As Variant
                GRID.ListItems.Clear
                GRID.Enabled = False
                GRID.Visible = False
                
                
                If UCase(Tabla) = "MA_PRODUCTOS" Or UCase(Tabla) = "MA_BANCOSCTA" Then
                    Set X = GRID.ColumnHeaders.Add(, , "MODELO")
                    Set X = GRID.ColumnHeaders.Add(, , "codigoplantilla")
                    Set X = GRID.ColumnHeaders.Add(, , "MARCA")
                    
                    
                    GRID.ColumnHeaders(2).Width = 4000
                    GRID.ColumnHeaders(3).Width = 2000
                    GRID.ColumnHeaders(4).Width = 0
                    If UCase(Tabla) = "MA_PRODUCTOS" Then
                        GRID.ColumnHeaders(5).Width = 2000
                    End If
                ElseIf UCase(Tabla) = "MA_PROVEEDORES" Then
                    Set X = GRID.ColumnHeaders.Add(, , "RAZON SOCIAL")
                    GRID.ColumnHeaders(3).Width = 4000
                
                End If
                
                Do Until RsBingo.EOF
                    Set ItmX = GRID.ListItems.Add(, , RsBingo.Fields(0))
                    ItmX.SubItems(1) = RsBingo.Fields(1)
                    If UCase(Tabla) = "MA_PRODUCTOS" Then
                        Set X = ItmX.ListSubItems.Add(, , RsBingo.Fields(2))
                        ' 26/08/2002
                        Set X = ItmX.ListSubItems.Add(, , RsBingo.Fields(3))
                        If RsBingo.Fields.Count > 4 Then
                           Set X = ItmX.ListSubItems.Add(, , RsBingo.Fields(4))
                        Else
                            Set X = ItmX.ListSubItems.Add(, , RsBingo.Fields(3))
                        End If
                    ElseIf UCase(Tabla) = "MA_PROVEEDORES" Then
                        ItmX.SubItems(2) = RsBingo.Fields("C_RAZON")
'                        Set X = ItmX.ListSubItems.add(, , RsBingo.Fields("C_RAZON"))

                    End If
                    RsBingo.MoveNext
                    barra_prg.value = barra_prg.value + 1
                    If CANCELAR_BUSQUEDA Then Exit Do
                    DoEvents
                Loop
                GRID.Enabled = True
                GRID.Visible = True
                barra_prg.value = 0
                GRID.SetFocus
                Call Cerrar_Recordset(RsBingo)
            Else
                IgnorarActivate
                Call mensaje(True, "B�squeda sin �xito.")
                barra_prg.value = 0
                Call Cerrar_Recordset(RsBingo)
            End If
            Screen.MousePointer = 0
            Toolbar1.Enabled = True
            txt_dato = LastTxtDatos
    End Select
INICIAR_BUSQUEDA = False
Exit Sub
error_txtdatokeypress:
    txt_dato = ""
    Toolbar1.Enabled = True
    Unload Me
End Sub

Private Sub limpiar_grid()
    With GRID
        .ListItems.Clear
    End With
End Sub

Private Sub cargar_productos(Campo As String, boton1 As Boolean, boton2 As Boolean, boton3 As Boolean, boton4 As Boolean, boton5 As Boolean, boton6 As Boolean, Ver As Boolean, Proveedor As Boolean, Cod_Alt As Boolean)
On Error GoTo error_cargar
'        Call Apertura_Recordset(RsProductos)
'        RsProductos.Open "select * from ma_productos where c_Codigo = '" & GRID.ListItems(GRID.SelectedItem.index) & "'", ENT.BDD, adOpenForwardOnly, adLockBatchOptimistic
'        Call Ficha_Productos.Cargar_Campos(RsProductos)
'        Call Ficha_Productos.Habilitar_Datos(False)
'        Ficha_Productos.Toolbar1.Buttons(1).Enabled = boton1
'        Ficha_Productos.Toolbar1.Buttons(2).Enabled = boton2
'        Ficha_Productos.Toolbar1.Buttons(6).Enabled = boton3
'        Ficha_Productos.Toolbar1.Buttons(3).Enabled = boton4
'        Ficha_Productos.Toolbar1.Buttons(10).Enabled = boton6
'        Ficha_Productos.Proveedor.Enabled = Proveedor
'        Ficha_Productos.Cod_Alt.Enabled = Cod_Alt
'        If Ver = True Then Ficha_Productos.Show vbModal

Exit Sub
error_cargar:
    'Call GRABAR_ERRORES(Err.Number, Err.Description, lcCodUsuario, Me.Name, Err.Source)
    Unload Me

End Sub

Public Sub Aplicar_Formato(Campo_Auto As String, Campo_Caract As String, dato As Variant)
On Error GoTo Errores
' Ivan Espinoza 07/09/01
    Dim Sql As String, rs As New ADODB.Recordset
    Dim NRelleno As Integer, Relleno As String
    If Me.opt_cod.value = True Then
        Sql = "Select " & Campo_Auto & ", " & Campo_Caract & " FROM Reglas_Comerciales"
        Set rs = Datos.BuscarRs(Sql, ENT.BDD)
        If Not rs.EOF Then
            If rs(Campo_Auto) = True Then
                NRelleno = rs(Campo_Caract)
                Relleno = String(NRelleno, "0")
                dato = Format(dato, Relleno)
                Me.txt_dato.Text = dato
            End If
        End If
    End If
Exit Sub
Errores:
    'Call GRABAR_ERRORES(Err.Number, Err.Description, lcCodUsuario, Me.Name, Err.Source)
    Unload Me
End Sub
