VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MainClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Public ModuloAcceso_Ready As Boolean
Public fClsRacionamiento As Object
Public Frm_Super_Consultas  As Form
Public Setup As String

Public Sub InicializarModuloLogin(pConexionAdm As ADODB.Connection, _
pConexionPOS As ADODB.Connection, _
pSetup As String)
    
    Setup = pSetup
    
    Set ENT.BDD = pConexionAdm
    Set ENT.POS = pConexionPOS
    
    'MANEJO DE DISPOSITIVO BIOMETRICO CAPTAHUELLAS
    
    ModuloCaptahuellas.ADM_CaptaHuellas_Maneja = Val(sGetIni(Setup, "CaptaHuellas", "Maneja", "0")) = 1
    ModuloCaptahuellas.ADM_CaptaHuellas_IDDispositivo = sGetIni(Setup, "CaptaHuellas", "Dispositivo", Empty)
    ModuloCaptahuellas.ADM_CaptaHuellas_TiempoEsperaPOSTVerificacion = Val(sGetIni(Setup, "CAPTAHUELLAS", "TiempoEsperaPOSTVerificacion", "1250"))
    'ModuloCaptahuellas.ADM_CaptaHuellas_DemoKey = sGetIni(Setup, "CAPTAHUELLAS", "DemoToken", Empty)
    
    ModuloCaptahuellas.ADM_CaptaHuellas_CodUsuarioActivo = Empty ' Caso cambiar de usuario / cambiar de empresa.
    
    'AQUI SOLO RECOJO VALORES DE CONFIGURACION
    'PERO ANTES DE MOSTRAR EL LOGIN VERIFICO QUE EL DISPOSITIVO ESTA EN LAS TABLAS
    'DE Srv_Remote_BD_ADM Y QUE SEA COMPATIBLE CON EL STELLAR
    'SINO SE CUMPLEN ESTAS CONDICIONES ENTONCES NO SE TRABAJA CON BIOMETRICO.
    
    ModuloAcceso_Ready = True
    
    'FIN MANEJO DE DISPOSITIVO BIOMETRICO CAPTAHUELLAS
    
End Sub

Public Sub VerificarConfiguracionModuloAcceso()
    
    If ADM_CaptaHuellas_Maneja Then
        
        Dim ConfigCaptaHuellas As New ADODB.Recordset
        
        ConfigCaptaHuellas.Open _
        "SELECT * FROM MA_CAPTAHUELLAS_PERFILES " & _
        "WHERE ID_DISPOSITIVO = '" & ADM_CaptaHuellas_IDDispositivo & "' ", _
        ENT.BDD, adOpenStatic, adLockReadOnly
        
        If Not ConfigCaptaHuellas.EOF Then
            ModuloCaptahuellas.ADM_CaptaHuellas_ModeloDispositivo = ConfigCaptaHuellas!c_Modelo
            ModuloCaptahuellas.ADM_CaptaHuellas_nVerificaciones = ConfigCaptaHuellas!NU_NUMVERIFICACIONES
            ModuloCaptahuellas.ADM_CaptaHuellas_nMaxErrores = ConfigCaptaHuellas!nu_MaxErroresVerificacion
            ModuloCaptahuellas.ADM_CaptaHuellas_LocalidadUsuarios = Sucursal
        Else
            ModuloCaptahuellas.ADM_CaptaHuellas_Maneja = False
            'ADM_CaptaHuellas_DispositivoCompatible = False
            Mensaje True, "No se encontró la configuración del dispositivo CaptaHuellas especificado."
        End If
        
        ModuloCaptahuellas.ADM_CaptaHuellas_DispositivoCompatible = VerificarDispositivoCompatible
        
        ConfigCaptaHuellas.Close
        
        ModuloCaptahuellas.ADM_CaptaHuellas_HayRegistros = True
        
    End If
    
End Sub

Property Get ADM_CaptaHuellas_Maneja() As Boolean
    ADM_CaptaHuellas_Maneja = ModuloCaptahuellas.ADM_CaptaHuellas_Maneja
End Property

Property Get ADM_CaptaHuellas_HayRegistros() As Boolean
    ADM_CaptaHuellas_HayRegistros = ModuloCaptahuellas.ADM_CaptaHuellas_HayRegistros
End Property

Property Get ADM_CaptaHuellas_DispositivoCompatible() As Boolean
    ADM_CaptaHuellas_DispositivoCompatible = ModuloCaptahuellas.ADM_CaptaHuellas_DispositivoCompatible
End Property

Property Get ADM_CaptaHuellas_IDDispositivo() As String
    ADM_CaptaHuellas_IDDispositivo = ModuloCaptahuellas.ADM_CaptaHuellas_IDDispositivo
End Property

Public Function VerificarRegistrosAcceso() As Boolean
    VerificarRegistrosAcceso = ModuloCaptahuellas.VerificarRegistros
End Function

Public Function ValidarAccesoCaptaHuellas() As Boolean
    ValidarAccesoCaptaHuellas = ModuloCaptahuellas.ValidarAccesoCaptaHuellas
End Function

Property Get ADM_CaptaHuellas_CodUsuarioActivo() As String
    ADM_CaptaHuellas_CodUsuarioActivo = ModuloCaptahuellas.ADM_CaptaHuellas_CodUsuarioActivo
End Property

Property Let ADM_CaptaHuellas_CriterioSQL(pValor As String)
    ModuloCaptahuellas.ADM_CaptaHuellas_CriterioSQL = pValor
End Property

Property Get ValidacionEnProceso() As Boolean
    ValidacionEnProceso = ModuloCaptahuellas.ValidacionEnProceso
End Property

Property Let PROP_FrmAppLink(ByVal pObj)
    Set FrmAppLink = pObj
End Property

Public Sub Set_gClsDatos(pCls As Object)
    Set gClsDatos = pCls
End Sub

Public Sub Set_DatosUsuario( _
ByVal pTipoUsuario, _
ByVal pLocalidad_User, _
ByVal pLocalidadDes_User, _
ByVal pCodUsuario, _
ByVal pNivelUsuario _
)
    
    LcTipoUsuario = pTipoUsuario
    LcLocalidad_User = pLocalidad_User
    LcLocalidadDes_User = pLocalidadDes_User
    LcCodUsuario = pCodUsuario
    NivelUsuario = pNivelUsuario
    
End Sub

'Public Sub Get_DatosUsuario( _
'ByRef pTipoUsuario, _
'ByRef pLocalidad_User, _
'ByRef pLocalidadDes_User, _
'ByRef pCodUsuario, _
'ByRef pNivelUsuario _
')
'
'    pTipoUsuario = lcTipoUsuario
'    pLocalidad_User = lcLocalidad_User
'    pLocalidadDes_User = lcLocalidadDes_User
'    pCodUsuario = lcCodUsuario
'    pNivelUsuario = NivelUsuario
'
'End Sub

Public Sub ModuloAcceso_AbrirFichadePerfiles()
    ficha_PerfilesCaptaHuellas.Show vbModal
End Sub

Public Sub ModuloRacionamiento_AbrirFichadePerfiles()
    Set frmRacionamiento_PerfilesBiometricos.fClsRac = fClsRacionamiento
    frmRacionamiento_PerfilesBiometricos.Show vbModal
    Set fClsRacionamiento = Nothing
End Sub

Public Sub ModuloRacionamiento_AbrirInterfazdeRegistro()
    Set frmRacionamiento_Registro_de_Clientes_con_Biometrico.fClsRac = fClsRacionamiento
    Set frmRacionamiento_Registro_de_Clientes_con_Biometrico.MyMainClass = Me
    frmRacionamiento_Registro_de_Clientes_con_Biometrico.Show vbModal
    Set fClsRacionamiento = Nothing
End Sub

Property Let ClaseRutinas(pCls As Object)
    Set Funciones.gRutinas = pCls
End Property

