Attribute VB_Name = "rtfClase"
Public Rep As Reporte


Function NumPaginas(LinRepeRepor As Integer, LinRepeCons As Integer) As Integer
    Dim var1 As Integer
    Dim var2 As Integer
  
  var1 = LinRepeCons Mod LinRepeRepor
  '***********************************************
  ' 12/11/2004
  '***********************************************
'    If LinRepeRepor = 1 Then
'      mDivisor = 1
'    Else
'      mDivisor = (LinRepeRepor - 1)
'    End If
'    If LinRepeCons Mod 2 = 0 Then
'       mRestarunaPag = 1
'    Else
'       mRestarunaPag = 0
'    End If
    
  '***********************************************
  If var1 > 0 Then
      NumPaginas = Int(LinRepeCons / LinRepeRepor) + 1

'      NumPaginas = Int(LinRepeCons / mDivisor) + 1 - mRestarunaPag
    Else
      NumPaginas = LinRepeCons / LinRepeRepor
'      NumPaginas = LinRepeCons / mDivisor
  End If

End Function




Public Function FormatearCadena(CadenaText As String, CadenaFormato As String) As String
  'limpia la CadenaText de que no tenga salto de carros
 
  CadenaText = Replace(CadenaText, Chr(13), " ")
  CadenaText = Replace(CadenaText, Chr(10), " ")
  CadenaText = Trim(CadenaText)
  
  
  
  If Mid(CadenaFormato, 2, 1) = " " And Mid(CadenaFormato, Len(CadenaFormato) - 1, 1) = " " Then
    'A = Mid(CadenaFormato, Len(CadenaFormato) - 1, 1)
     Long_Formato = Len(CadenaFormato)
     Long_Texto = Len(CadenaText)
        If CDbl(Long_Formato) > CDbl(Long_Texto) Then




             Spacios = CInt((CDbl(Long_Formato) - CDbl(Long_Texto)))
             'FormatearCadena = Space(Len(CadenaFormato) - Len(Trim(CadenaText))) & CadenaText
             FormatearCadena = Space(Spacios) & CadenaText
             a = Len(FormatearCadena)
             
        Else
              FormatearCadena = CadenaText
        End If
        
     Exit Function
  End If
  
  If Mid(CadenaFormato, 2, 1) = " " Then ' se alinea a la derecha
      If Len(Trim(CadenaText)) <= Len(CadenaFormato) Then
          FormatearCadena = Space(Len(CadenaFormato) - Len(Trim(CadenaText))) & CadenaText
      Else
          FormatearCadena = CadenaText
      End If
  Else 'de otra forma a la izquierda
      If Len(Trim(CadenaText)) >= Len(CadenaFormato) Then
         FormatearCadena = Mid(Trim(CadenaText), 1, Len(CadenaFormato))
      Else
         FormatearCadena = CadenaText & Space(Len(CadenaFormato) - Len(Trim(CadenaText)))
        
      End If
        c = Len(FormatearCadena)
        d = Len(CadenaFormato)
  End If
  
End Function
